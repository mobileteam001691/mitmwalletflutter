import 'package:flutter/material.dart';


class LanguagePage extends StatefulWidget {
  @override
  _LanguagePageState createState() => _LanguagePageState();
}

class _LanguagePageState extends State<LanguagePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(40, 103, 178, 1),
        centerTitle: true,
        title: Text("Language Setting"),
      ),
      body: Container(
              color: Colors.white,
              child: Column(
                
         children: <Widget>[
              SizedBox(height:10),
              ListTile(
                
                // leading:CircleAvatar(
                //   radius: 30.0,
                //   ) ,
                title:Text("English",style: TextStyle(fontSize: 18,color: Colors.black)),
                
                trailing:Icon(Icons.chevron_right,color: Color.fromRGBO(40, 103, 178, 1)),
                  onTap: (){
                    // Navigator.push(
                    // context,
                    // MaterialPageRoute(builder: (context) => ChangePassword()),
                    // );
                  },
                ),
                Divider(color: Colors.grey,),
                SizedBox(height:5),
                ListTile(
                
                // leading:CircleAvatar(
                //   radius: 30.0,
                //   ) ,
                title:Text("Myanmar",style: TextStyle(fontSize: 18,color: Colors.black)),
                
                trailing:Icon(Icons.chevron_right,color: Color.fromRGBO(40, 103, 178, 1)),
                  onTap: (){
                    // Navigator.push(
                    // context,
                    // MaterialPageRoute(builder: (context) => ResetPassword()),
                    // );
                  },
                ),
                Divider(color: Colors.grey,),

            ]
              ),
            ),
    );
  }
}