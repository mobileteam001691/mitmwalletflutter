import 'package:flutter/material.dart';


class FontPage extends StatefulWidget {
  @override
  _FontPageState createState() => _FontPageState();
}

class _FontPageState extends State<FontPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(40, 103, 178, 1),
        centerTitle: true,
        title: Text("Font Setting"),
      ),
      body: Container(
              color: Colors.white,
              child: Column(
                
         children: <Widget>[
           SizedBox(height: 10),
              ListTile(
                
                title:Text("Unicode",style: TextStyle(fontSize: 18,color: Colors.black)),
                
                trailing:Icon(Icons.chevron_right,color: Color.fromRGBO(40, 103, 178, 1)),
                  onTap: (){
                    // Navigator.push(
                    // context,
                    // MaterialPageRoute(builder: (context) => ChangePassword()),
                    // );
                  },
                ),
                Divider(color: Colors.grey,),
                
                ListTile(
                
                title:Text("Zawgyi",style: TextStyle(fontSize: 18,color: Colors.black)),
                
                trailing:Icon(Icons.chevron_right,color: Color.fromRGBO(40, 103, 178, 1)),
                  onTap: (){
                    // Navigator.push(
                    // context,
                    // MaterialPageRoute(builder: (context) => ResetPassword()),
                    // );
                  },
                ),
                Divider(color: Colors.grey,),

            ]
              ),
            ),
    );
  }
}