import 'package:flutter/material.dart';
import 'package:nsb/pages/setting/font.dart';
import 'package:nsb/pages/setting/language.dart';


class SettingPage extends StatefulWidget {
  @override
  _SettingPageState createState() => _SettingPageState();
}

class _SettingPageState extends State<SettingPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(40, 103, 178, 1),
        centerTitle: true,
        title: Text("Setting"),
      ),
      body: Container(
              color: Colors.white,
              child: Column(
                
         children: <Widget>[
              ListTile(
                
                title:Text("Language",style: TextStyle(fontSize: 18,color: Colors.black)),
                
                trailing:Icon(Icons.chevron_right,color: Color.fromRGBO(40, 103, 178, 1)),
                  onTap: (){
                    Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => LanguagePage()),
                    );
                  },
                ),
                Divider(color: Colors.grey,),
                
                ListTile(
                
                title:Text("Font",style: TextStyle(fontSize: 18,color: Colors.black)),
                
                trailing:Icon(Icons.chevron_right,color: Color.fromRGBO(40, 103, 178, 1)),
                  onTap: (){
                    Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => FontPage()),
                    );
                  },
                ),
                Divider(color: Colors.grey,),

            ]
              ),
            ),
    );
  }
}