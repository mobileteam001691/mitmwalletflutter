import 'dart:async';
import 'dart:convert';
import 'package:nsb/framework/http_service.dart' as http;
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:nsb/constants/constant.dart';
import 'package:nsb/model/GetAccountListRequest.dart';
import 'package:nsb/model/GetAccountListResponse.dart';
import 'package:nsb/pages/Transfer/transferconfirm.dart';
import 'package:nsb/pages/agent/agentTransfer.dart';
import 'package:shared_preferences/shared_preferences.dart';

class BranchLocation extends StatefulWidget {
  final String value1;
  final String value2;
  final String value3;

  BranchLocation({
    Key key,
    this.value1,
    Key key1,
    this.value2,
    Key key2,
    this.value3,
  }) : super(key: key);

  @override
  _BranchLocationState createState() => _BranchLocationState();
}

class _BranchLocationState extends State<BranchLocation> {
  // String phoneno;
  String alertmsg = "";
  bool _isLoading;
  // String phoneNo="";
  var all = [];
  final _formKey = new GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldkey = new GlobalKey<ScaffoldState>();
  static final CREATE_POST_URL =
      'http://52.187.13.89:8080/WalletService/module001';
  var contactList = [];
  final myController = TextEditingController();
  String a;

  checkPhNoRequest() async {
    final prefs = await SharedPreferences.getInstance();
    String userID = prefs.getString('userId');
    String sessionID = prefs.getString('sessionID');
    String loginID = "+959" + "${widget.value2}".substring(2, 11);
    print(loginID);
    a = loginID;
    print(a);
    String url =
        "http://52.187.13.89:8080/WalletService/module001/chatservice/checkPhoneNo";
    Map<String, String> headers = {"Content-type": "application/json"};
    String json = '{ "userID": "' +
        userID +
        '", "sessionID": "' +
        sessionID +
        '","loginID":"' +
        loginID +
        '" }';
    http.Response response = await http.post(url, headers: headers, body: json);
    int statusCode = response.statusCode;
    if (statusCode == 200) {
      String body = response.body;
      print(body);
      var data = jsonDecode(body);
      print(data);
      setState(() {
        all = data["data"];
      });
    } else {
      print("Connection Fail");
    }
  }

  void _method1() {
    _scaffoldkey.currentState.showSnackBar(new SnackBar(
        content: new Text(this.alertmsg), duration: Duration(seconds: 2)));
  }

  @override
  void initState() {
    super.initState();
    checkLanguage();
    checkPhNoRequest();
  }

  getAccountList() async {
    final prefs = await SharedPreferences.getInstance();
    String userID = prefs.getString('userId');
    String sessionID = prefs.getString('sessionID');
    print("GETACC");
    GetAccountListRequest accountListRequest =
        new GetAccountListRequest(userID: userID, sessionID: sessionID);
    // Navigator.pop(context);
    GetAccountListResponse accountListResponse = await getAllAddContact(
        CREATE_POST_URL + '/service001/getAccountList',
        accountListRequest.toMap());
    //  myController.text="";
    if (accountListResponse.code == '0000') {
      this.alertmsg = accountListResponse.desc;
      this._method1();
      // this.checkPhNo();
      // print(contactList[8]);
      checkPhNoRequest();
    } else {
      this.alertmsg = accountListResponse.desc;
      print(Text("ewrwerwerwer"));
      this._method1();
      final form = _formKey.currentState;
      form.validate();
    }
  }

  Future<GetAccountListResponse> getAllAddContact(url, Map jsonMap) async {
    GetAccountListResponse p = new GetAccountListResponse();

    var body;
    try {
      body = await http.doPost(url, jsonMap);
      p = GetAccountListResponse.fromJson(json.decode(body.toString()));
      var data = jsonDecode(body);
      setState(() {
        contactList = data["accountList"];
      });
    } catch (e) {
      p.code = Constants.responseCode_Error;
      p.desc = e.toString();
    }
    print(p.toMap());
    return p;
  }

  //  checkPhNo() async{
  //     final prefs = await SharedPreferences.getInstance();
  //         print(Text("checkPhNO....."));
  //        String userID=prefs.getString('userId');
  //        String sessionID=prefs.getString('sessionID');
  //        print(userID);
  //        print(sessionID);
  //        print("${widget.value2}");

  //    checkPhNoRequest checkPhnolistrequest=new checkPhNoRequest(userID: userID,sessionID: sessionID,loginID: "${widget.value2}");
  //    CheckPhNoResponse checkPhnolistresponse= await getAllList(CREATE_POST_URL+'/chatservice/checkPhoneNo', checkPhnolistrequest.toMap());
  //    if(checkPhnolistresponse.code=='0000'){
  //      print(Text("ttt"));
  //       //  var route = new MaterialPageRoute(
  //       //         builder: (BuildContext context) =>
  //       //           new AgentTransfer(value1: (contactList[index]["name"]), value2: value2));
  //       //         Navigator.of(context).push(route);
  //        }
  // }
  // Future<CheckPhNoResponse> getAllList(url, Map jsonMap) async{
  //   CheckPhNoResponse p = new CheckPhNoResponse();
  //   var body;
  //   try{
  //     body = await http.doPost(url, jsonMap);
  //     p = CheckPhNoResponse.fromJson(json.decode(body.toString()));
  //   }catch(e){
  //     p.code= Constants.responseCode_Error;
  //     p.desc = e.toString();
  //   }

  //   print(p.toMap());
  //   return p;
  // }

  Completer<GoogleMapController> _controller = Completer();
  static const LatLng _center = const LatLng(16.8818934, 96.1215111);
  final Set<Marker> _markers = {};
  LatLng _lastMapPosition = _center;
  MapType _currentMapType = MapType.normal;
  String checklang = '';
  List textMyan = ["တည်နေရာ", "ငွေလွှဲရန်"];
  List textEng = ["Location", "Transfer"];

  static final CameraPosition _position1 = CameraPosition(
    // bearing: 192.833,
    target: LatLng(16.8818934, 96.1215111),
    // tilt: 59.440,
    zoom: 11.0,
  );

  Future<void> _gotoPosition1() async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(_position1));
  }

  _onMapCreated(GoogleMapController controller) {
    _controller.complete(controller);
  }

  _onCameraMove(CameraPosition position) {
    _lastMapPosition = position.target;
  }

  // _onMapTypeButtonPressed(){
  //   setState((){
  //     _currentMapType = _currentMapType == MapType.normal
  //       ? MapType.satellite
  //       : MapType.normal;
  //   });
  // }

  _onMapTypeMapButtonPressed() {
    setState(() {
      _currentMapType = MapType.normal;
    });
  }

  _onMapTypeSatelliteButtonPressed() {
    setState(() {
      _currentMapType = MapType.satellite;
    });
  }

  _onAddMarkerButtonPressed() {
    setState(() {
      _markers.add(
        Marker(
          markerId: MarkerId(_lastMapPosition.toString()),
          position: _lastMapPosition,
          infoWindow: InfoWindow(
            title: 'This is a Title',
            snippet: 'This is a snippet',
          ),
          icon: BitmapDescriptor.defaultMarker,
        ),
      );
    });
  }

  checkLanguage() async {
    final prefs = await SharedPreferences.getInstance();
    checklang = prefs.getString("Lang");
    // print(lang);
    if (checklang == "" || checklang == null || checklang.length == 0) {
      checklang = "Eng";
    } else {
      checklang = checklang;
    }
    setState(() {});
  }

  Widget button(Function function, IconData icon) {
    return FloatingActionButton(
      onPressed: function,
      materialTapTargetSize: MaterialTapTargetSize.padded,
      backgroundColor: Colors.blue,
      child: Icon(
        icon,
        size: 36.0,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldkey,
      appBar: AppBar(
        centerTitle: true,
        title: (checklang == "Eng")
            ? Text(
                (checklang == "Eng") ? textEng[0] : textMyan[0],
                style: TextStyle(fontSize: 20, color: Colors.white),
              )
            : Text(
                (checklang == "Eng") ? textEng[0] : textMyan[0],
                style: TextStyle(fontSize: 18, color: Colors.white),
              ),
        backgroundColor: Color.fromRGBO(40, 103, 178, 1),
      ),
      body: SingleChildScrollView(
        key: _formKey,
        child: Column(
          children: <Widget>[
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.65,
              child: Stack(
                children: <Widget>[
                  GoogleMap(
                    onMapCreated: _onMapCreated,
                    initialCameraPosition: CameraPosition(
                      target: _center,
                      zoom: 17.0,
                    ),
                    mapType: _currentMapType,
                    markers: _markers,
                    onCameraMove: _onCameraMove,
                  ),
                  Padding(
                    padding: EdgeInsets.all(16.0),
                    child: Align(
                      alignment: Alignment.topRight,
                      child: Column(
                        children: <Widget>[
                          // button(_onMapTypeButtonPressed, Icons.map),
                          Row(
                            children: <Widget>[
                              RaisedButton(
                                child: Text("Map"),
                                onPressed: _onMapTypeMapButtonPressed,
                                color: Colors.white,
                                textColor: Colors.black,
                                padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                              ),
                              RaisedButton(
                                child: Text("Satellite"),
                                onPressed: _onMapTypeSatelliteButtonPressed,
                                color: Colors.white,
                                textColor: Colors.black,
                                padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                              ),
                              SizedBox(
                                height: 16.0,
                              ),
                              // button(_onAddMarkerButtonPressed, Icons.add_location),
                              SizedBox(
                                height: 16.0,
                              ),
                              // button(_gotoPosition1, Icons.location_searching),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Align(
                      alignment: Alignment.bottomRight,
                      child: FloatingActionButton(
                        onPressed: _onAddMarkerButtonPressed,
                        child: Icon(Icons.location_on),
                      ),
                    ),
                  )
                ],
              ),
            ),
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.25,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Card(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            children: <Widget>[
                              Text('Name:'),
                              Padding(
                                padding: const EdgeInsets.only(left: 80.0),
                                child: Text("${widget.value1}"),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            children: <Widget>[
                              Text('Phone No:'),
                              Padding(
                                padding: const EdgeInsets.only(left: 56.0),
                                child: Text("${widget.value2}"),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            children: <Widget>[
                              Text('Address:'),
                              Padding(
                                padding: const EdgeInsets.only(left: 66.0),
                                child: Text("${widget.value3}"),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 10.0),
                          child: Row(
                            children: <Widget>[
                              RaisedButton(
                                onPressed: () {
                                  getAccountList();
                                  var route = new MaterialPageRoute(
                                      builder: (BuildContext context) =>
                                          new AgentTransfer(
                                              value1: a,
                                              value2: "${widget.value1}"));
                                  Navigator.of(context).push(route);
                                },
                                child: (checklang == "Eng")
                                    ? Text(
                                        (checklang == "Eng")
                                            ? textEng[1]
                                            : textMyan[1],
                                        style: TextStyle(
                                            fontSize: 16, color: Colors.white),
                                      )
                                    : Text(
                                        (checklang == "Eng")
                                            ? textEng[1]
                                            : textMyan[1],
                                        style: TextStyle(
                                            fontSize: 14, color: Colors.white),
                                      ),
                                color: Colors.blue[900],
                                textColor: Colors.white,
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
