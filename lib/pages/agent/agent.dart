// import 'package:flutter/material.dart';
// import 'branchlocation.dart';
// import 'nearby.dart';

// class Agent extends StatefulWidget {
//   @override
//   _AgentState createState() => _AgentState();
// }

// class _AgentState extends State<Agent> {

//   List branch = [
//     Branch(
//           branchname: 'KAWTAWGYI Branch',
//           phonenum: '09778189884',
//         ),
//     Branch(
//           branchname: 'SULE Branch',
//           phonenum: '09770001680',
//         ),

//   ];

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         backgroundColor: Color.fromRGBO(40, 103, 178, 1),
//         title: Padding(
//           padding: const EdgeInsets.only(right:45.0),
//           child: Center(child: new Text('Agent')),
//         ),
//         elevation: 5.0,
//         actions: <Widget>[
//           new IconButton(
//             icon: new Image.asset('assets/images/googlemap.png',height: 100,width: 200,fit: BoxFit.cover),
//             // tooltip: 'Press',
//             onPressed: (){
//               Navigator.push(
//                     context,
//                     MaterialPageRoute(builder: (context) =>Nearby()),
//               );
//             },
//           ),
//         ],
//       ),
//       body: new Container(
//         child: Column(
//           children: <Widget>[
//               new Padding(
//                 padding: const EdgeInsets.all(15.0),
//                 child: Card(
//                   elevation: 3,
//                   shape: RoundedRectangleBorder(
//                       borderRadius: BorderRadius.circular(5.0),
//                   ),
//                  child: Padding(
//                    padding: const EdgeInsets.only(left: 20.0),
//                    child: new TextField(
//                        // controller: searchController,
//                        decoration: InputDecoration(
//                         icon: new Icon(Icons.search),
//                         hintText: 'Search',
//                         focusedBorder: InputBorder.none,
//                         border: InputBorder.none,
//                        ),
//                      ),
//                  ),
//                 ),
//               ),
//             Expanded(
//               child: Card(
//                 child: Padding(
//                   padding: const EdgeInsets.all(8.0),
//                   child: ListView.builder(
//                   itemCount: branch.length,
//                   itemBuilder: (context, index) {
//                     return Container(
//                       child: Column(
//                         children: <Widget>[
//                           ListTile(
//                             title: Text(branch[index].branchname,style: TextStyle(fontWeight: FontWeight.w800,fontSize: 14),),
//                             subtitle: Text(branch[index].phonenum, style: TextStyle(fontSize: 13),),
//                             trailing: Icon(Icons.phone,color: Colors.blue,),
//                             onTap: () {
//                               Navigator.push(
//                                 context,
//                                 MaterialPageRoute(builder: (context) =>BranchLocation()),
//                               );
//                             },
//                           ),
//                           Divider(),
//                         ],
//                       ),
//                     );
//                   },
//                 ),
//                  ),
//               ),
//             ),
//           ],
//         ),
//       ),
//     );
//   }
// }

// class Branch {
//   String branchname;
//   String phonenum;

//   Branch({this.branchname, this.phonenum});
// }

import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:nsb/pages/LocationDetail.dart';
import 'package:nsb/pages/Transaction/TransitionMain.dart';
import 'package:nsb/pages/agent/branchlocation.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class AgentPage extends StatefulWidget {
  final String value1;
  final String value2;
  final String value3;

  AgentPage({
    Key key,
    this.value1,
    Key key1,
    this.value2,
    Key key2,
    this.value3,
  }) : super(key: key);

  @override
  _AgentPageState createState() => _AgentPageState();
}

class _AgentPageState extends State<AgentPage> {
  Completer<GoogleMapController> _controller = Completer();

  static const LatLng _center = const LatLng(45.521563, -122.677433);

  final Set<Marker> _markers = {};

  LatLng _lastMapPosition = _center;

  MapType _currentMapType = MapType.normal;

  void _onMapTypeButtonPressed() {
    setState(() {
      _currentMapType = _currentMapType == MapType.normal
          ? MapType.satellite
          : MapType.normal;
    });
  }

  void _onAddMarkerButtonPressed() {
    setState(() {
      _markers.add(Marker(
        markerId: MarkerId(_lastMapPosition.toString()),
        position: _lastMapPosition,
        infoWindow: InfoWindow(
          title: 'Really cool place',
          snippet: '5 Star Rating',
        ),
        icon: BitmapDescriptor.defaultMarker,
      ));
    });
  }

  void _onCameraMove(CameraPosition position) {
    _lastMapPosition = position.target;
  }

  void _onMapCreated(GoogleMapController controller) {
    _controller.complete(controller);
  }

  var location = new Location();
  Map<String, double> userLocation;

  List agentDataList = new List();
  TextEditingController controller = new TextEditingController();
  String checklang = '';
  List textMyan = ["ကိုယ်စားလှယ်"];
  List textEng = ["Agent"];
  List locationList = [];
  List searchList = [];
  List agentList = [];
  void initState() {
    checkLanguage();
    super.initState();
    getLocation();
  }

  checkLanguage() async {
    final prefs = await SharedPreferences.getInstance();
    checklang = prefs.getString("Lang");
    // print(lang);
    if (checklang == "" || checklang == null || checklang.length == 0) {
      checklang = "Eng";
    } else {
      checklang = checklang;
    }
    setState(() {});
  }

  getLocation() async {
    String url =
        "http://52.187.13.89:8080/WalletService/module001/service002/getLocation";
    Map<String, String> headers = {"Content-type": "application/json"};
    String json = '{ "userID": "", "sessionID": "" }';
    http.Response response = await http.post(url, headers: headers, body: json);
    int statusCode = response.statusCode;
    if (statusCode == 200) {
      String body = response.body;
      // print(body);
      // var data = jsonDecode(body);
      var data = jsonDecode(utf8.decode(response.bodyBytes));
      setState(() {
        locationList = data["data"];
      });
      // print("1");
      // print(locationList);
    } else {
      print("Connection Fail");
    }
  }

  Widget build(BuildContext context) {
    void _launchMapsUrl(String lat, String lon) async {
      final url = 'https://www.google.com/maps/search/?api=1&query=$lat,$lon';
      if (await canLaunch(url)) {
        await launch(url);
      } else {
        throw 'Could not launch $url';
      }
    }

    Future<Map<String, double>> _getLocation() async {
      var currentLocation = <String, double>{};
      try {
        currentLocation = (await location.getLocation()) as Map<String, double>;
      } catch (e) {
        currentLocation = null;
      }
      return currentLocation;
    }

    for (var dataLoop = 0; dataLoop < locationList.length; dataLoop++) {
      if (locationList[dataLoop]["locationType"] == "Agent") {
        agentDataList = locationList[dataLoop]["dataList"];
        for (var a = 0; a < agentDataList.length; a++) {
          agentList.add(agentDataList[a]["name"]);
        }
        setState(() {});
      }
    }

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        // backgroundColor: Colors.white,
        appBar: AppBar(
          leading: new IconButton(
              icon: new Icon(Icons.arrow_back),
              // color: Colors.indigo,
              disabledColor: Colors.indigo,
              onPressed: () => Navigator.of(context).pop()),
          // backgroundColor: Colors.white,
          backgroundColor: Color.fromRGBO(40, 103, 178, 1),
          title: (checklang == "Eng")
              ? Text(
                  (checklang == "Eng") ? textEng[0] : textMyan[0],
                  style: TextStyle(fontSize: 20, color: Colors.indigo),
                )
              : Text(
                  (checklang == "Eng") ? textEng[0] : textMyan[0],
                  style: TextStyle(fontSize: 18, color: Colors.indigo),
                ),
          // title: Text(
          //   "Agent",
          //   style: TextStyle(
          //     color: Colors.indigo,
          //   ),
          // ),
          centerTitle: true,
          actions: <Widget>[
            new IconButton(
                icon: Image.asset("assets/images/mapicon.jpg"),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => LocationDetail()),
                  );
                }),
          ],
        ),
        body: new Container(
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Card(
                  elevation: 5,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.only(left: 20.0),
                    child: new TextField(
                      onChanged: onSearchResult,
                      controller: controller,
                      decoration: InputDecoration(
                        icon: new Icon(Icons.search),
                        hintText: 'Search',
                        focusedBorder: InputBorder.none,
                        border: InputBorder.none,
                      ),
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: (controller.text.isEmpty || searchList.length == null)
                      ? ListView.builder(
                          itemCount: agentDataList.length,
                          itemBuilder: (BuildContext ctxt, int index) {
                            return Column(
                              children: <Widget>[
                                ListTile(
                                  contentPadding: EdgeInsets.symmetric(
                                      horizontal: 20.0, vertical: 10.0),
                                  title: Text(
                                    agentDataList[index]["name"],
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  subtitle: Text(
                                    "\nTelephone : " +
                                        agentDataList[index]["phone1"] +
                                        "\n\nAddress : " +
                                        agentDataList[index]["address"],
                                    style: TextStyle(color: Colors.black),
                                  ),
                                  trailing: IconButton(
                                    icon:
                                        Icon(Icons.phone, color: Colors.indigo),
                                    onPressed: () {
                                      launch('tel:' +
                                          agentDataList[index]["phone1"]);
                                    },
                                  ),
                                  onTap: () {
                                    // _launchMapsUrl(agentDataList[index]["latitude"],agentDataList[index]["longitude"]);
                                    var route = new MaterialPageRoute(
                                        builder: (BuildContext context) =>
                                            new BranchLocation(
                                              value1: agentDataList[index]
                                                  ["name"],
                                              value2: agentDataList[index]
                                                  ["phone1"],
                                              value3: agentDataList[index]
                                                  ["address"],
                                            ));
                                    Navigator.of(context).push(route);
                                    print(Text("AgentBranch"));
                                  },
                                ),
                                Divider(
                                  height: 2,
                                )
                              ],
                            );
                          })
                      : ListView.builder(
                          itemCount: searchList.length,
                          itemBuilder: (BuildContext ctxt, int index) {
                            return Column(
                              children: <Widget>[
                                ListTile(
                                  contentPadding: EdgeInsets.symmetric(
                                      horizontal: 20.0, vertical: 10.0),
                                  title: Text(
                                    searchList[index]["name"],
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  subtitle: Text(
                                    "\nTelephone : " +
                                        searchList[index]["phone1"] +
                                        "\n\nAddress : " +
                                        searchList[index]["address"],
                                    style: TextStyle(color: Colors.black),
                                  ),
                                  trailing: IconButton(
                                    icon:
                                        Icon(Icons.phone, color: Colors.indigo),
                                    onPressed: () {
                                      launch(
                                          'tel:' + searchList[index]["phone1"]);
                                    },
                                  ),
                                  onTap: () {
                                    // _launchMapsUrl(agentDataList[index]["latitude"],agentDataList[index]["longitude"]);
                                    var route = new MaterialPageRoute(
                                        builder: (BuildContext context) =>
                                            new BranchLocation(
                                              value1: searchList[index]["name"],
                                              value2: searchList[index]
                                                  ["phone1"],
                                              value3: searchList[index]
                                                  ["address"],
                                            ));
                                    Navigator.of(context).push(route);
                                    print(Text("AgentBranch"));
                                  },
                                ),
                                Divider(
                                  height: 2,
                                )
                              ],
                            );
                          }),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  onSearchResult(String text) async {
    searchList.clear();
    // text = text.toLowerCase();
    if (text.isNotEmpty) {
      for (var a = 0; a < agentDataList.length; a++) {
        if (agentDataList[a]["name"].contains(text)) {
          searchList.add(agentDataList[a]);
          print("sarchlist ==>  $searchList");
        }
      }
      setState(() {});
    }
  }
}
