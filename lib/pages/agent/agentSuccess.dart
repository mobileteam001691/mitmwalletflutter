import 'package:flutter/material.dart';
import 'package:nsb/constants/rout_path.dart' as routes;
import 'package:nsb/pages/Wallet.dart';

class AgentSuccessPage extends StatefulWidget {
  final String value;
  final String value1;
  final String value2;
  final String value3;

  AgentSuccessPage({Key key, this.value,this.value1,this.value2,this.value3}) : super(key: key);

  @override
  _AgentSuccessPageState createState() => _AgentSuccessPageState();
}

class _AgentSuccessPageState extends State<AgentSuccessPage> {
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: new AppBar(//Application Bar
        elevation: 0.0,
        backgroundColor:Color.fromRGBO(40, 103, 178, 1),
        title: Center(child: Text('Transfer Success', style: TextStyle(
        fontSize: 20.0,
        color: Colors.white,
        height: 1.0,
        fontWeight: FontWeight.w600),),)),
        body: new Form(
          child: new ListView(
            children: <Widget>[
                      Center(
                        child: new Container(
                          padding: EdgeInsets.only(left: 30.0, right: 30.0),
                        ),
                      ),
                      SizedBox(height: 10.0),
                      Center(
                        child: new Image.asset(
                          'assets/images/correct6.png',
                          width: 80.0,
                          height: 80.0,
                        ),
                      ),
                      SizedBox(height: 30.0),
                      Center(
                          child: Text("Success",
                              style: TextStyle(
                                  fontSize: 15.0,
                                  fontWeight: FontWeight.bold))),
                      SizedBox(height: 30.0),    
                      Center(
                          child: Text("${widget.value}"+".00 MMK",
                              style: TextStyle(
                                  fontSize: 25.0,
                                  fontWeight: FontWeight.bold))),
                      SizedBox(height: 40.0),    
                      Center(
                        child: Card(
                           shape: RoundedRectangleBorder(
                           borderRadius: BorderRadius.circular(20.0),),
                           elevation: 3.0,
                           child: Column(
                             children: <Widget>[
                               SizedBox(height: 20.0,width: 170.0,),
                               new Row(
                                 children: <Widget>[
                                   Padding(
                                     padding: const EdgeInsets.fromLTRB(20.0, 10.0, 10.0, 0.0),
                                     child: new Text("Date",style: TextStyle(
                                  fontSize: 15.0,)),
                                   ),
                                   Padding(
                                     padding: const EdgeInsets.fromLTRB(200.0, 10.0, 20.0, 0.0),
                                     child: new Text("${widget.value2}",style: TextStyle(
                                  fontSize: 18.0,
                                  fontWeight: FontWeight.bold)),
                                   ),
                                 ],
                               ),
                               SizedBox(height: 40.0,width: 170.0,),
                               new Row(
                                 children: <Widget>[
                                   Padding(
                                     padding: const EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 0.0),
                                     child: new Text("Transaction No",style: TextStyle(
                                  fontSize: 15.0,)),
                                   ),
                                   Padding(
                                     padding: const EdgeInsets.fromLTRB(170.0, 10.0, 20.0, 0.0),
                                     child: new Text("${widget.value1}",style: TextStyle(
                                  fontSize: 18.0,
                                  fontWeight: FontWeight.bold)),
                                   ),
                                 ],
                               ),
                               SizedBox(height: 40.0,width: 170.0,),
                               new Row(
                                 children: <Widget>[
                                   Padding(
                                     padding: const EdgeInsets.fromLTRB(20.0, 10.0, 10.0, 0.0),
                                     child: new Text("Transfer To",style: TextStyle(
                                  fontSize: 15.0,)),
                                   ),
                                   Padding(
                                     padding: const EdgeInsets.fromLTRB(140.0, 10.0, 20.0, 0.0),
                                     child: new Text("${widget.value3}",style: TextStyle(
                                  fontSize: 18.0,
                                  fontWeight: FontWeight.bold)),
                                   ),
                                 ],
                               ),
                               SizedBox(height: 40.0,width: 170.0),
                             ],
                           ),
                        ),
                      ),
                       SizedBox(height: 30.0),    
                      Center(
                        child: Container(
                          width: 300,
                          height: 50,
                          child: RaisedButton(
                            color:Color.fromRGBO(40, 103, 178, 1),
                            elevation: 5.0,
                            onPressed: (){
                                Navigator.push(context,MaterialPageRoute(builder: (context) => WalletPage()),
                               );
                            },
                            child: new Text("Close",style: TextStyle(fontSize: 18),),
                            textColor: Colors.white,
                          ),
                        ),
                         ),
            ],
          ),
        ),
    );
  }
}
