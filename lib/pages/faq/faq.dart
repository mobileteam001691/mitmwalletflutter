import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import 'package:nsb/pages/faq/faqresponse.dart';
import 'package:shared_preferences/shared_preferences.dart';

class FAQ extends StatefulWidget {
  @override
  _FAQState createState() => _FAQState();
}

class _FAQState extends State<FAQ> {
  final String url =
      'http://122.248.120.16:8080/WalletService/module001/service002/getFAQList';
  List data = new List();
  int i;
  bool isLoading = true;
  String checklang = '';
  List faqQueEngList = [];
  List faqQueUniList = [];
  List faqAnsEngList = [];
  List faqAnsUniList = [];
  List textMyan = [
    "မေးလေ့ရှိသောမေးခွန်းများ"
  ];
  List textEng = [
    "FAQ"
  ];

  void initState() {
    super.initState();
    this.getJsonData();
    checkLanguage();
    this.faqData();
  }

  checkLanguage() async {
    final prefs = await SharedPreferences.getInstance();
    checklang = prefs.getString("Lang");
    if (checklang == "" || checklang == null || checklang.length == 0) {
      checklang = "Eng";
    } else {
      checklang = checklang;
    }
    setState(() {});
  }

   faqData() async {
    final url =
        "http://122.248.120.16:8080/WalletService/module001/service002/getFAQList";
    http.get(Uri.encodeFull(url), headers: {
      "Accept": "application/json",
      "content-type": "application/json"
    }).then((dynamic res) {
      var result = json.decode(utf8.decode(res.bodyBytes));
      print("result ===> $result");
      if (result['code'] == "0000") {
        var data = result['faqData'];
        for (var i = 0; i < data.length; i++) {
          faqQueEngList.add(data[i]['questionEng'].toString());
          faqAnsEngList.add(data[i]['answerEng'].toString());
          faqQueUniList.add(data[i]['questionUni'].toString());
          faqAnsUniList.add(data[i]['answerUni'].toString());
        }
      }
    });
  }

  FaqResponse p = new FaqResponse();

  Future<List<FaqResponse>> getJsonData() async {
    var response = await http
        .get(Uri.encodeFull(url), headers: {"Accept": "application/json"});

    setState(() {
      p = FaqResponse.fromJson(json.decode(utf8.decode(response.bodyBytes)));

      data = p.faqData;
      if (p.code == "0000") {
        isLoading = false;
      } else {
        isLoading = true;
      }
    });
    
  }

  @override
  Widget build(BuildContext context) {
    var body = new ListView.builder(
      itemCount: data.length,
      itemBuilder: (BuildContext contxt, int index) {
        i = index + 1;
        return new ExpansionTile(
          title: (checklang == "Eng") ? new Text('$i. ' + '${data[index].questionEng}') :new Text('$i. ' + '${data[index].questionUni}'),
          children: <Widget>[
            new ListTile(
             title: (checklang == "Eng") ?  new Text('${data[index].answerEng}') : new Text('${data[index].answerUni}'),
            ),
          ],
        );
      },
    );
    var bodyProgress = new Container(
      child: new Stack(
        children: <Widget>[
          body,
          Container(
              decoration: new BoxDecoration(
                color: Color.fromRGBO(255, 255, 255, 0.5),
              ),
              width: MediaQuery.of(context).size.width * 0.99,
              height: MediaQuery.of(context).size.height * 0.9,
              child: Center(
                child: CircularProgressIndicator(
                  backgroundColor: Colors.amber,
                ),
              ))
        ],
      ),
    );

    return Scaffold(
        appBar: AppBar(
          title: Padding(
            padding: const EdgeInsets.only(right: 35.0),
            child: Center(
              child: (checklang == "Eng") ? new Text(textEng[0]) : new Text(textMyan[0]),
            ),
          ),
        ),
        body: isLoading ? bodyProgress : body
        );
  }
}
