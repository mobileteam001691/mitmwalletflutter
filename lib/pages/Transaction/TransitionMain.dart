// import 'dart:convert';

// import 'package:flutter/material.dart';
// import 'package:intl/intl.dart';
// import 'package:nsb/constants/constant.dart';
// import 'package:nsb/main.dart';
// import 'package:nsb/model/TransitionListRequest.dart';
// import 'package:nsb/model/TransitionListResponse.dart';
// import 'package:nsb/pages/Transaction/Details.dart';
// import 'package:shared_preferences/shared_preferences.dart';
// import 'package:nsb/framework/http_service.dart' as http;
// import 'package:http/http.dart' as http;

// class TransitionMain extends StatefulWidget {
//   @override
//   _TransitionMainState createState() => _TransitionMainState();
// }

// class _TransitionMainState extends State<TransitionMain> {
//   TextEditingController startCon = TextEditingController();
//   TextEditingController endCon = TextEditingController();
//   var endDate;
//   var startDate;

//   static final CREATE_POST_URL =
//       'http://122.248.120.16:8080/WalletService/module001';
//   var balance = 0.0;
//   var locationList = [];
//   var displayArray = [];
//   List paymentList = [];
//   List receiveList = [];
//   List dateList = [];
//   List nameList = [];
//   List dateDifference = [];
//   var datas = [], q;
//   var duration = 0;
//   var pageC;
//   var currPage;
//   var pageSize;
//   var finaldate;
//   var date;
//   var selected;
//   var toDate;
//   var fromDate;
//   @override
//   void initState() {
//     super.initState();
//     pageSize = 10;
//     currPage = 1;
//     pageC = 0;
//     this.getTransactionList(duration);
//   }

//   getTransactionList(value) async {
//     final prefs = await SharedPreferences.getInstance();
//     String userID = prefs.getString('userId');
//     String sessionID = prefs.getString('sessionID');
//     String accountNo = prefs.getString('accountNo');

//     TransitionListRequest transitionListRequest = new TransitionListRequest(
//       userID: userID,
//       sessionID: sessionID,
//       customerNo: "",
//       durationType: value,
//       fromDate: startDate,
//       toDate: endDate,
//       totalCount: 0,
//       acctNo: accountNo,
//       currentPage: 2,
//       pageSize: 10,
//       pageCount: 0,
//     );

//     TransitionListResponse transitionListResponse = await getAllList1(
//         CREATE_POST_URL + '/service001/getTransactionActivityList',
//         transitionListRequest.toMap());
//     for (var a = 0; a < locationList.length; a++) {
//       if (locationList[a]["drcr"] == "1") {
//         paymentList.add(locationList[a]);
//         print(paymentList);
//       } else if (locationList[a]["drcr"] == "2") {
//         receiveList.add(locationList[a]);
//         print(receiveList);
//       }
//     }
//     if (transitionListResponse.code == '0000') {
//       print(locationList);

//       print('Get Everything.............');
//     }
//   }

//   callDatePicker() async {
//     var order = await getDate();
//     setState(() {
//       finaldate = order;
//     });
//   }

//   Future<DateTime> getDate() {
//     // Imagine that this function is
//     // more complex and slow.
//     return showDatePicker(
//       context: context,
//       initialDate: DateTime.now(),
//       firstDate: DateTime(2018),
//       lastDate: DateTime(2030),
//       builder: (BuildContext context, Widget child) {
//         return Theme(
//           data: ThemeData.light(),
//           child: child,
//         );
//       },
//     );
//   }

//   Future<TransitionListResponse> getAllList1(url, Map jsonMap) async {
//     TransitionListResponse p = new TransitionListResponse();
//     var body;
//     try {
//       body = await http.doPost(url, jsonMap);
//       p = TransitionListResponse.fromJson(json.decode(body.toString()));
//       var data = jsonDecode(body);
//       setState(() {
//         locationList = data['data'];
//       });
//     } catch (e) {
//       p.code = Constants.responseCode_Error;
//       p.desc = e.toString();
//     }
//     print(p.toMap());
//     return p;
//   }

//   String _currText = "";

//   _onSelect(duration) {
//     print("duration ==> $duration");
//   }

//   @override
//   Widget build(BuildContext context) {
//     var a = new Column(
//       children: <Widget>[
//         Container(
//           child: Card(
//             color: Colors.grey,
//             elevation: 1.0,
//             child: Text(
//               "06-02-2020",
//               style: TextStyle(
//                 fontSize: 25.0,
//                 height: 2.0,
//               ),
//             ),
//           ),
//         )
//       ],
//     );

//     var all = new ListView.separated(
//         separatorBuilder: (context, index) => Divider(
//               color: Colors.black,
//             ),
//         itemCount: dateDifference.length,
//         itemBuilder: (BuildContext context, int index) {
//           this.displayArray =
//               this.locationList[index]["txnTypeDesc"].split(',');
//           var aa = this.displayArray[1];
//           nameList.add(aa);
//           String dateString = locationList[index]["txnDate"];
//           String d = dateString.substring(0, 4);
//           String e = dateString.substring(5, 7);
//           String f = dateString.substring(8, 10);
//           String g = d + e + f;
//           var date = new DateFormat.yMMMd().format(DateTime.parse(g));
//           datas.add(date);
//           print("data ==> $datas");
//           print("name  ==> $displayArray");
//           return new Material(
//             child: Column(children: [
//               Container(
//                 width: double.infinity,
//                 child: Text(
//                   locationList[index]["txnDate"],
//                   style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
//                 ),
//                 decoration: BoxDecoration(
//                   color: Color(0xffE4E4E4),
//                 ),
//                 padding: EdgeInsets.all(10),
//               ),
//               Column(
//                 children: [
//                   Container(
//                     padding: const EdgeInsets.all(17.0),
//                     child: new GestureDetector(
//                       onTap: () {
//                         Navigator.push(
//                           context,
//                           MaterialPageRoute(
//                             builder: (context) => DetailsPage(
//                               name: nameList[index],
//                               type: locationList[index]["remark"],
//                               amount: locationList[index]["txnAmount"],
//                               refNo: locationList[index]["txtReferenceNo"],
//                               date: datas[index],
//                               phNo: displayArray[0],
//                             ),
//                           ),
//                         );
//                       },
//                       child: Row(children: <Widget>[
//                         Column(
//                           crossAxisAlignment: CrossAxisAlignment.start,
//                           children: <Widget>[
//                             Text(
//                               nameList[index],
//                               style: TextStyle(
//                                   fontSize: 16, fontWeight: FontWeight.bold),
//                             ),
//                             Divider(height: 10),
//                             Text(
//                               locationList[index]["remark"],
//                               style: TextStyle(
//                                   fontSize: 16, fontWeight: FontWeight.w300),
//                             ),
//                           ],
//                         ),
//                         Spacer(),
//                         Column(
//                           crossAxisAlignment: CrossAxisAlignment.end,
//                           children: <Widget>[
//                             (locationList[index]['drcr'] == "1")
//                                 ? Text(
//                                     locationList[index]["txnAmount"] + " MMK",
//                                     style: TextStyle(
//                                         fontSize: 16,
//                                         fontWeight: FontWeight.bold,
//                                         color: Colors.red),
//                                   )
//                                 : Text(
//                                     locationList[index]["txnAmount"] + " MMK",
//                                     style: TextStyle(
//                                         fontSize: 16,
//                                         fontWeight: FontWeight.bold,
//                                         color: Colors.blue),
//                                   ),
//                             Divider(height: 10),
//                             Text(
//                               datas[index],
//                               style: TextStyle(
//                                   fontSize: 15, fontWeight: FontWeight.w300),
//                             ),
//                           ],
//                         ),
//                       ]),
//                     ),
//                   ),
//                 ],
//               ),
//             ]),
//           );
//         });
//     new ListView.separated(
//         separatorBuilder: (context, index) => Divider(
//               color: Colors.black,
//             ),
//         itemCount: locationList.length,
//         itemBuilder: (BuildContext context, int index) {
//           this.displayArray =
//               this.locationList[index]["txnTypeDesc"].split(',');
//           var aa = this.displayArray[1];
//           nameList.add(aa);
//           String dateString = locationList[index]["txnDate"];
//           String d = dateString.substring(0, 4);
//           String e = dateString.substring(5, 7);
//           String f = dateString.substring(8, 10);
//           String g = d + e + f;
//           var date = new DateFormat.yMMMd().format(DateTime.parse(g));
//           datas.add(date);
//           print("data ==> $datas");
//           print("name  ==> $displayArray");
//           return new Material(
//             child: Column(children: [
//               Container(
//                 width: double.infinity,
//                 child: Text(
//                   locationList[index]["txnDate"],
//                   style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
//                 ),
//                 decoration: BoxDecoration(
//                   color: Color(0xffE4E4E4),
//                 ),
//                 padding: EdgeInsets.all(10),
//               ),
//               Column(
//                 children: [
//                   Container(
//                     padding: const EdgeInsets.all(17.0),
//                     child: new GestureDetector(
//                       onTap: () {
//                         Navigator.push(
//                           context,
//                           MaterialPageRoute(
//                             builder: (context) => DetailsPage(
//                               name: nameList[index],
//                               type: locationList[index]["remark"],
//                               amount: locationList[index]["txnAmount"],
//                               refNo: locationList[index]["txtReferenceNo"],
//                               date: datas[index],
//                               phNo: displayArray[0],
//                             ),
//                           ),
//                         );
//                       },
//                       child: Row(children: <Widget>[
//                         Column(
//                           crossAxisAlignment: CrossAxisAlignment.start,
//                           children: <Widget>[
//                             Text(
//                               nameList[index],
//                               style: TextStyle(
//                                   fontSize: 16, fontWeight: FontWeight.bold),
//                             ),
//                             Divider(height: 10),
//                             Text(
//                               locationList[index]["remark"],
//                               style: TextStyle(
//                                   fontSize: 16, fontWeight: FontWeight.w300),
//                             ),
//                           ],
//                         ),
//                         Spacer(),
//                         Column(
//                           crossAxisAlignment: CrossAxisAlignment.end,
//                           children: <Widget>[
//                             (locationList[index]['drcr'] == "1")
//                                 ? Text(
//                                     locationList[index]["txnAmount"] + " MMK",
//                                     style: TextStyle(
//                                         fontSize: 16,
//                                         fontWeight: FontWeight.bold,
//                                         color: Colors.red),
//                                   )
//                                 : Text(
//                                     locationList[index]["txnAmount"] + " MMK",
//                                     style: TextStyle(
//                                         fontSize: 16,
//                                         fontWeight: FontWeight.bold,
//                                         color: Colors.blue),
//                                   ),
//                             Divider(height: 10),
//                             Text(
//                               datas[index],
//                               style: TextStyle(
//                                   fontSize: 15, fontWeight: FontWeight.w300),
//                             ),
//                           ],
//                         ),
//                       ]),
//                     ),
//                   ),
//                 ],
//               ),
//             ]),
//           );
//         });

//     var payment = new ListView.separated(
//         separatorBuilder: (context, index) => Divider(
//               color: Colors.black,
//             ),
//         itemCount: paymentList.length,
//         itemBuilder: (BuildContext context, int index) {
//           this.displayArray = this.paymentList[index]["txnTypeDesc"].split(',');
//           var name = this.displayArray[1];
//           nameList.add(name);
//           String dateString = paymentList[index]["txnDate"];
//           String d = dateString.substring(0, 4);
//           String e = dateString.substring(5, 7);
//           String f = dateString.substring(8, 10);
//           String g = d + e + f;
//           var date = new DateFormat.yMMMd().format(DateTime.parse(g));
//           datas.add(date);

//           return new Material(
//             child: Container(
//               padding: const EdgeInsets.all(17.0),
//               child: new GestureDetector(
//                 onTap: () {
//                   Navigator.push(
//                     context,
//                     MaterialPageRoute(
//                       builder: (context) => DetailsPage(
//                         name: nameList[index],
//                         type: paymentList[index]["remark"],
//                         amount: paymentList[index]["txnAmount"],
//                         refNo: paymentList[index]["txtReferenceNo"],
//                         date: datas[index],
//                         phNo: displayArray[0],
//                       ),
//                     ),
//                   );
//                 },
//                 child: Row(children: <Widget>[
//                   Column(
//                     crossAxisAlignment: CrossAxisAlignment.start,
//                     children: <Widget>[
//                       Text(
//                         nameList[index],
//                         style: TextStyle(
//                             fontSize: 16, fontWeight: FontWeight.bold),
//                       ),
//                       Divider(height: 10),
//                       Text(
//                         paymentList[index]["remark"],
//                         style: TextStyle(
//                             fontSize: 16, fontWeight: FontWeight.w300),
//                       ),
//                     ],
//                   ),
//                   Spacer(),
//                   Column(
//                     crossAxisAlignment: CrossAxisAlignment.end,
//                     children: <Widget>[
//                       Text(
//                         paymentList[index]["txnAmount"] + " MMK",
//                         style: TextStyle(
//                             fontSize: 16,
//                             fontWeight: FontWeight.bold,
//                             color: Colors.red),
//                       ),
//                       Divider(height: 10),
//                       Text(
//                         datas[index],
//                         style: TextStyle(
//                             fontSize: 15, fontWeight: FontWeight.w300),
//                       ),
//                     ],
//                   ),
//                 ]),
//               ),
//             ),
//           );
//         });
//     var receive = new ListView.separated(
//         separatorBuilder: (context, index) => Divider(
//               color: Colors.black,
//             ),
//         itemCount: receiveList.length,
//         itemBuilder: (BuildContext context, int index) {
//           this.displayArray = this.receiveList[index]["txnTypeDesc"].split(',');
//           var name = this.displayArray[1];
//           nameList.add(name);
//           String dateString = receiveList[index]["txnDate"];
//           String d = dateString.substring(0, 4);
//           String e = dateString.substring(5, 7);
//           String f = dateString.substring(8, 10);
//           String g = d + e + f;
//           var date = new DateFormat.yMMMd().format(DateTime.parse(g));
//           datas.add(date);
//           return new Material(
//             child: Container(
//               padding: const EdgeInsets.all(17.0),
//               child: new GestureDetector(
//                 onTap: () {
//                   Navigator.push(
//                     context,
//                     MaterialPageRoute(
//                       builder: (context) => DetailsPage(
//                         name: nameList[index],
//                         type: receiveList[index]["remark"],
//                         amount: receiveList[index]["txnAmount"],
//                         refNo: receiveList[index]["txtReferenceNo"],
//                         date: datas[index],
//                         phNo: displayArray[0],
//                       ),
//                     ),
//                   );
//                 },
//                 child: Row(children: <Widget>[
//                   Column(
//                     crossAxisAlignment: CrossAxisAlignment.start,
//                     children: <Widget>[
//                       Text(
//                         nameList[index],
//                         style: TextStyle(
//                             fontSize: 16, fontWeight: FontWeight.bold),
//                       ),
//                       Divider(height: 10),
//                       Text(
//                         receiveList[index]["remark"],
//                         style: TextStyle(
//                             fontSize: 16, fontWeight: FontWeight.w300),
//                       ),
//                     ],
//                   ),
//                   Spacer(),
//                   Column(
//                     crossAxisAlignment: CrossAxisAlignment.end,
//                     children: <Widget>[
//                       Text(
//                         receiveList[index]["txnAmount"] + " MMK",
//                         style: TextStyle(
//                             fontSize: 16,
//                             fontWeight: FontWeight.bold,
//                             color: Colors.blue),
//                       ),
//                       Divider(height: 10),
//                       Text(
//                         datas[index],
//                         style: TextStyle(
//                             fontSize: 15, fontWeight: FontWeight.w300),
//                       ),
//                     ],
//                   ),
//                 ]),
//               ),
//             ),
//           );
//         });

//     var custom = new ListView.separated(
//         separatorBuilder: (context, index) => Divider(
//               color: Colors.black,
//             ),
//         itemCount: dateDifference.length,
//         itemBuilder: (BuildContext context, int index) {
//           this.displayArray = this.receiveList[index]["txnTypeDesc"].split(',');
//           var name = this.displayArray[1];
//           nameList.add(name);
//           String dateString = receiveList[index]["txnDate"];
//           String d = dateString.substring(0, 4);
//           String e = dateString.substring(5, 7);
//           String f = dateString.substring(8, 10);
//           String g = d + e + f;
//           var date = new DateFormat.yMMMd().format(DateTime.parse(g));
//           datas.add(date);
//           return new Material(
//             child: Container(
//               padding: const EdgeInsets.all(17.0),
//               child: new GestureDetector(
//                 onTap: () {
//                   Navigator.push(
//                     context,
//                     MaterialPageRoute(
//                       builder: (context) => DetailsPage(
//                         name: nameList[index],
//                         type: receiveList[index]["remark"],
//                         amount: receiveList[index]["txnAmount"],
//                         refNo: receiveList[index]["txtReferenceNo"],
//                         date: datas[index],
//                         phNo: displayArray[0],
//                       ),
//                     ),
//                   );
//                 },
//                 child: Row(children: <Widget>[
//                   Column(
//                     crossAxisAlignment: CrossAxisAlignment.start,
//                     children: <Widget>[
//                       Text(
//                         nameList[index],
//                         style: TextStyle(
//                             fontSize: 16, fontWeight: FontWeight.bold),
//                       ),
//                       Divider(height: 10),
//                       Text(
//                         receiveList[index]["remark"],
//                         style: TextStyle(
//                             fontSize: 16, fontWeight: FontWeight.w300),
//                       ),
//                     ],
//                   ),
//                   Spacer(),
//                   Column(
//                     crossAxisAlignment: CrossAxisAlignment.end,
//                     children: <Widget>[
//                       Text(
//                         receiveList[index]["txnAmount"] + " MMK",
//                         style: TextStyle(
//                             fontSize: 16,
//                             fontWeight: FontWeight.bold,
//                             color: Colors.blue),
//                       ),
//                       Divider(height: 10),
//                       Text(
//                         datas[index],
//                         style: TextStyle(
//                             fontSize: 15, fontWeight: FontWeight.w300),
//                       ),
//                     ],
//                   ),
//                 ]),
//               ),
//             ),
//           );
//         });

//     return MaterialApp(
//       debugShowCheckedModeBanner: false,
//       home: DefaultTabController(
//         length: 3,
//         child: Scaffold(
//           appBar: AppBar(
//             backgroundColor: Colors.white,
//             centerTitle: true,
//             title: Text(
//               'Transactions',
//               style: TextStyle(
//                 fontSize: 22,
//                 color: Color.fromRGBO(40, 103, 178, 1),
//               ),
//             ),
//             leading: IconButton(
//               icon: Icon(
//                 Icons.arrow_back,
//                 color: Colors.black,
//               ),
//               onPressed: () {
//                 Navigator.pop(context);
//               },
//             ),
//             actions: <Widget>[
//               PopupMenuButton<String>(
//                   onSelected: (String value) {
//                     setState(() {
//                       selected = value;
//                       print("selected ==> $selected");
//                       _choiceAction(selected);
//                     });
//                   },
//                   color: Colors.white,
//                   child: Padding(
//                     padding: EdgeInsets.all(14),
//                     child: Icon(
//                       Icons.arrow_drop_down,
//                       color: Colors.black,
//                       size: 30.0,
//                     ),
//                   ),
//                   itemBuilder: (BuildContext context) {
//                     return [
//                       PopupMenuItem(
//                         value: "default",
//                         child: GestureDetector(
//                           child: Text("Last 10 Transaction",
//                               style: TextStyle(color: Colors.blue)),
//                         ),
//                       ),
//                       PopupMenuItem(
//                         value: "towD",
//                         child: GestureDetector(
//                           child: Text("2 Days",
//                               style: TextStyle(color: Colors.blue)),
//                         ),
//                       ),
//                       PopupMenuItem(
//                         value: "fiveD",
//                         child: GestureDetector(
//                           onTap: () {
//                             _choiceAction('fiveD');
//                           },
//                           child: Text("5 Days",
//                               style: TextStyle(color: Colors.blue)),
//                         ),
//                       ),
//                       PopupMenuItem(
//                         value: "custom",
//                         child: GestureDetector(
//                           child: Text("Custom",
//                               style: TextStyle(color: Colors.blue)),
//                         ),
//                       ),
//                     ];
//                   }),
//             ],
//             bottom: (duration == 3)
//                 ? PreferredSize(
//                     preferredSize: const Size.fromHeight(120.0),
//                     child: Padding(
//                       padding:
//                           EdgeInsets.only(left: 5, right: 5, top: 0, bottom: 0),
//                       child: Column(
//                         children: <Widget>[
//                           Row(children: [
//                             new Flexible(
//                               child: Padding(
//                                 padding: EdgeInsets.all(5),
//                                 child: TextField(
//                                   controller: startCon,
//                                   keyboardType: TextInputType.datetime,
//                                   onTap: () {
//                                     FocusScope.of(context)
//                                         .requestFocus(new FocusNode());
//                                     getStartDate();
//                                   },
//                                   decoration: InputDecoration(
//                                     labelText: "From Date",
//                                   ),
//                                 ),
//                               ),
//                             ),
//                             new Flexible(
//                               child: Padding(
//                                 padding: EdgeInsets.all(10),
//                                 child: TextField(
//                                   controller: endCon,
//                                   keyboardType: TextInputType.datetime,
//                                   onTap: () {
//                                     FocusScope.of(context)
//                                         .requestFocus(new FocusNode());
//                                     getEndDate();
//                                   },
//                                   decoration:
//                                       InputDecoration(labelText: "To Date"),
//                                 ),
//                               ),
//                             ),
//                           ]),
//                           Row(
//                               mainAxisAlignment: MainAxisAlignment.end,
//                               children: [
//                                 Padding(
//                                   padding:
//                                       EdgeInsets.only(bottom: 10, right: 10),
//                                   child: RaisedButton(
//                                       color: Color.fromRGBO(40, 103, 178, 1),
//                                       child: Text(
//                                         "Search",
//                                         style: TextStyle(color: Colors.white),
//                                       ),
//                                       onPressed: () async {
//                                         // if (startDate > endDate) {
//                                         //   print("Impossible ...");
//                                         // } else {
//                                           searchDatawithDate();
//                                           // print(startDate);
//                                         // }
//                                       }),
//                                 ),
//                               ]),
//                         ],
//                       ),
//                     ),
//                   )
//                 : TabBar(
//                     unselectedLabelColor: Colors.blue,
//                     indicatorSize: TabBarIndicatorSize.label,
//                     tabs: [
//                       Tab(
//                         child: Container(
//                           child: Align(
//                             alignment: Alignment.center,
//                             child: Text(
//                               "ALL",
//                               style: TextStyle(color: Colors.blue),
//                             ),
//                           ),
//                         ),
//                       ),
//                       Tab(
//                         child: Container(
//                           child: Align(
//                             alignment: Alignment.center,
//                             child: Text(
//                               "PAYMENT",
//                               style: TextStyle(color: Colors.blue),
//                             ),
//                           ),
//                         ),
//                       ),
//                       Tab(
//                         child: Container(
//                           child: Align(
//                             alignment: Alignment.center,
//                             child: Text(
//                               "RECEIVE",
//                               style: TextStyle(color: Colors.blue),
//                             ),
//                           ),
//                         ),
//                       ),
//                     ],
//                   ),
//           ),
//           body: TabBarView(children: <Widget>[all, payment, receive]),
//         ),
//       ),
//     );
//   }

//   searchDate() {
//     DateTime diff = endDate.difference(startDate);
//     DateTime queryDate2 = DateTime.utc(diff.year, diff.month, diff.day);
//     print("==> $queryDate2");
//   }

//   searchDatawithDate() async {
//     final prefs = await SharedPreferences.getInstance();
//     String userID = prefs.getString('userId');
//     String sessionID = prefs.getString('sessionID');
//     String accountNo = prefs.getString('accountNo');
//     String url =
//         "http://122.248.120.16:8080/WalletService/module001/service001/getTransactionActivityList";
//     Map<String, String> headers = {"Content-type": "application/json"};

//     String json = '{ "userID": "' +
//         userID +
//         '", "sessionID":"' +
//         sessionID +
//         '" , "customerNo":"' +
//         "" +
//         '", "durationType":"' +
//         "$duration" +
//         '", "fromDate":"' +
//         toDate +
//         '", "toDate":"' +
//         fromDate +
//         '", "totalCount":"' +
//         "0" +
//         '", "acctNo":"' +
//         accountNo +
//         '", "currentPage":"' +
//         "1" +
//         '", "pageSize":"' +
//         "10" +
//         '", "pageCount":"' +
//         "0" +
//         '"}';
//     http.Response response = await http.post(url, headers: headers, body: json);
//     int statusCode = response.statusCode;
//     if (statusCode == 200) {
//       String body = response.body;
//       print(body);
//       var data = jsonDecode(body);
//       print(data);
//       setState(() {});
//     } else {
//       print("Connection Fail");
//     }
//   }

//   _choiceAction(selected) {
//     print("object ===> $selected");
//     if (selected == "default") {
//       duration = 0;
//       locationList = [];
//       paymentList = [];
//       receiveList = [];
//       print("duration ==> $duration");
//       getTransactionList(duration);
//     } else if (selected == "twoD") {
//       duration = 1;
//       locationList = [];
//       paymentList = [];
//       receiveList = [];
//       print("duration ==> $duration");
//       getTransactionList(duration);
//     } else if (selected == "fiveD") {
//       duration = 2;
//       locationList = [];
//       paymentList = [];
//       receiveList = [];
//       print("duration ==> $duration");
//       getTransactionList(duration);
//     } else if (selected == "custom") {
//       duration = 3;
//       return PreferredSize;
//     }
//   }

//   getEndDate() async {
//     var end = await getendDate();
//     setState(() {
//       endDate = end;
//       endCon.text = DateFormat('dd-MM-yyyy').format(endDate).toString();
//       var splitDate = DateFormat('yyyy,MM,dd').format(endDate).toString().split(",");
//       fromDate = splitDate[0] + splitDate[1] + splitDate[2];
//     });
//   }

//   getStartDate() async {
//     var start = await getstartDate();
//     setState(() {
//       startDate = start;
//       startCon.text = DateFormat('dd-MM-yyyy').format(startDate).toString();
//       var splitDate = DateFormat('yyyy,MM,dd').format(startDate).toString().split(",");
//       toDate = splitDate[0] + splitDate[1] + splitDate[2];
//       print("controller ==> " + startCon.text);
//     });
//   }

//   Future<DateTime> getstartDate() {
//     return showDatePicker(
//       context: context,
//       initialDate: DateTime.now(),
//       firstDate: DateTime(2018),
//       lastDate: DateTime(2030),
//       builder: (BuildContext context, Widget child) {
//         return Theme(
//           data: ThemeData.light(),
//           child: child,
//         );
//       },
//     );
//   }

//   Future<DateTime> getendDate() {
//     return showDatePicker(
//       context: context,
//       initialDate: DateTime.now(),
//       firstDate: DateTime(2018),
//       lastDate: DateTime(2030),
//       builder: (BuildContext context, Widget child) {
//         return Theme(
//           data: ThemeData.light(),
//           child: child,
//         );
//       },
//     );
//   }
// }

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:nsb/constants/constant.dart';
import 'package:nsb/main.dart';
import 'package:nsb/model/TransitionListRequest.dart';
import 'package:nsb/model/TransitionListResponse.dart';
import 'package:nsb/pages/Transaction/Details.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:nsb/framework/http_service.dart' as http;
import 'package:http/http.dart' as http;

class TransitionMain extends StatefulWidget {
  @override
  _TransitionMainState createState() => _TransitionMainState();
}

class popupItem {
  static const String Default = 'Default';
  static const String TwoDays = '2Days';
  static const String FiveDays = '5Days';
  static const String Custom = 'Custom';

  static const List<String> choice = <String>[
    Default,
    TwoDays,
    FiveDays,
    Custom
  ];
}

class _TransitionMainState extends State<TransitionMain> {
  static final CREATE_POST_URL =
      'http://122.248.120.16:8080/WalletService/module001';
  var balance = 0.0;
  var locationList = [];
  var displayArray = [];
  List paymentList = [];
  List receiveList = [];
  var datas, q;
  var duration = 0;
  var pageC;
  var currPage;
  var pageSize;
  var date;
  var toDate;
  var fromDate;
  var endDate;
  var startDate;
  var selected;
  bool isLoading = true;
  TextEditingController startCon = TextEditingController();
  TextEditingController endCon = TextEditingController();
  String checklang = '';
  List textMyan = [
    "လုပ်ဆောင်မှု မှတ်တမ်း",
    "နောက်ဆုံး၁၀ကြောင်း",
    "လွန်ခဲ့သော၂ရက်",
    "လွန်ခဲ့သော၅ရက်" "စိတ်ကြိုက်ရွေးချယ်မည်"
  ];
  List textEng = [
    "Transactions",
    "Last 10 Transactions",
    "2 Days",
    "5 Days",
    "Custom"
  ];
  @override
  void initState() {
    super.initState();
    checkLanguage();
    pageSize = 10;
    currPage = 1;
    pageC = 0;
    this.getTransactionList(duration);
  }
   Future _viewload() async{
        setState((){
          isLoading = false;
        });
      }

  checkLanguage() async {
    final prefs = await SharedPreferences.getInstance();
    checklang = prefs.getString("Lang");
    // print(lang);
    if (checklang == "" || checklang == null || checklang.length == 0) {
      checklang = "Eng";
    } else {
      checklang = checklang;
    }
    setState(() {});
  }

  _choiceAction(selected) {
    print("object ===> $selected");
    if (selected == "default") {
      setState(() {
        isLoading=true;
      });
      duration = 0;
      locationList = [];
      paymentList = [];
      receiveList = [];
      print("duration ==> $duration");
      getTransactionList(duration);
    } else if (selected == "twoD") {
      setState(() {
        isLoading=true;
      });
      duration = 1;
      locationList = [];
      paymentList = [];
      receiveList = [];
      print("duration ==> $duration");
      getTransactionList(duration);
    } else if (selected == "fiveD") {
      setState(() {
        isLoading=true;
      });
      duration = 2;
      locationList = [];
      paymentList = [];
      receiveList = [];
      print("duration ==> $duration");
      getTransactionList(duration);
    } else if (selected == "custom") {
      duration = 3;
      return PreferredSize;
    }
  }

  getTransactionList(value) async {
    final prefs = await SharedPreferences.getInstance();
    String userID = prefs.getString('userId');
    String sessionID = prefs.getString('sessionID');
    String accountNo = prefs.getString('accountNo');

    TransitionListRequest transitionListRequest = new TransitionListRequest(
      userID: userID,
      sessionID: sessionID,
      customerNo: "",
      durationType: value,
      fromDate: "",
      toDate: "",
      totalCount: 0,
      acctNo: accountNo,
      currentPage: 2,
      pageSize: 10,
      pageCount: 0,
    );
    TransitionListResponse transitionListResponse = await getAllList1(
        CREATE_POST_URL + '/service001/getTransactionActivityList',
        transitionListRequest.toMap());
    for (var a = 0; a < locationList.length; a++) {
      if (locationList[a]["drcr"] == "1") {
        paymentList.add(locationList[a]);
        print(paymentList);
      } else if (locationList[a]["drcr"] == "2") {
        receiveList.add(locationList[a]);
        print(receiveList);
      }
    }
    if (transitionListResponse.code == '0000') {
      setState(() {
        isLoading=false;
      });
      print(locationList);
      print('Get Everything.................');
    }
  }

  Future<TransitionListResponse> getAllList1(url, Map jsonMap) async {
    TransitionListResponse p = new TransitionListResponse();
    var body;
    try {
      body = await http.doPost(url, jsonMap);
      p = TransitionListResponse.fromJson(json.decode(body.toString()));
      var data = jsonDecode(body);
      setState(() {
        locationList = data['data'];
      });
    } catch (e) {
      p.code = Constants.responseCode_Error;
      p.desc = e.toString();
    }
    print(p.toMap());
    return p;
  }

  String _currText = "";

  _onSelect(duration) {
    print("duration ==> $duration");
  }

  @override
  Widget build(BuildContext context) {
    var a = new Column(
      children: <Widget>[
        Container(
          child: Card(
            color: Colors.grey,
            elevation: 1.0,
            child: Text(
              "06-02-2020",
              style: TextStyle(
                fontSize: 25.0,
                height: 2.0,
              ),
            ),
          ),
        )
      ],
    );

    var all = new ListView.separated(
        separatorBuilder: (context, index) => Divider(
              color: Colors.white,
            ),
        itemCount: locationList.length,
        itemBuilder: (BuildContext context, int index) {
            isLoading=false;
          this.displayArray =
              this.locationList[index]["txnTypeDesc"].split(',');
          print("display ==> " + displayArray[1]);
          String dateString = locationList[index]["txnDate"];
          String d = dateString.substring(0, 4);
          String e = dateString.substring(5, 7);
          String f = dateString.substring(8, 10);
          String g = d + e + f;
          var date = new DateFormat.yMMMd().format(DateTime.parse(g));
          datas = date;
          print("data ==> $datas");
          return new Material(
            child: Column(children: <Widget>[
              Column(
                children: [
                  Container(
                    width: double.infinity,
                    height: 40,
                    child: Text(
                      locationList[index]["txnDate"],
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                    ),
                    decoration: BoxDecoration(
                      color: Color(0xffE4E4E4),
                      border: Border(
                        bottom: BorderSide(color: Colors.grey, width: 0.5),
                      ),
                    ),
                    padding: EdgeInsets.all(10),
                  ),
                ],
              ),
              Container(
                padding: const EdgeInsets.all(17.0),
                child: new GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => DetailsPage(
                          name: displayArray[1],
                          type: locationList[index]["remark"],
                          amount: locationList[index]["txnAmount"],
                          refNo: locationList[index]["txtReferenceNo"],
                          date: datas,
                          phNo: displayArray[0],
                        ),
                      ),
                    );
                  },
                  child: Row(children: <Widget>[
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          displayArray[1],
                          style: TextStyle(
                              fontSize: 16, fontWeight: FontWeight.bold),
                        ),
                        Divider(height: 10),
                        Text(
                          locationList[index]["remark"],
                          style: TextStyle(
                              fontSize: 16, fontWeight: FontWeight.w300),
                        ),
                      ],
                    ),
                    Spacer(),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: <Widget>[
                        (locationList[index]['drcr'] == "1")
                            ? Text(
                                locationList[index]["txnAmount"] + " MMK",
                                style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.red),
                              )
                            : Text(
                                locationList[index]["txnAmount"] + " MMK",
                                style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.blue),
                              ),
                        Divider(height: 10),
                        Text(
                          "$datas",
                          style: TextStyle(
                              fontSize: 15, fontWeight: FontWeight.w300),
                        ),
                      ],
                    ),
                  ]),
                ),
              ),
            ]),
          );
        });
    var payment = new ListView.separated(
        separatorBuilder: (context, index) => Divider(
              color: Colors.black,
            ),
        itemCount: paymentList.length,
        itemBuilder: (BuildContext context, int index) {
            isLoading=false;
          this.displayArray = this.paymentList[index]["txnTypeDesc"].split(',');
          String dateString = paymentList[index]["txnDate"];
          String d = dateString.substring(0, 4);
          String e = dateString.substring(5, 7);
          String f = dateString.substring(8, 10);
          String g = d + e + f;
          var date = new DateFormat.yMMMd().format(DateTime.parse(g));
          datas = date;

          return new Material(
            child: Container(
              padding: const EdgeInsets.all(17.0),
              child: new GestureDetector(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => DetailsPage(
                        name: displayArray[1],
                        type: paymentList[index]["remark"],
                        amount: paymentList[index]["txnAmount"],
                        refNo: paymentList[index]["txtReferenceNo"],
                        date: datas,
                        phNo: displayArray[0],
                      ),
                    ),
                  );
                },
                child: Row(children: <Widget>[
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        displayArray[1],
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.bold),
                      ),
                      Divider(height: 10),
                      Text(
                        paymentList[index]["remark"],
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.w300),
                      ),
                    ],
                  ),
                  Spacer(),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      Text(
                        paymentList[index]["txnAmount"] + " MMK",
                        style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                            color: Colors.red),
                      ),
                      Divider(height: 10),
                      Text(
                        "$datas",
                        style: TextStyle(
                            fontSize: 15, fontWeight: FontWeight.w300),
                      ),
                    ],
                  ),
                ]),
              ),
            ),
          );
        });
    var receive = new ListView.separated(
        separatorBuilder: (context, index) => Divider(
              color: Colors.black,
            ),
        itemCount: receiveList.length,
        itemBuilder: (BuildContext context, int index) {
            isLoading=false;
          this.displayArray = this.receiveList[index]["txnTypeDesc"].split(',');
          String dateString = receiveList[index]["txnDate"];
          String d = dateString.substring(0, 4);
          String e = dateString.substring(5, 7);
          String f = dateString.substring(8, 10);
          String g = d + e + f;
          var date = new DateFormat.yMMMd().format(DateTime.parse(g));
          datas = date;
          return new Material(
            child: Container(
              padding: const EdgeInsets.all(17.0),
              child: new GestureDetector(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => DetailsPage(
                        name: displayArray[1],
                        type: receiveList[index]["remark"],
                        amount: receiveList[index]["txnAmount"],
                        refNo: receiveList[index]["txtReferenceNo"],
                        date: datas,
                        phNo: displayArray[0],
                      ),
                    ),
                  );
                },
                child: Row(children: <Widget>[
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        displayArray[1],
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.bold),
                      ),
                      Divider(height: 10),
                      Text(
                        receiveList[index]["remark"],
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.w300),
                      ),
                    ],
                  ),
                  Spacer(),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      Text(
                        receiveList[index]["txnAmount"] + " MMK",
                        style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                            color: Colors.blue),
                      ),
                      Divider(height: 10),
                      Text(
                        "$datas",
                        style: TextStyle(
                            fontSize: 15, fontWeight: FontWeight.w300),
                      ),
                    ],
                  ),
                ]),
              ),
            ),
          );
        });

    var viewbody = TabBarView(
            children: <Widget>[
              all,
              payment,
              receive,
            ],
          );

    var bodyProgress = new Container(
      child: new Stack(
        children: <Widget>[
          viewbody,
          Container(
              decoration: new BoxDecoration(
                color: Color.fromRGBO(255, 255, 255, 0.5),
              ),
              width: MediaQuery.of(context).size.width * 0.99,
              height: MediaQuery.of(context).size.height * 0.9,
              child: Center(
                child: CircularProgressIndicator(
                  backgroundColor: Colors.amber,
                ),
              ))
        ],
      ),
    );

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: DefaultTabController(
        length: 3,
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.white,
            centerTitle: true,
            title: (checklang == "Eng")
                ? Text(
                    (checklang == "Eng") ? textEng[0] : textMyan[0],
                    style: TextStyle(
                        fontSize: 22, color: Color.fromRGBO(40, 103, 178, 1)),
                  )
                : Text(
                    (checklang == "Eng") ? textEng[0] : textMyan[0],
                    style: TextStyle(
                        fontSize: 20, color: Color.fromRGBO(40, 103, 178, 1)),
                  ),
            // title: Text(
            //   ,
            //   style: TextStyle(
            //     fontSize: 22,
            //     color: Color.fromRGBO(40, 103, 178, 1),
            //   ),
            // ),
            leading: IconButton(
              icon: Icon(
                Icons.arrow_back,
                color: Colors.black,
              ),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
            actions: <Widget>[
              PopupMenuButton<String>(
                  onSelected: (String value) {
                    setState(() {
                      selected = value;
                      print("selected ==> $selected");
                      _choiceAction(selected);
                    });
                  },
                  color: Colors.white,
                  child: Padding(
                    padding: EdgeInsets.all(14),
                    child: Icon(
                      Icons.arrow_drop_down,
                      color: Colors.black,
                      size: 30.0,
                    ),
                  ),
                  itemBuilder: (BuildContext context) {
                    return [
                      PopupMenuItem(
                        value: "default",
                        child: GestureDetector(
                          child: Text("Last 10 Transaction",
                              style: TextStyle(color: Colors.blue)),
                        ),
                      ),
                      PopupMenuItem(
                        value: "twoD",
                        child: GestureDetector(
                          child: Text("2 Days",
                              style: TextStyle(color: Colors.blue)),
                        ),
                      ),
                      PopupMenuItem(
                        value: "fiveD",
                        child: GestureDetector(
                          onTap: () {
                            _choiceAction('fiveD');
                          },
                          child: Text("5 Days",
                              style: TextStyle(color: Colors.blue)),
                        ),
                      ),
                      PopupMenuItem(
                        value: "custom",
                        child: GestureDetector(
                          child: Text("Custom",
                              style: TextStyle(color: Colors.blue)),
                        ),
                      ),
                    ];
                  }),
            ],
            elevation: 0,
            bottom: (duration == 3)
                ? PreferredSize(
                    preferredSize: const Size.fromHeight(120.0),
                    child: Padding(
                      padding:
                          EdgeInsets.only(left: 5, right: 5, top: 0, bottom: 0),
                      child: Column(
                        children: <Widget>[
                          Row(children: [
                            new Flexible(
                              child: Padding(
                                padding: EdgeInsets.all(5),
                                child: TextField(
                                  controller: startCon,
                                  keyboardType: TextInputType.datetime,
                                  onTap: () {
                                    FocusScope.of(context)
                                        .requestFocus(new FocusNode());
                                    getStartDate();
                                  },
                                  decoration: InputDecoration(
                                    labelText: "From Date",
                                  ),
                                ),
                              ),
                            ),
                            new Flexible(
                              child: Padding(
                                padding: EdgeInsets.all(10),
                                child: TextField(
                                  controller: endCon,
                                  keyboardType: TextInputType.datetime,
                                  onTap: () {
                                    FocusScope.of(context)
                                        .requestFocus(new FocusNode());
                                    getEndDate();
                                  },
                                  decoration:
                                      InputDecoration(labelText: "To Date"),
                                ),
                              ),
                            ),
                          ]),
                          Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                Padding(
                                  padding:
                                      EdgeInsets.only(bottom: 10, right: 10),
                                  child: RaisedButton(
                                      color: Color.fromRGBO(40, 103, 178, 1),
                                      child: Text(
                                        "Search",
                                        style: TextStyle(color: Colors.white),
                                      ),
                                      onPressed: () async {
                                        // if (startDate > endDate) {
                                        //   print("Impossible ...");
                                        // } else {
                                        searchDatawithDate();
                                        // print(startDate);
                                        // }
                                      }),
                                ),
                              ]),
                        ],
                      ),
                    ),
                  )
                : TabBar(
                    unselectedLabelColor: Colors.blue,
                    indicatorSize: TabBarIndicatorSize.label,
                    tabs: [
                      Tab(
                        child: Container(
                          child: Align(
                            alignment: Alignment.center,
                            child: Text(
                              "ALL",
                              style: TextStyle(color: Colors.blue),
                            ),
                          ),
                        ),
                      ),
                      Tab(
                        child: Container(
                          child: Align(
                            alignment: Alignment.center,
                            child: Text(
                              "PAYMENT",
                              style: TextStyle(color: Colors.blue),
                            ),
                          ),
                        ),
                      ),
                      Tab(
                        child: Container(
                          child: Align(
                            alignment: Alignment.center,
                            child: Text(
                              "RECEIVE",
                              style: TextStyle(color: Colors.blue),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
          ),
          body: isLoading ? bodyProgress : viewbody
          // body: TabBarView(
          //   children: <Widget>[
          //     all,
          //     payment,
          //     receive,
          //   ],
          // ),
        ),
      ),
    );
  }

  searchDatawithDate() async {
     setState(() {
      isLoading=true;
    });
    locationList = [];
    paymentList = [];
    receiveList = [];
    final prefs = await SharedPreferences.getInstance();
    String userID = prefs.getString('userId');
    String sessionID = prefs.getString('sessionID');
    String accountNo = prefs.getString('accountNo');
    String url =
        "http://122.248.120.16:8080/WalletService/module001/service001/getTransactionActivityList";
    Map<String, String> headers = {"Content-type": "application/json"};
    String json = '{ "userID": "' +
        userID +
        '", "sessionID":"' +
        sessionID +
        '" , "customerNo":"' +
        "" +
        '", "durationType":"' +
        "$duration" +
        '", "fromDate":"' +
        toDate +
        '", "toDate":"' +
        fromDate +
        '", "totalCount":"' +
        "0" +
        '", "acctNo":"' +
        accountNo +
        '", "currentPage":"' +
        "1" +
        '", "pageSize":"' +
        "10" +
        '", "pageCount":"' +
        "0" +
        '"}';
    http.Response response = await http.post(url, headers: headers, body: json);
    int statusCode = response.statusCode;
    if (statusCode == 200) {
        setState(() {
          isLoading=false;
        });
      String body = response.body;
      print(body);
      var data = jsonDecode(body);
      print(data);
      duration = 0;
      locationList = data["data"];
      print(locationList.length);
      for (var a = 0; a < locationList.length; a++) {
        if (locationList[a]["drcr"] == "1") {
          paymentList.add(locationList[a]);
          print(paymentList);
        } else if (locationList[a]["drcr"] == "2") {
          receiveList.add(locationList[a]);
          print(receiveList);
        }
      }
      setState(() {});
    } else {
      print("Connection Fail");
      setState(() {
          isLoading=true;
        });
    }
  }

  getEndDate() async {
    var end = await getendDate();
    setState(() {
      endDate = end;
      endCon.text = DateFormat('dd-MM-yyyy').format(endDate).toString();
      var splitDate =
          DateFormat('yyyy,MM,dd').format(endDate).toString().split(",");
      fromDate = splitDate[0] + splitDate[1] + splitDate[2];
    });
  }

  getStartDate() async {
    var start = await getstartDate();
    setState(() {
      startDate = start;
      startCon.text = DateFormat('dd-MM-yyyy').format(startDate).toString();
      var splitDate =
          DateFormat('yyyy,MM,dd').format(startDate).toString().split(",");
      toDate = splitDate[0] + splitDate[1] + splitDate[2];
      print("controller ==> " + startCon.text);
    });
  }

  Future<DateTime> getstartDate() {
    return showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(2018),
      lastDate: DateTime(2030),
      builder: (BuildContext context, Widget child) {
        return Theme(
          data: ThemeData.light(),
          child: child,
        );
      },
    );
  }

  Future<DateTime> getendDate() {
    return showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(2018),
      lastDate: DateTime(2030),
      builder: (BuildContext context, Widget child) {
        return Theme(
          data: ThemeData.light(),
          child: child,
        );
      },
    );
  }
}
