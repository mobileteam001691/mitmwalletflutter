import 'package:flutter/material.dart';
import 'package:nsb/pages/Wallet.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MeterBillSuccessPage extends StatefulWidget {
  final String value;
  final String value1;
  final String value2;
  final String value3;
  final String value4;
  MeterBillSuccessPage(
      {Key key, this.value, this.value1, this.value2, this.value3, this.value4})
      : super(key: key);

  @override
  _MeterBillSuccessPageState createState() => _MeterBillSuccessPageState();
}

class _MeterBillSuccessPageState extends State<MeterBillSuccessPage> {
  String alertmsg = "";
  String rkey = "";
  bool _isLoading;
  final myControllerno = TextEditingController();
  final myControllername = TextEditingController();
  final myControlleramout = TextEditingController();
  final myControllerref = TextEditingController();
  String checklang = '';
  List textMyan = ["ငွေပေးချေမှုရလဒ်", "ငွေပေးချေမှု အောင်မြင်ပါသည်", "အမှတ်စဥ်", "အမည်", "ငွေပမာဏ", "လုပ်ဆောင်ခဲ့သည့်ရက်", "ပိတ်မည်"];
  List textEng = ["Transaction Success","Payment Successful","Transcation No.", "Name", "Amount", "Transaction Date", "Close"];
  final _formKey = new GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldkey = new GlobalKey<ScaffoldState>();

  void initState() {
    this._isLoading = false;
    checkLanguage();
    super.initState();
  }

  checkLanguage() async {
    final prefs = await SharedPreferences.getInstance();
    checklang = prefs.getString("Lang");
    // print(lang);
    if (checklang == "" || checklang == null || checklang.length == 0) {
      checklang = "Eng";
    } else {
      checklang = checklang;
    }
    setState(() {});
  }

  void _method1() {
    print("Snack Bar");
    print(this.alertmsg);
    _scaffoldkey.currentState.showSnackBar(new SnackBar(
        content: new Text(this.alertmsg), duration: Duration(seconds: 1)));
  }

  Widget build(BuildContext context) {
    final style = TextStyle(fontFamily: 'Montserrat', fontSize: 16.0);

    final titleField = new Container(
        child: Row(children: <Widget>[
      Text(
        (checklang == "Eng") ? textEng[1] : textMyan[1],
        style: TextStyle(
          fontSize: 19,
          color: Colors.white,
        ),
        textAlign: TextAlign.center,
      ),
    ]));

    final loginField = new Container(
        child: Column(children: <Widget>[
      ListTile(
        title: Padding(
          padding: const EdgeInsets.fromLTRB(8, 2, 5, 10),
          child: Text(
            (checklang == "Eng") ? textEng[2] : textMyan[2],
            style: TextStyle(fontSize: 16),
          ),
        ),
        subtitle: Padding(
          padding: const EdgeInsets.all(5.0),
          child: Text("${widget.value}", style: TextStyle(fontSize: 18)),
        ),
      ),
      Divider(
        color: Colors.black,
      )
    ]));

    final passwordField = new Container(
        child: Column(children: <Widget>[
      ListTile(
        title: Padding(
          padding: const EdgeInsets.fromLTRB(8, 2, 5, 10),
          child: Text(
            (checklang == "Eng") ? textEng[3] : textMyan[3],
            style: TextStyle(fontSize: 16, color: Colors.black),
          ),
        ),
        subtitle: Padding(
          padding: const EdgeInsets.all(5.0),
          child: Text("${widget.value4}", style: TextStyle(fontSize: 18)),
        ),
      ),
      Divider(
        color: Colors.black,
      )
    ]));

    final passwordField2 = new Container(
        child: Column(children: <Widget>[
      ListTile(
        title: Padding(
          padding: const EdgeInsets.fromLTRB(5, 2, 5, 10),
          child: Text(
            (checklang == "Eng") ? textEng[4] : textMyan[4],
            style: TextStyle(fontSize: 16, color: Colors.black),
          ),
        ),
        subtitle: Padding(
          padding: const EdgeInsets.all(5.0),
          child: Text("${widget.value2}" + ".00 MMK",
              style: TextStyle(fontSize: 18)),
        ),
      ),
      Divider(
        color: Colors.black,
      )
    ]));

    final transactiondate = new Container(
        child: Column(children: <Widget>[
      ListTile(
        title: Padding(
          padding: const EdgeInsets.fromLTRB(5, 2, 5, 10),
          child: Text(
            (checklang == "Eng") ? textEng[5] : textMyan[5],
            style: TextStyle(fontSize: 16, color: Colors.black),
          ),
        ),
        subtitle: Padding(
          padding: const EdgeInsets.all(5.0),
          child: Text("${widget.value3}", style: TextStyle(fontSize: 18)),
        ),
      ),
      Divider(
        color: Colors.black,
      )
    ]));

    final transferbutton = new RaisedButton(
      onPressed: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => WalletPage()),
        );
      },
      color: Color.fromRGBO(40, 103, 178, 1),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      textColor: Colors.white,
      child: Container(
        width: 300.0,
        height: 43.0,
        child: Center(
            child: Text((checklang == "Eng") ? textEng[6] : textMyan[6],
                style: TextStyle(fontSize: 18, color: Colors.white))),
      ),
    );

    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: new AppBar(
          //Application Bar
          centerTitle: true,
          elevation: 0.0,
          backgroundColor: Color.fromRGBO(40, 103, 178, 1),
          title: Center(
            child: Text(
              (checklang == "Eng") ? textEng[0] : textMyan[0],
              style: TextStyle(
                  fontSize: 20.0,
                  color: Colors.white,
                  height: 1.0,
                  fontWeight: FontWeight.w600),
            ),
          )),
      body: new Form(
        key: _formKey,
        child: new ListView(
          children: <Widget>[
            SizedBox(height: 10.0),
            Container(
              padding: EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 0.0),
              height: 600,
              child: Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                elevation: 3.0,
                child: ListView(
                  padding: EdgeInsets.all(5.0),
                  children: <Widget>[
                    Center(
                      child: new Container(
                        height: 80,
                        color: Colors.green[500],
                        padding: EdgeInsets.only(left: 30.0, right: 10.0),
                        child: titleField,
                      ),
                    ),
                    SizedBox(height: 5.0),
                    Center(
                      child: new Container(
                        padding: EdgeInsets.only(left: 10.0, right: 10.0),
                        child: loginField,
                      ),
                    ),
                    SizedBox(height: 5.0),
                    Center(
                      child: new Container(
                        padding: EdgeInsets.only(left: 10.0, right: 10.0),
                        child: passwordField,
                      ),
                    ),
                    SizedBox(height: 5.0),
                    Center(
                      child: new Container(
                        padding: EdgeInsets.only(left: 10.0, right: 10.0),
                        child: passwordField2,
                      ),
                    ),
                    SizedBox(height: 5.0),
                    Center(
                      child: new Container(
                        padding: EdgeInsets.only(left: 10.0, right: 10.0),
                        child: transactiondate,
                      ),
                    ),
                    SizedBox(height: 20.0),
                    Center(
                      child: new Container(
                        padding: EdgeInsets.only(left: 30.0, right: 30.0),
                        child: transferbutton,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
