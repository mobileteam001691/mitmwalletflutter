import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:nsb/constants/constant.dart';
import 'package:nsb/model/ReadMessageRequest.dart';
import 'package:nsb/model/ReadMessageResponse.dart';
import 'package:nsb/pages/MeterBill/meterBillConfirm.dart';
import 'package:nsb/pages/Transfer/transferconfirm.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:nsb/framework/http_service.dart' as http;

class MeterBillPage extends StatefulWidget {
  final String value;
  final String value1;
  final String value2;
  MeterBillPage({Key key, this.value, this.value1, this.value2})
      : super(key: key);

  @override
  _MeterBillPageState createState() => _MeterBillPageState();
}

class _MeterBillPageState extends State<MeterBillPage> {
  String alertmsg = "";
  String rkey = "";
  bool _isLoading;
  final myControllerno = TextEditingController();
  final myControllername = TextEditingController();
  final myControlleramout = TextEditingController();
  final myControllerref = TextEditingController();
  final _formKey = new GlobalKey<FormState>();
  String checklang = '';
  List textMyan = [
    "ငွေပေးသွင်းရန်",
    "ဖုန်းနံပါတ်",
    "အမည်",
    "ငွေပမာဏ",
    "ပေးသွင်းမည်"
  ];
  List textEng = ["Payment", "Phone Number", "Name", "Amount", "Pay"];
  final GlobalKey<ScaffoldState> _scaffoldkey = new GlobalKey<ScaffoldState>();
  static final CREATE_POST_URL =
      'http://122.248.120.16:8080/WalletService/module001';

  void initState() {
    checkLanguage();
    myControllerno.text = widget.value2;
myControllername.text = widget.value;
    super.initState();
  }

  checkLanguage() async {
    final prefs = await SharedPreferences.getInstance();
    checklang = prefs.getString("Lang");
    // print(lang);
    if (checklang == "" || checklang == null || checklang.length == 0) {
      checklang = "Eng";
    } else {
      checklang = checklang;
    }
    setState(() {});
  }

  void _method1() {
    print("Snack Bar");
    print(this.alertmsg);
    _scaffoldkey.currentState.showSnackBar(new SnackBar(
        content: new Text(this.alertmsg), duration: Duration(seconds: 1)));
  }

  Widget build(BuildContext context) {
    final style = TextStyle(fontFamily: 'Montserrat', fontSize: 16.0);

    final titleField = new Container(
        child: Row(children: <Widget>[
      Text("Merchant Name:",
          style: TextStyle(fontSize: 19, color: Colors.black)),
      Text(
        "${widget.value}",
        style: TextStyle(color: Color.fromRGBO(40, 103, 178, 1), fontSize: 18),
      ),
    ]));

    // final loginField = new Container(
    //     child: Column(children: <Widget>[
    //   ListTile(
    //     title: Padding(
    //       padding: const EdgeInsets.fromLTRB(8, 2, 8, 15),
    //       child: Text(
    //         (checklang == "Eng") ? textEng[1] : textMyan[1],
    //         style: TextStyle(fontSize: 15),
    //       ),
    //     ),
    //     subtitle: Padding(
    //       padding: const EdgeInsets.all(5.0),
    //       child: Text("${widget.value2}", style: TextStyle(fontSize: 18)),
    //     ),
    //   ),
    //   Divider(
    //     color: Colors.black,
    //   )
    // ]));

    // final passwordField = new Container(
    //     child: Column(children: <Widget>[
    //   ListTile(
    //     title: Padding(
    //       padding: const EdgeInsets.fromLTRB(8, 2, 8, 15),
    //       child: Text(
    //         (checklang == "Eng") ? textEng[2] : textMyan[2],
    //         style: TextStyle(fontSize: 15, color: Colors.black),
    //       ),
    //     ),
    //     subtitle: Padding(
    //       padding: const EdgeInsets.all(5.0),
    //       child: Text("${widget.value}", style: TextStyle(fontSize: 18)),
    //     ),
    //   ),
    //   Divider(
    //     color: Colors.black,
    //   )
    // ]));

    final loginField = new TextFormField(
      controller: myControllerno,
      keyboardType: TextInputType.number,
      enabled: false,
      decoration: InputDecoration(
        // icon: const Icon(Icons.attach_money),
        // suffixText: "MMK",
        labelText: (checklang == "Eng") ? textEng[1] : textMyan[1],
        hasFloatingPlaceholder: true,
        labelStyle: (checklang == "Eng")
            ? TextStyle(fontSize: 18, color: Colors.black, height: 0)
            : TextStyle(fontSize: 17, color: Colors.black, height: 0),
        fillColor: Colors.black87,
      ),
    );

    final passwordField = new TextFormField(
      controller: myControllername,
      enabled: false,
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
        // icon: const Icon(Icons.attach_money),
        // suffixText: "MMK",
        labelText: (checklang == "Eng") ? textEng[2] : textMyan[2],
        hasFloatingPlaceholder: true,
        labelStyle: (checklang == "Eng")
            ? TextStyle(fontSize: 18, color: Colors.black, height: 0)
            : TextStyle(fontSize: 17, color: Colors.black, height: 0),
        fillColor: Colors.black87,
      ),
    );

    final passwordField2 = new TextFormField(
      controller: myControlleramout,
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
        // icon: const Icon(Icons.attach_money),
        suffixText: "MMK",
        labelText: (checklang == "Eng") ? textEng[3] : textMyan[3],
        hasFloatingPlaceholder: true,
        labelStyle: (checklang == "Eng")
            ? TextStyle(fontSize: 18, color: Colors.black, height: 0)
            : TextStyle(fontSize: 17, color: Colors.black, height: 0),
        fillColor: Colors.black87,
      ),
    );

    final transferbutton = new RaisedButton(
      onPressed: () async {
        this.alertmsg = '';
        final prefs = await SharedPreferences.getInstance();
        String userID = prefs.getString('userId');
        String sessionID = prefs.getString('sessionID');
        ReadMessageRequest readMessageRequest = new ReadMessageRequest(
          userID: userID,
          sessionID: sessionID,
          type: "1",
          merchantID: "${widget.value1}",
        );
        ReadMessageResponse readMessageResponse = await goLogin(
            CREATE_POST_URL + '/service002/readMessageSetting',
            readMessageRequest.toMap());
        print(userID);
        print(sessionID);
        if (readMessageResponse.code == '0000') {
          print(readMessageResponse.toString());
          var route = new MaterialPageRoute(
              builder: (BuildContext context) => new meterBillConfirmPage(
                  value: myControlleramout.text,
                  value1: "${widget.value}",
                  value2: "${widget.value1}"));
          Navigator.of(context).push(route);
        }
      },
      color: Color.fromRGBO(40, 103, 178, 1),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      textColor: Colors.white,
      child: Container(
        width: 300.0,
        height: 43.0,
        child: Center(
           child: (checklang == "Eng")
              ? Text(
                  (checklang == "Eng") ? textEng[4] : textMyan[4],
                  style: TextStyle(fontSize: 18),
                )
              : Text(
                  (checklang == "Eng") ? textEng[4] : textMyan[4],
                  style: TextStyle(fontSize: 16),
                ),
        ),
      ),
    );

    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: new AppBar(
          //Application Bar
          elevation: 0.0,
          backgroundColor: Color.fromRGBO(40, 103, 178, 1),
          centerTitle: true,
          title: Text(
              (checklang == "Eng") ? textEng[0] : textMyan[0],
              style: TextStyle(
                  fontSize: 20.0,
                  color: Colors.white,
                  height: 1.0,
                  fontWeight: FontWeight.w600),
            ),
          ),
      body: new Form(
        key: _formKey,
        child: new ListView(
          children: <Widget>[
            SizedBox(height: 10.0),
            Container(
              padding: EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 0.0),
              height: 530,
              child: Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                elevation: 3.0,
                child: ListView(
                  padding: EdgeInsets.all(5.0),
                  children: <Widget>[
                    Center(
                      child: new Container(
                        height: 80,
                        color: Colors.grey[300],
                        padding: EdgeInsets.only(left: 30.0, right: 10.0),
                        child: titleField,
                      ),
                    ),
                    SizedBox(height: 20.0),
                    Center(
                      child: new Container(
                        padding: EdgeInsets.only(left: 10.0, right: 10.0),
                        child: loginField,
                      ),
                    ),
                    SizedBox(height: 30.0),
                    Center(
                      child: new Container(
                        padding: EdgeInsets.only(left: 10.0, right: 10.0),
                        child: passwordField,
                      ),
                    ),
                    SizedBox(height: 30.0),
                    Center(
                      child: new Container(
                        padding: EdgeInsets.only(left: 10.0, right: 10.0),
                        child: passwordField2,
                      ),
                    ),
                    SizedBox(height: 40.0),
                    Center(
                      child: new Container(
                        padding: EdgeInsets.only(left: 30.0, right: 30.0),
                        child: transferbutton,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<ReadMessageResponse> goLogin(url, Map jsonMap) async {
    ReadMessageResponse p = new ReadMessageResponse();
    var body;
    try {
      body = await http.doPost(url, jsonMap);
      p = ReadMessageResponse.fromJson(json.decode(body.toString()));
    } catch (e) {
      p.otpCode = Constants.responseCode_Error;
      p.otpMessage = e.toString();
    }
    print(p.toMap());
    return p;
  }
}
