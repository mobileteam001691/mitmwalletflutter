import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:nsb/constants/constant.dart';
import 'package:nsb/model/GoMerchantPaymentRequest.dart';
import 'package:nsb/model/GoTransferResponse.dart';
import 'package:nsb/pages/MeterBill/meterBillSuccess.dart';
import 'package:nsb/pages/Wallet.dart';
import 'package:nsb/utils/crypt_helper.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:nsb/framework/http_service.dart' as http;

class meterBillConfirmPage extends StatefulWidget {
  final String value;
  final String value1;
  final String value2;

  meterBillConfirmPage({Key key, this.value, this.value1, this.value2})
      : super(key: key);
  @override
  _meterBillConfirmPageState createState() => _meterBillConfirmPageState();
}

class _meterBillConfirmPageState extends State<meterBillConfirmPage> {
  String alertmsg = "";
  String rkey = "";
  String password = "";
  bool _isLoading;
  bool isLoading = false;
  String checklang = '';
  List textMyan = [
    "အတည်ပြုခြင်း",
    "MerchantID",
    "အမည်",
    "ငွေပမာဏ",
    "ပယ်ဖျက်မည်",
    "ပေးသွင်းမည်",
    "ကျပ်"
  ];
  List textEng = [
    "Transfer Confirm",
    "MerchantID",
    "Name",
    "Amount",
    "Cancel",
    "Pay",
    "MMK"
  ];
  final myControllerno = TextEditingController();
  final myControllername = TextEditingController();
  final myControlleramout = TextEditingController();
  final myControllerref = TextEditingController();
  final _formKey = new GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldkey = new GlobalKey<ScaffoldState>();
  static final CREATE_POST_URL =
      'http://122.248.120.16:8080/WalletService/module001';

  void initState() {
    this._isLoading = false;
    checkLanguage();
    super.initState();
  }

  void _method1() {
    print("Snack Bar");
    print(this.alertmsg);
    _scaffoldkey.currentState.showSnackBar(new SnackBar(
        content: new Text(this.alertmsg), duration: Duration(seconds: 1)));
  }

  checkLanguage() async {
    final prefs = await SharedPreferences.getInstance();
    checklang = prefs.getString("Lang");
    // print(lang);
    if (checklang == "" || checklang == null || checklang.length == 0) {
      checklang = "Eng";
    } else {
      checklang = checklang;
    }
    setState(() {});
  }

  Widget build(BuildContext context) {
    final style = TextStyle(fontFamily: 'Montserrat', fontSize: 16.0);

    final titleField = new Container(
        child: Column(children: <Widget>[
      ListTile(
        title: Padding(
          padding: const EdgeInsets.fromLTRB(5, 2, 8, 15),
          child: Text(
            (checklang == "Eng") ? textEng[1] : textMyan[1],
            style: TextStyle(fontSize: 15),
          ),
        ),
        subtitle: Padding(
          padding: const EdgeInsets.all(5.0),
          child: Text("${widget.value2}",
              style: TextStyle(fontSize: 18, color: Colors.grey)),
        ),
      ),
      Divider(
        color: Colors.black,
      )
    ]));

    final passwordField = new Container(
        child: Column(children: <Widget>[
      ListTile(
        title: Padding(
          padding: const EdgeInsets.fromLTRB(8, 2, 8, 15),
          child: Text(
            (checklang == "Eng") ? textEng[2] : textMyan[2],
            style: TextStyle(fontSize: 15),
          ),
        ),
        subtitle: Padding(
          padding: const EdgeInsets.all(5.0),
          child: Text("${widget.value1}",
              style: TextStyle(fontSize: 18, color: Colors.grey)),
        ),
      ),
      Divider(
        color: Colors.black,
      )
    ]));

    final passwordField2 = new Container(
        child: Column(children: <Widget>[
      ListTile(
        title: Padding(
          padding: const EdgeInsets.fromLTRB(5, 2, 8, 15),
          child: Text(
            (checklang == "Eng") ? textEng[3] : textMyan[3],
            style: TextStyle(fontSize: 15, color: Colors.black),
          ),
        ),
        subtitle: Padding(
          padding: const EdgeInsets.all(5.0),
          child: Text("${widget.value}" + " .00MMK",
              style: TextStyle(fontSize: 18, color: Colors.grey)),
        ),
      ),
      Divider(
        color: Colors.black,
      )
    ]));

    final cancelbutton = new RaisedButton(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      onPressed: () async {
        this.alertmsg = '';
        Navigator.pop(context);
      },
      color: Colors.grey[400],
      textColor: Colors.white,
      child: Container(
        width: 120.0,
        height: 43.0,
        child: Center(
          child: (checklang == "Eng")
              ? Text(
                  (checklang == "Eng") ? textEng[4] : textMyan[4],
                  style: TextStyle(fontSize: 18),
                )
              : Text(
                  (checklang == "Eng") ? textEng[4] : textMyan[4],
                  style: TextStyle(fontSize: 16),
                ),
        ),
      ),
    );

    final transferbutton = new RaisedButton(
      onPressed: () async {
        setState(() {
          isLoading=true;
        });
        final prefs = await SharedPreferences.getInstance();
        String userID = prefs.getString('userId');
        String username = prefs.getString('name');
        String sessionID = prefs.getString('sessionID');
        final iv = AesUtil.random(16);
        print("iv :" + iv);
        final dm = AesUtil.random(16);
        print("dm :" + dm);
        final salt = AesUtil.random(16);
        print("salt :" + salt);
        String res = AesUtil.encrypt(salt, iv, this.password);
        print("res is :" + res);
        GoMerchantPaymentRequest goTransferRequest =
            new GoMerchantPaymentRequest(
                token: sessionID,
                senderCode: userID,
                merchantID: "${widget.value2}",
                fromName: username,
                amount: "${widget.value}",
                prevBalance: "",
                password: res,
                iv: iv,
                dm: dm,
                salt: salt);
        GoTransferResponse goTransferResponse = await goOpenAccount(
            CREATE_POST_URL + '/payment/goMerchantPayment',
            goTransferRequest.toMap());
        if (goTransferResponse.code == '0000') {
          setState(() {
            isLoading=false;
          });
          this.alertmsg = "";
          print(goTransferResponse);
          print(goTransferResponse.toString());
          var route = new MaterialPageRoute(
              builder: (BuildContext context) => new MeterBillSuccessPage(
                  value: goTransferResponse.bankRefNumber,
                  value1: goTransferRequest.fromName,
                  value2: "${widget.value}",
                  value3: goTransferResponse.transactionDate,
                  value4: "${widget.value1}"));
          Navigator.of(context).push(route);
          this.alertmsg = goTransferResponse.desc;
          this._method1();
        } else {
          this.alertmsg = goTransferResponse.desc;
          this._method1();
          final form = _formKey.currentState;
          form.validate();
          this._isLoading = false;
          setState(() {
            isLoading=false;
          });
        }
      },
      color: Color.fromRGBO(40, 103, 178, 1),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      textColor: Colors.white,
      child: Container(
        width: 120.0,
        height: 43.0,
        child: Center(
          child: (checklang == "Eng")
              ? Text(
                  (checklang == "Eng") ? textEng[5] : textMyan[5],
                  style: TextStyle(fontSize: 18),
                )
              : Text(
                  (checklang == "Eng") ? textEng[5] : textMyan[5],
                  style: TextStyle(fontSize: 16),
                ),
        ),
      ),
    );

    var billbody = new Form(
        key: _formKey,
        child: new ListView(
          children: <Widget>[
            SizedBox(height: 10.0),
            Container(
              padding: EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 0.0),
              height: 420,
              child: Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                elevation: 3.0,
                child: ListView(
                  padding: EdgeInsets.all(5.0),
                  children: <Widget>[
                    Center(
                      child: new Container(
                        padding: EdgeInsets.only(left: 10.0, right: 10.0),
                        child: titleField,
                      ),
                    ),
                    SizedBox(height: 10.0),
                    Center(
                      child: new Container(
                        padding: EdgeInsets.only(left: 10.0, right: 10.0),
                        child: passwordField,
                      ),
                    ),
                    SizedBox(height: 10.0),
                    Center(
                      child: new Container(
                        padding: EdgeInsets.only(left: 10.0, right: 10.0),
                        child: passwordField2,
                      ),
                    ),
                    SizedBox(height: 30.0),
                    Row(
                      children: <Widget>[
                        new Container(
                          padding: EdgeInsets.only(left: 12.0),
                          child: cancelbutton,
                        ),
                        new Container(
                          padding: EdgeInsets.only(left: 14.0),
                          child: transferbutton,
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      );

    var bodyProgress = new Container(
      child: new Stack(
        children: <Widget>[
          billbody,
          Container(
              decoration: new BoxDecoration(
                color: Color.fromRGBO(255, 255, 255, 0.5),
              ),
              width: MediaQuery.of(context).size.width * 0.99,
              height: MediaQuery.of(context).size.height * 0.9,
              child: Center(
                child: CircularProgressIndicator(
                  backgroundColor: Colors.amber,
                ),
              ))
        ],
      ),
    );

    return Scaffold(
      key: _scaffoldkey,
      backgroundColor: Colors.grey[200],
      appBar: new AppBar(
        //Application Bar
        elevation: 0.0,
        backgroundColor: Color.fromRGBO(40, 103, 178, 1),
        centerTitle: true, 
        title: (checklang == "Eng")
            ? Text(
                (checklang == "Eng") ? textEng[0] : textMyan[0],
                style: TextStyle(fontSize: 20, color: Colors.white),
              )
            : Text(
                (checklang == "Eng") ? textEng[0] : textMyan[0],
                style: TextStyle(fontSize: 18, color: Colors.white),
              ),
      ),
      body: isLoading ? bodyProgress : billbody
      // body: new Form(
      //   key: _formKey,
      //   child: new ListView(
      //     children: <Widget>[
      //       SizedBox(height: 10.0),
      //       Container(
      //         padding: EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 0.0),
      //         height: 420,
      //         child: Card(
      //           shape: RoundedRectangleBorder(
      //             borderRadius: BorderRadius.circular(10.0),
      //           ),
      //           elevation: 3.0,
      //           child: ListView(
      //             padding: EdgeInsets.all(5.0),
      //             children: <Widget>[
      //               Center(
      //                 child: new Container(
      //                   padding: EdgeInsets.only(left: 10.0, right: 10.0),
      //                   child: titleField,
      //                 ),
      //               ),
      //               SizedBox(height: 10.0),
      //               Center(
      //                 child: new Container(
      //                   padding: EdgeInsets.only(left: 10.0, right: 10.0),
      //                   child: passwordField,
      //                 ),
      //               ),
      //               SizedBox(height: 10.0),
      //               Center(
      //                 child: new Container(
      //                   padding: EdgeInsets.only(left: 10.0, right: 10.0),
      //                   child: passwordField2,
      //                 ),
      //               ),
      //               SizedBox(height: 30.0),
      //               Row(
      //                 children: <Widget>[
      //                   new Container(
      //                     padding: EdgeInsets.only(left: 12.0),
      //                     child: cancelbutton,
      //                   ),
      //                   new Container(
      //                     padding: EdgeInsets.only(left: 14.0),
      //                     child: transferbutton,
      //                   )
      //                 ],
      //               ),
      //             ],
      //           ),
      //         ),
      //       ),
      //     ],
      //   ),
      // ),
    );
  }

  Future<GoTransferResponse> goOpenAccount(url, Map jsonMap) async {
    GoTransferResponse p = new GoTransferResponse();
    var body;
    try {
      body = await http.doPost(url, jsonMap);
      p = GoTransferResponse.fromJson(json.decode(body.toString()));
    } catch (e) {
      p.code = Constants.responseCode_Error;
      p.desc = e.toString();
    }
    print(p.toMap());
    return p;
  }
}
