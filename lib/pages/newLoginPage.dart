import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:nsb/constants/constant.dart';
import 'package:nsb/constants/rout_path.dart' as routes;
import 'package:nsb/framework.dart/popUp.dart';
import 'package:nsb/main.dart';
import 'package:nsb/model/Otp.dart';
import 'package:nsb/model/OtpResponse.dart';
import 'package:nsb/model/WalletResponseData.dart';
import 'package:nsb/framework/http_service.dart' as http;
import 'package:nsb/pages/Wallet.dart';
import 'package:nsb/pages/exchangerate/exchangerate.dart';
import 'package:nsb/pages/faq/faq.dart';
import 'package:nsb/pages/newOtpPage.dart';
import 'package:back_button_interceptor/back_button_interceptor.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:local_auth/local_auth.dart';
import 'package:device_id/device_id.dart';

class NewLoginPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => new NewLoginPageState();
}

class NewLoginPageState extends State<NewLoginPage> {
  final myController = TextEditingController();
  final myPwdController = TextEditingController();
  bool _validate = false;
  bool isLoading = false;
  String _textString = 'Get OTP';
  String alertmsg = "";
  String rkey = "";
  String data = "";
  String checklang = "";
  bool _isLoading;
  String phoneNo = "";
  List textMyan = [
    "ကုဒ်ရယူပါ",
    "ဝင်​ရန်​",
    "တည်​​နေရာ",
    "​ငွေလွှဲနှုန်း",
    "​မေးခွန်း",
    "ဆက်​သွယ်​ရန်​"
  ];
  List textEng = [
    "Get OTP",
    "Login",
    "Location",
    "Exchange Rate",
    "FAQ",
    "Contact Us"
  ];
  String deviceId = "";

  final _formKey = new GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldkey = new GlobalKey<ScaffoldState>();
  static final CREATE_POST_URL =
      'http://122.248.120.16:8080/WalletService/module001';
  final LocalAuthentication _localAuthentication = LocalAuthentication();
  // 3. variable for track whether your device support local authentication means
  //    have fingerprint or face recognization sensor or not
  bool _hasFingerPrintSupport = false;
  // 4. we will set state whether user authorized or not
  String _authorizedOrNot = "Not Authorized";
  // 5. list of avalable biometric authentication supports of your device will be saved in this array
  List<BiometricType> _availableBuimetricType = List<BiometricType>();

  void initState() {
    this._isLoading = false;
    checkLanguage();
    super.initState();
  }

  checkLanguage() async {
    final prefs = await SharedPreferences.getInstance();
    checklang = prefs.getString("Lang");
    // print(lang);
    if (checklang == "" || checklang == null || checklang.length == 0) {
      checklang = "Eng";
    } else {
      checklang = checklang;
    }
    setState(() {});
  }

  void _method1() {
    print("Snack Bar");
    print(this.alertmsg);
    _scaffoldkey.currentState.showSnackBar(new SnackBar(
      content: new Text(this.alertmsg),
      duration: Duration(seconds: 1),
    ));
  }

  @override
  void dispose() {
    myController.dispose();
    super.dispose();
  }

  void _doSomething(String text) {
    setState(() {
      _textString = text;
    });
  }
   Future _login() async{
        setState((){
          isLoading = false;
        });
      }
  Future<void> _getBiometricsSupport() async {
    // 6. this method checks whether your device has biometric support or not
    bool hasFingerPrintSupport = false;
    try {
      hasFingerPrintSupport = await _localAuthentication.canCheckBiometrics;
    } catch (e) {
      print(e);
    }
    if (!mounted) return;
    setState(() {
      _hasFingerPrintSupport = hasFingerPrintSupport;
    });
  }

  Future<void> _getAvailableSupport() async {
    // 7. this method fetches all the available biometric supports of the device
    List<BiometricType> availableBuimetricType = List<BiometricType>();
    try {
      availableBuimetricType =
          await _localAuthentication.getAvailableBiometrics();
    } catch (e) {
      print(e);
    }
    if (!mounted) return;
    setState(() {
      _availableBuimetricType = availableBuimetricType;
    });
  }

  Future<void> _authenticateMe() async {
    // 8. this method opens a dialog for fingerprint authentication.
    //    we do not need to create a dialog nut it popsup from device natively.
    bool authenticated = false;
    try {
      authenticated = await _localAuthentication.authenticateWithBiometrics(
        localizedReason: "Authenticate for Testing", // message for dialog
        useErrorDialogs: true, // show error in dialog
        stickyAuth: true, // native process
      );
    } catch (e) {
      print(e);
    }
    if (!mounted) return;
    setState(() {
      _authorizedOrNot = authenticated ? "Authorized" : "Not Authorized";
      if (_authorizedOrNot == "Authorized") {
        fingerPrintLogin();
      } else {}
    });
  }

  _getDeviceId() async {
    String device_id = await DeviceId.getID;
    deviceId = device_id;
    print(deviceId);
  }

  fingerPrintLogin() async {
    String url =
        "http://122.248.120.16:8080/WalletService/module001/service001/checkDeviceIdV2";
    Map<String, String> headers = {"Content-type": "application/json"};
    String json = '{ "deviceID": "' + deviceId + '"}';
    http.Response response = await http.post(url, headers: headers, body: json);
    int statusCode = response.statusCode;
    print(statusCode);
    if (statusCode == 200) {
      String body = response.body;
      print(body);
      var data = jsonDecode(body);
      print(data);
      if (data["messageCode"] == "0000") {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => WalletPage()));
      } else {
        this.alertmsg = data["messageDesc"];
        this._method1();
        final form = _formKey.currentState;
        form.validate();
        this._isLoading = false;
      }
      setState(() {});
    }
  }

  bool myInterceptor(bool stopDefaultButtonEvent) {
    // Do some stuff.
    return true;
  }

  @override
  Widget build(BuildContext context) {
    final style = TextStyle(fontFamily: 'Montserrat', fontSize: 16.0);
    final loginField = new TextField(
        controller: myController,
        obscureText: false,
        style: style,
        keyboardType: TextInputType.number,
        onChanged: (text) {
          _doSomething(text);
        },
        decoration: InputDecoration(
          prefixIcon: Icon(Icons.account_box),
          contentPadding:
              new EdgeInsets.symmetric(vertical: 9.0, horizontal: 25.0),
          hintText: "+959",
          errorText: _validate ? 'Please enter your phone number ' : null,
          errorStyle: TextStyle(
              fontSize: 14, color: Colors.red, fontWeight: FontWeight.w400),
          hintStyle: TextStyle(
              fontSize: 15,
              color: Color.fromARGB(200, 90, 90, 90),
              fontWeight: FontWeight.w100),
          fillColor: Colors.black,
          border: OutlineInputBorder(
            borderSide: const BorderSide(
                color: Color.fromRGBO(40, 103, 178, 1), width: 1),
            borderRadius: BorderRadius.circular(15.0),
          ),
        ));

    final passwordField = Padding(
        padding: EdgeInsets.fromLTRB(0.0, 20.0, 00.0, 0.0),
        child: SizedBox(
          height: 45.0,
          width: MediaQuery.of(context).size.width,
          child: new RaisedButton(
            elevation: 9.0,
            shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(10.0)),
            child: new Text(
              (checklang == "Eng") ? textEng[0] : textMyan[0],
              style: TextStyle(fontSize: 17, fontWeight: FontWeight.w300),
            ),
            textColor: Colors.white,
            onPressed: () async {
              setState(() {
                isLoading = true;
              });
              this.alertmsg = '';
              print(myController.text);
              setState(() {
                myController.text.isEmpty
                    ? _validate = true
                    : _validate = false;
                phoneNo = myController.text;
                if (phoneNo.indexOf("7") == 0 && phoneNo.length == 9) {
                  phoneNo = '+959' + this.phoneNo;
                } else if (phoneNo.indexOf("9") == 0 && phoneNo.length == 9) {
                  phoneNo = '+959' + phoneNo;
                } else if (phoneNo.indexOf("+") != 0 &&
                    phoneNo.indexOf("7") != 0 &&
                    phoneNo.indexOf("9") != 0 &&
                    (phoneNo.length == 8 ||
                        phoneNo.length == 9 ||
                        phoneNo.length == 7)) {
                  this.phoneNo = '+959' + this.phoneNo;
                } else if (phoneNo.indexOf("09") == 0 &&
                    (phoneNo.length == 10 ||
                        phoneNo.length == 11 ||
                        phoneNo.length == 9)) {
                  phoneNo = '+959' + phoneNo.substring(2);
                } else if (phoneNo.indexOf("959") == 0 &&
                    (phoneNo.length == 11 ||
                        phoneNo.length == 12 ||
                        phoneNo.length == 10)) {
                  phoneNo = '+959' + phoneNo.substring(3);
                }
              });
              print('Phone no: ' + phoneNo);
              myController.text = phoneNo;
              Otp otp =
                  new Otp(userID: myController.text, type: '11', deviceID: '');

              OtpResponse res = await goLoginOTP(
                  CREATE_POST_URL + '/service001/getLoginOTP', otp.toMap());
              if (res.code == Constants.responseCode_Success) {
                setState(() {
                  isLoading = false;
                });
                this.alertmsg = res.desc;
                this._method1();
                this.rkey = res.rKey;
                this._isLoading = true;
                _scaffoldkey.currentState.removeCurrentSnackBar();
                var route = new MaterialPageRoute(
                    builder: (BuildContext context) =>
                        new NewOtpPage(value: myController.text, value1: rkey));
                Navigator.of(context).push(route);
                print(this.rkey);
                print(res.rKey);
                print(res.code);
              } else {
                setState(() {
                  isLoading = true;
                  new Future.delayed(new Duration(seconds: 3), _login);
                });
                this.alertmsg = res.desc;
                this._method1();
                final form = _formKey.currentState;
                form.validate();
                this._isLoading = false;
              }
              
            },
            color: _validate
                ? Color.fromRGBO(40, 103, 178, 1)
                : Color.fromRGBO(40, 103, 178, 1),
          ),
        ));
   
    final fingerprint = Padding(
      padding: EdgeInsets.fromLTRB(0.0, 20.0, 00.0, 0.0),
      child: SizedBox(
        height: 45.0,
        width: MediaQuery.of(context).size.width,
        child: new RaisedButton(
          onPressed: () {
            _getBiometricsSupport();
            _getAvailableSupport();
            BackButtonInterceptor.add(myInterceptor);
            _authenticateMe();
          },
          elevation: 9.0,
          shape: new RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(10.0)),
          child: Icon(
            Icons.fingerprint,
            color: Colors.black,
          ),
        ),
      ),
    );

    final size = MediaQuery.of(context).size;
    var body = new Form(
      key: _formKey,
      child: new ListView(
        children: <Widget>[
          SizedBox(height: 50.0),
          new Container(),
          Container(
            padding: EdgeInsets.all(10.0),
            height: 500,
            child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0),
              ),
              elevation: 8.0,
              child: ListView(
                padding: EdgeInsets.all(5.0),
                children: <Widget>[
                  Center(
                    child: new Container(
                      padding: EdgeInsets.only(left: 30.0, right: 30.0),
                    ),
                  ),
                  SizedBox(height: 10.0),
                  Center(
                    child: new Image.asset(
                      'assets/Picture10.png',
                      width: size.width,
                    ),
                  ),
                  SizedBox(height: 23.0),
                  // Center(
                  //     child: Text("What's Your Mobile Number?",
                  //         style: TextStyle(
                  //             fontSize: 25.0,
                  //             fontWeight: FontWeight.w500,
                  //             ),
                  //             textAlign: TextAlign.center,
                  //             )),
                  // SizedBox(height: 15.0),
                  Center(
                      child: Text(
                    "One-Time Password(OTP) will be sent to this phone number.",
                    style: TextStyle(
                        fontSize: 16.0,
                        color: Colors.grey,
                        fontWeight: FontWeight.w300),
                    textAlign: TextAlign.center,
                  )),
                  SizedBox(height: 27.0),
                  Center(
                    child: new Container(
                      padding: EdgeInsets.only(left: 30.0, right: 30.0),
                      child: loginField,
                    ),
                  ),
                  SizedBox(height: 20.0),
                  Center(
                    child: Row(
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.fromLTRB(30.0, 0.0, 0.0, 0.0),
                          width: 230,
                          child: passwordField,
                        ),
                        Container(
                          padding: EdgeInsets.fromLTRB(40.0, 0.0, 5.0, 0.0),
                          width: 100,
                          child: fingerprint,
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 24.0),
                ],
              ),
            ),
          ),
        ],
      ),
    );
    var bodyProgress = new Container(
      child: new Stack(
        children: <Widget>[
          body,
          Container(
              decoration: new BoxDecoration(
                color: Color.fromRGBO(255, 255, 255, 0.5),
              ),
              width: MediaQuery.of(context).size.width * 0.99,
              height: MediaQuery.of(context).size.height * 0.9,
              child: Center(
                child: CircularProgressIndicator(
                  backgroundColor: Colors.amber,
                ),
              ))
        ],
      ),
    );

    return new MaterialApp(
      debugShowCheckedModeBanner: false,
      home: new Scaffold(
        key: _scaffoldkey,
        appBar: new AppBar(
          elevation: 0.0,
          backgroundColor: Color.fromRGBO(40, 103, 178, 1),
          title: new Center(
            child: new Text(
              'NSB',
              style: TextStyle(
                  fontSize: 20.0,
                  color: Colors.white,
                  height: 1.0,
                  fontWeight: FontWeight.w500),
            ),
          ),
          actions: <Widget>[
            PopupMenuButton<String>(
              offset: Offset(0, 45),
              onSelected: (result) async {
                // setState(() { var selected = result; });
                var selected = result;
                final prefs = await SharedPreferences.getInstance();
                if (selected == "Eng") {
                  setState(() {
                    checklang = "Eng";
                    prefs.setString("Lang", checklang);
                  });
                } else if (selected == "Myan") {
                  setState(() {
                    checklang = "Myan";
                    prefs.setString("Lang", checklang);
                  });
                } else {
                  print(selected);
                }
                // setState(() {
                //   selected = result;
                // });
              },
              itemBuilder: (BuildContext context) {
                return [
                  PopupMenuItem(
                    value: 'Eng',
                    child: Row(
                      children: <Widget>[
                        Icon(
                          Icons.info,
                        ),
                        Container(
                          padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
                          child: Text("English"),
                        )
                      ],
                    ),
                  ),
                  PopupMenuItem(
                    value: 'Myan',
                    child: Row(
                      children: <Widget>[
                        Icon(
                          Icons.info,
                        ),
                        Container(
                          padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
                          child: Text("မြန်မာ"),
                        )
                      ],
                    ),
                  ),
                ];
              },
            )
          ],
        ),
        body: isLoading ? bodyProgress : body,
        // body: new Form(
        //   key: _formKey,
        //   child: new ListView(
        //     children: <Widget>[
        //       SizedBox(height: 50.0),
        //       new Container(),
        //       Container(
        //         padding: EdgeInsets.all(10.0),
        //         height: 500,
        //         child: Card(
        //           shape: RoundedRectangleBorder(
        //             borderRadius: BorderRadius.circular(15.0),
        //           ),
        //           elevation: 8.0,
        //           child: ListView(
        //             padding: EdgeInsets.all(5.0),
        //             children: <Widget>[
        //               Center(
        //                 child: new Container(
        //                   padding: EdgeInsets.only(left: 30.0, right: 30.0),
        //                 ),
        //               ),
        //               SizedBox(height: 10.0),
        //               Center(
        //                 child: new Image.asset(
        //                   'assets/Picture10.png',
        //                   width: size.width,
        //                 ),
        //               ),
        //               SizedBox(height: 23.0),
        //               // Center(
        //               //     child: Text("What's Your Mobile Number?",
        //               //         style: TextStyle(
        //               //             fontSize: 25.0,
        //               //             fontWeight: FontWeight.w500,
        //               //             ),
        //               //             textAlign: TextAlign.center,
        //               //             )),
        //               // SizedBox(height: 15.0),
        //               Center(
        //                   child: Text(
        //                 "One-Time Password(OTP) will be sent to this phone number.",
        //                 style: TextStyle(
        //                     fontSize: 16.0,
        //                     color: Colors.grey,
        //                     fontWeight: FontWeight.w300),
        //                 textAlign: TextAlign.center,
        //               )),
        //               SizedBox(height: 27.0),
        //               Center(
        //                 child: new Container(
        //                   padding: EdgeInsets.only(left: 30.0, right: 30.0),
        //                   child: loginField,
        //                 ),
        //               ),
        //               SizedBox(height: 20.0),
        //               Center(
        //                 child: Row(
        //                   children: <Widget>[
        //                     Container(
        //                       padding: EdgeInsets.fromLTRB(30.0, 0.0, 0.0, 0.0),
        //                       width: 230,
        //                       child: passwordField,
        //                     ),
        //                     Container(
        //                       padding: EdgeInsets.fromLTRB(40.0, 0.0, 5.0, 0.0),
        //                       width: 100,
        //                       child: fingerprint,
        //                     ),
        //                   ],
        //                 ),
        //               ),
        //               SizedBox(height: 24.0),
        //             ],
        //           ),
        //         ),
        //       ),
        //     ],
        //   ),
        // ),
        drawer: _drawer(),
      ),
    );
  }

  Widget _showCircularProgress() {
    if (_isLoading) {
      return Center(child: CircularProgressIndicator());
    }
    return Container(
      height: 0.0,
      width: 0.0,
    );
  }

  Widget _drawer() {
    return Container(
      color: Colors.white,
      child: new SizedBox(
          width: MediaQuery.of(context).size.width * 0.75,
          child: new ListView(
            children: <Widget>[
              new Padding(
                padding: EdgeInsets.only(top: 30.0),
              ),
              _Login(),
              new Padding(
                padding: EdgeInsets.only(top: 20.0),
              ),
              _Location(),
              new Padding(
                padding: EdgeInsets.only(top: 20.0),
              ),
              _ExchangeRate(),
              new Padding(
                padding: EdgeInsets.only(top: 20.0),
              ),
              _Faq(),
              new Padding(
                padding: EdgeInsets.only(top: 20.0),
              ),
              _ContactUs(),
              new Padding(
                padding: EdgeInsets.only(top: 20.0),
              ),
            ],
          )),
    );
  }

  Widget _Login() {
    return Padding(
      padding: const EdgeInsets.only(left: 30.0, right: 30.0),
      child: Container(
        height: 50.0,
        child: new RaisedButton(
          shape: RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(10.0),
              side: BorderSide(color: Colors.white)),
          // onPressed: () async {
          //   Navigator.of(context).pushNamed(routes.NewLoginPageRoute);
          // },
          onPressed: () {
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => NewLoginPage()));
          },
          color: Color.fromRGBO(40, 103, 178, 1),
          textColor: Colors.white,
          child: Row(
            children: <Widget>[
              new Padding(
                padding: EdgeInsets.only(
                  left: 10.0,
                ),
              ),
              Icon(
                Icons.lock,
                color: Colors.white,
              ),
              new Padding(
                  padding: EdgeInsets.only(
                right: 15.0,
              )),
              (checklang == "Eng")
                  ? Text(
                      (checklang == "Eng") ? textEng[1] : textMyan[1],
                      style: TextStyle(fontSize: 18),
                    )
                  : Text(
                      (checklang == "Eng") ? textEng[1] : textMyan[1],
                      style: TextStyle(fontSize: 16),
                    ),
            ],
          ), //new Text('Account Summery'),
        ),
      ),
    );
  }

  Widget _Faq() {
    return Padding(
      padding: const EdgeInsets.only(left: 30.0, right: 30.0),
      child: Container(
        height: 50.0,
        child: new RaisedButton(
          shape: RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(10.0),
              side: BorderSide(color: Colors.white)),
          onPressed: () {
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => FAQ()));
          },
          color: Color.fromRGBO(40, 103, 178, 1),
          textColor: Colors.white,
          child: Row(
            children: <Widget>[
              new Padding(
                  padding: EdgeInsets.only(
                left: 10.0,
              )),
              Icon(
                Icons.message,
                color: Colors.white,
              ),
              new Padding(
                  padding: EdgeInsets.only(
                right: 15.0,
              )),
              Text(
                checklang == "Eng" ? textEng[4] : textMyan[4],
                style:
                    TextStyle(color: Colors.white, fontWeight: FontWeight.w400),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _Location() {
    return Padding(
      padding: const EdgeInsets.only(left: 30.0, right: 30.0),
      child: Container(
        height: 50.0,
        child: new RaisedButton(
          shape: RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(10.0),
              side: BorderSide(color: Colors.white)),
          onPressed: () async {
            Navigator.of(context).pushNamed(routes.LocationPageRoute);
          },
          color: Color.fromRGBO(40, 103, 178, 1),
          textColor: Colors.white,
          child: Row(
            children: <Widget>[
              new Padding(
                  padding: EdgeInsets.only(
                left: 10.0,
              )),
              Icon(
                Icons.location_on,
                color: Colors.white,
              ),
              new Padding(
                  padding: EdgeInsets.only(
                right: 15.0,
              )),
              Text(
                checklang == "Eng" ? textEng[2] : textMyan[2],
                style:
                    TextStyle(color: Colors.white, fontWeight: FontWeight.w400),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _ExchangeRate() {
    return Padding(
      padding: const EdgeInsets.only(left: 30.0, right: 30.0),
      child: Container(
        height: 50.0,
        child: new RaisedButton(
          shape: RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(10.0),
              side: BorderSide(color: Colors.white)),
          onPressed: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => Exchanerate()));
          },
          color: Color.fromRGBO(40, 103, 178, 1),
          textColor: Colors.white,
          child: Row(
            children: <Widget>[
              new Padding(
                  padding: EdgeInsets.only(
                left: 10.0,
              )),
              Icon(
                Icons.money_off,
                color: Colors.white,
              ),
              new Padding(
                  padding: EdgeInsets.only(
                right: 15.0,
              )),
              Text(
                checklang == "Eng" ? textEng[3] : textMyan[3],
                style:
                    TextStyle(color: Colors.white, fontWeight: FontWeight.w400),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _ContactUs() {
    return Padding(
      padding: const EdgeInsets.only(left: 30.0, right: 30.0),
      child: Container(
        height: 50.0,
        child: new RaisedButton(
          shape: RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(10.0),
              side: BorderSide(color: Colors.white)),
          onPressed: () async {
            Navigator.of(context).pushNamed(routes.ContactPageRoute);
          },
          color: Color.fromRGBO(40, 103, 178, 1),
          textColor: Colors.white,
          child: Row(
            children: <Widget>[
              new Padding(
                padding: EdgeInsets.only(
                  left: 10.0,
                ),
              ),
              Icon(
                Icons.import_contacts,
                color: Colors.white,
              ),
              new Padding(
                  padding: EdgeInsets.only(
                right: 15.0,
              )),
              Text(
                checklang == "Eng" ? textEng[5] : textMyan[5],
                style:
                    TextStyle(color: Colors.white, fontWeight: FontWeight.w400),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<WalletResponseData> goLogin(url, Map jsonMap) async {
    WalletResponseData p = new WalletResponseData();
    var body;
    try {
      body = await http.doPost(url, jsonMap);
      p = WalletResponseData.fromJson(json.decode(body.toString()));
    } catch (e) {
      p.messageCode = Constants.responseCode_Error;
      p.messageDesc = e.toString();
    }
    print(p.toMap());
    return p;
  }

  Future<OtpResponse> goLoginOTP(url, Map jsonMap) async {
    OtpResponse p = new OtpResponse();
    var body;
    try {
      body = await http.doPost(url, jsonMap);
      p = OtpResponse.fromJson(json.decode(body.toString()));
    } catch (e) {
      p.code = Constants.responseCode_Error;
      p.desc = e.toString();
    }
    print(p.toMap());
    return p;
  }
}
