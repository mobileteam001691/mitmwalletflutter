import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'exchangerateRequest.dart';
import 'exchangerateResponse.dart';

class Exchanerate extends StatefulWidget {
  @override
  _ExchanerateState createState() => _ExchanerateState();
}

class _ExchanerateState extends State<Exchanerate> {
  List fromList = [];
  List toList = [];
  List sellList = [];
  List buyList = [];
  List currList = [];
  List searchList = [];
  List allData = [];
  String userID;
  String sId;
  bool isLoaing = true;

  TextEditingController searchController = TextEditingController();
  String checklang = '';
  List textMyan = ["နိုင်ငံခြားငွေလွှဲနှုန်း"];
  List textEng = ["Foreign Exchange Rate"];

  @override
  void initState() {
    checkLanguage();
    setState(() {
      getexchangeList();
    });
    super.initState();
  }

  getexchangeList() async {
    final url =
        'http://122.248.120.16:8080/AppService/module001/service001/getForeignExchangeRates';
    var body = jsonEncode({"userId": '', "sessionID": ''});

    http.post(Uri.encodeFull(url), body: body, headers: {
      "Accept": "application/json",
      "content-type": "application/json"
    }).then((dynamic res) {
      var data = json.decode(res.body);
      var result = data['data'];
      if (data['code'] == "0000") {
        isLoaing = false;
        for (var a = 0; result.length > a; a++) {
          allData.add(result[a]);
          fromList.add(result[a]["ccy1"]);
          toList.add(result[a]["ccy2"]);
          sellList.add(result[a]["numSellRate"]);
          buyList.add(result[a]["numBuyRate"]);
          currList.add(result[a]["namCURRTo"]);
        }
        setState(() {});
      } else {
        isLoaing = true;
      }
    });
  }

  checkLanguage() async {
    final prefs = await SharedPreferences.getInstance();
    checklang = prefs.getString("Lang");
    // print(lang);
    if (checklang == "" || checklang == null || checklang.length == 0) {
      checklang = "Eng";
    } else {
      checklang = checklang;
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(40, 103, 178, 1),
        centerTitle: true,
          title: (checklang == "Eng")
            ? Text(
                (checklang == "Eng") ? textEng[0] : textMyan[0],
                style: TextStyle(fontSize: 20, color: Colors.white),
              )
            : Text(
                (checklang == "Eng") ? textEng[0] : textMyan[0],
                style: TextStyle(fontSize: 18, color: Colors.white),
              ),
        bottom: PreferredSize(
          preferredSize: const Size.fromHeight(70.0),
          child: Container(
            color: Colors.white,
            child: new Padding(
              padding: EdgeInsets.all(8.0),
              child: Card(
                elevation: 5,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15.0),
                ),
                child: Padding(
                  padding: const EdgeInsets.only(left: 20.0),
                  child: new TextField(
                    onChanged: searchResult,
                    controller: searchController,
                    decoration: InputDecoration(
                      icon: new Icon(Icons.search),
                      hintText: 'Search',
                      focusedBorder: InputBorder.none,
                      border: InputBorder.none,
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
      body: isLoaing
          ? Center(
              child: CircularProgressIndicator(
                backgroundColor: Colors.amber,
              ),
            )
          : Padding(
              padding: const EdgeInsets.all(10.0),
              child: (searchList.length == 0 || searchController.text.isEmpty)
                  ? ListView.separated(
                      separatorBuilder: (context, index) => Row(
                            children: <Widget>[
                              Text(
                                "--------------------------",
                                style: TextStyle(color: Colors.white),
                              ),
                              Expanded(
                                child: new Container(
                                    margin: const EdgeInsets.only(
                                        left: 0.0, right: 10.0),
                                    child: Divider(
                                      color: Colors.grey,
                                    )),
                              ),
                            ],
                          ),
                      itemCount: allData.length,
                      itemBuilder: (context, index) {
                        return SizedBox(
                          height: 70,
                          child: ListTile(
                            leading: ClipRRect(
                                borderRadius: BorderRadius.circular(20.0),
                                child: Image.asset(
                                  'assets/flag/' + fromList[index] + '.jpg',
                                  width: 80,
                                  height: 200,
                                  fit: BoxFit.cover,
                                )),
                            title:
                                Text(fromList[index] + ' - ' + toList[index]),
                            subtitle: Text('Buy Rate - ' +
                                buyList[index] +
                                ' ' +
                                currList[index] +
                                '\n' +
                                'Sell Rate - ' +
                                sellList[index] +
                                ' ' +
                                currList[index]),
                          ),
                        );
                      })
                  : ListView.separated(
                      separatorBuilder: (context, index) => Row(
                            children: <Widget>[
                              Text(
                                "--------------------------",
                                style: TextStyle(color: Colors.white),
                              ),
                              Expanded(
                                child: new Container(
                                    margin: const EdgeInsets.only(
                                        left: 0.0, right: 10.0),
                                    child: Divider(
                                      color: Colors.grey,
                                    )),
                              ),
                            ],
                          ),
                      itemCount: searchList.length,
                      itemBuilder: (context, index) {
                        return SizedBox(
                          height: 70,
                          child: ListTile(
                            leading: ClipRRect(
                                borderRadius: BorderRadius.circular(20.0),
                                child: Image.asset(
                                  'assets/flag/' +
                                      searchList[index]["ccy1"] +
                                      '.jpg',
                                  width: 80,
                                  height: 200,
                                  fit: BoxFit.cover,
                                )),
                            title: Text(searchList[index]["ccy1"] +
                                ' - ' +
                                searchList[index]["ccy2"]),
                            subtitle: Text('Buy Rate - ' +
                                searchList[index]["numBuyRate"] +
                                ' ' +
                                searchList[index]["namCURRTo"] +
                                '\n' +
                                'Sell Rate - ' +
                                searchList[index]["numSellRate"] +
                                ' ' +
                                searchList[index]["namCURRTo"]),
                          ),
                        );
                      })),
    );
  }

  searchResult(String text) async {
    searchList.clear();
    text = text.toUpperCase();
    if (text.isNotEmpty) {
      fromList.forEach((name) {
        if (name.contains(text)) {
          for (var a = 0; a < allData.length; a++) {
            print("ccy1 " + allData[a]["ccy1"]);
            if (allData[a]["ccy1"] == name) {
              searchList.add(allData[a]);
            }
            setState(() {});
          }
        }
      });

      print("no data found");
    }
  }
}
