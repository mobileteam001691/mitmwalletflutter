import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:nsb/pages/billTabPage.dart';
import 'package:nsb/pages/contactsTab.dart';
import 'package:nsb/pages/skyNet/skynetPage.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SkynetBeneficiary extends StatefulWidget {
  @override
  _SkynetBeneficiaryState createState() => _SkynetBeneficiaryState();
}

class _SkynetBeneficiaryState extends State<SkynetBeneficiary> {
  String alertmsg = "";
  final _formKey = new GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldkey = new GlobalKey<ScaffoldState>();
  List contactList=new List();
  String checklang = '';
  bool isLoading = true;
  List textMyan = ["ငွေပေးချေမှု"];
  List textEng = ["Skynet Card Beneficiary"];
  void initState() {
    super.initState();
    checkLanguage();
    getCardno();
  }
   void _method1(){
    print("Snack Bar");
    print(this.alertmsg);
    _scaffoldkey.currentState.showSnackBar(new SnackBar(content: new Text(this.alertmsg),duration: Duration(seconds: 1)));
  }
  getCardno() async {
    final prefs = await SharedPreferences.getInstance();
    String userID=prefs.getString('userId');
    String sessionID=prefs.getString('sessionID');
    String url = "http://122.248.120.16:8080/WalletService/module001/payment/getCardNo";
    Map<String, String> headers = {"Content-type": "application/json"};
    String json = '{ "userID": "' + userID + '", "sessionID":"' + sessionID + '" }';
    http.Response response = await http.post(url, headers: headers, body: json);
    int statusCode = response.statusCode;
    if (statusCode == 200) {
      String body = response.body;
      print(body);
      var data = jsonDecode(body);
      print(data);
      setState(() {
        isLoading=false;
        contactList=data["cadData"];
        this.alertmsg=data["desc"];
        this._method1();
      });
      print(contactList);
    }
    else {
      print("Connection Fail");
      setState(() {
        isLoading=true;
      });
    }
  }

  checkLanguage() async {
    final prefs = await SharedPreferences.getInstance();
    checklang = prefs.getString("Lang");
    // print(lang);
    if (checklang == "" || checklang == null || checklang.length == 0) {
      checklang = "Eng";
    } else {
      checklang = checklang;
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
     void getCardNo(String cardNo) async {
       print("CardNo "+cardNo);
      Navigator.push(context, MaterialPageRoute(builder: (context)=>skynetPage(value:cardNo)));
    }

    var skynetbody = ListView.separated(
         separatorBuilder: (context, index) => Divider(
        color: Colors.black,
      ),
         key: _formKey,
              itemCount: contactList==null? 0:contactList.length,
              itemBuilder: (BuildContext context,int index){
                return ListTile(
                  leading:CircleAvatar(
                  radius: 30.0,
                  child: Text("${contactList[index]["name"]}".substring(0,1)),),
                  onTap: (){
                   getCardNo(contactList[index]["cardNo"]);

                  },
                  contentPadding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
                  title:Text("${contactList[index]["name"]}",style: TextStyle(fontSize: 16.0 ,fontWeight:FontWeight.bold,color: Colors.black)) ,
                  subtitle: Text("${contactList[index]["cardNo"]}"),
                );
              },
            );

     var bodyProgress = new Container(
      child: new Stack(
        children: <Widget>[
          skynetbody,
          Container(
              decoration: new BoxDecoration(
                color: Color.fromRGBO(255, 255, 255, 0.5),
              ),
              width: MediaQuery.of(context).size.width * 0.99,
              height: MediaQuery.of(context).size.height * 0.9,
              child: Center(
                child: CircularProgressIndicator(
                  backgroundColor: Colors.amber,
                ),
              ))
        ],
      ),
    );
    return Scaffold(
      key: _scaffoldkey,
        backgroundColor: Colors.white,
        appBar: new AppBar(//Application Bar
        elevation: 0.0,
        backgroundColor:Color.fromRGBO(40, 103, 178, 1),
        title: Center(child: Text(checklang == "Eng" ? textEng[0] : textMyan[0], style: TextStyle(
        fontSize: 20.0,
        color: Colors.white,
        height: 1.0,
        fontWeight: FontWeight.w600),),)),
        body: isLoading ? bodyProgress : skynetbody
      //  body:ListView.separated(
      //    separatorBuilder: (context, index) => Divider(
      //   color: Colors.black,
      // ),
      //    key: _formKey,
      //         itemCount: contactList==null? 0:contactList.length,
      //         itemBuilder: (BuildContext context,int index){
      //           return ListTile(
      //             leading:CircleAvatar(
      //             radius: 30.0,
      //             child: Text("${contactList[index]["name"]}".substring(0,1)),),
      //             onTap: (){
      //              getCardNo(contactList[index]["cardNo"]);

      //             },
      //             contentPadding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
      //             title:Text("${contactList[index]["name"]}",style: TextStyle(fontSize: 16.0 ,fontWeight:FontWeight.bold,color: Colors.black)) ,
      //             subtitle: Text("${contactList[index]["cardNo"]}"),
      //           );
      //         },
      //       ),
    );
  }
}