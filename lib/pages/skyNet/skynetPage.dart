import 'package:flutter/material.dart';
import 'package:nsb/pages/skyNet/skynetBeneficiary.dart';
import 'package:nsb/pages/skyNet/skynetConfirm.dart';
import 'package:nsb/pages/skyNet/skynetDetailPage.dart';
import 'package:shared_preferences/shared_preferences.dart';

class skynetPage extends StatefulWidget {
  final String value;
  final String value1;
  final String value2;
  skynetPage({Key key, this.value, this.value1, this.value2}) : super(key: key);
  @override
  _skynetPageState createState() => _skynetPageState();
}

class _skynetPageState extends State<skynetPage> {
  String checklang = '';
  List textMyan = ["Skynet ငွေပေးချေမှု", "ကဒ်နံပါတ်"];
  List textEng = ["Skynet Payment", "Card No"];
  final myControllerno = TextEditingController();
  @override
  void initState() {
    checkLanguage();
    super.initState();
    myControllerno.text = "${widget.value}";
  }

  checkLanguage() async {
    final prefs = await SharedPreferences.getInstance();
    checklang = prefs.getString("Lang");
    // print(lang);
    if (checklang == "" || checklang == null || checklang.length == 0) {
      checklang = "Eng";
    } else {
      checklang = checklang;
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    final passwordField2 = new TextFormField(
      controller: myControllerno,
      style: TextStyle(
          fontFamily: 'Montserrat', fontSize: 19.0, color: Colors.black),
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
          labelText: (checklang == "Eng") ? textEng[1] : textMyan[1],
          hasFloatingPlaceholder: true,
          labelStyle: (checklang == "Eng")
            ? TextStyle(fontSize: 18, color: Colors.black,height: 0)
            : TextStyle(fontSize: 17, color: Colors.black, height:  0),
          fillColor: Colors.black87,
          suffixIcon: IconButton(
            padding: EdgeInsets.all(0),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) =>
                        skynetDetailPage(value: myControllerno.text)),
              );
            },
            iconSize: 25,
            icon: Icon(
              Icons.send,
              color: Color.fromRGBO(40, 103, 178, 1),
              size: 30,
            ),
          )),
    );

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: new AppBar(
        //Application Bar
        centerTitle: true,
        elevation: 0.0,
        backgroundColor: Color.fromRGBO(40, 103, 178, 1),
        title: Text(
          (checklang == "Eng") ? textEng[0] : textMyan[0],
          textAlign: TextAlign.center,
          style: TextStyle(color: Colors.white),
        ),
        actions: <Widget>[
          IconButton(
            padding: EdgeInsets.all(0),
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => SkynetBeneficiary()));
            },
            iconSize: 25,
            icon: Icon(
              Icons.view_list,
              color: Colors.white,
            ),
          )
        ],
      ),
      body: new Form(
        child: new ListView(
          children: <Widget>[
            SizedBox(height: 10.0),
            Container(
              padding: EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 0.0),
              height: 130,
              child: Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                elevation: 3.0,
                child: ListView(
                  padding: EdgeInsets.all(5.0),
                  children: <Widget>[
                    SizedBox(height: 20.0),
                    Center(
                      child: new Container(
                        padding: EdgeInsets.only(left: 22.0, right: 10.0),
                        child: passwordField2,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              height: 550,
              width: 500,
              child: new Image.asset('assets/images/skynetinfo3.png'),
            ),
          ],
        ),
      ),
    );
  }
}
