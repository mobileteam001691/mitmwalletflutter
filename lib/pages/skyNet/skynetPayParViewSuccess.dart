import 'package:flutter/material.dart';
import 'package:nsb/pages/Wallet.dart';

class SkynetPayParViewSuccess extends StatefulWidget {
  final String value;
  final String value1;
  final String value2;
  final String value3;
  final String value4;
  final String value5;

  SkynetPayParViewSuccess({Key key,this.value,this.value1,this.value2,this.value3,this.value4,this.value5}) : super(key: key);
  @override
  _DetailsState createState() => _DetailsState();
}

class _DetailsState extends State<SkynetPayParViewSuccess> {
  double c;
  @override
  void initState() {
    super.initState();
    plus();
  }
  plus(){
    double a=double.parse("${widget.value3}");
    double b=double.parse("${widget.value4}");
    c=a+b;
    print(c);
  }
  @override
  Widget build(BuildContext context) {
    final style =TextStyle(fontFamily:'Montserrat', fontSize:19.0,color: Colors.black);
    return MaterialApp(
       debugShowCheckedModeBanner: false,
       title: 'Title is here',
       home: Scaffold(
      appBar:AppBar(
        title: Center(child: new Text("Skynet Payment Success")),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(15.0),
          child: Column(
            children: <Widget>[
              SizedBox(
                width: MediaQuery.of(context).size.width * 0.91,
                height: 100,
                child: Container(
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.white),
                    borderRadius: BorderRadius.only(topRight: Radius.circular(15),topLeft: Radius.circular(15)),
                    color: Colors.green,
                  ),
                  child: Padding(
                    padding: const EdgeInsets.only(top: 30.0),
                    child: Text('Transaction with Reference Number\n ${widget.value} is in Accepted state.',textAlign: TextAlign.center,style: TextStyle(fontWeight: FontWeight.w900),),
                  ),
                  // height: MediaQuery.of(context).size.height * 1,
                  // width: MediaQuery.of(context).size.width * 1,
                ),
              ),
              SizedBox(
                width: MediaQuery.of(context).size.width * 0.91,
                // height: 80,
                child: Container(
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.grey[300]),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top:15.0,bottom:8.0,left: 10),
                      child: Text('Bank Reference No.',style: TextStyle(fontSize: 15,fontWeight: FontWeight.w300,),),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom:15.0,left: 10),
                      child: Text("${widget.value}",style: style,),
                    ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                width: MediaQuery.of(context).size.width * 0.91,
                // height: 80,
                child: Container(
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.grey[300]),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top:15.0,bottom:8.0,left: 10),
                      child: Text('Card No.',style: TextStyle(fontSize: 15,fontWeight: FontWeight.w300,),),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom:15.0,left: 10),
                      child: Text("${widget.value1}",style: style,),
                    ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                width: MediaQuery.of(context).size.width * 0.91,
                // height: 80,
                child: Container(
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.grey[300]),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top:15.0,bottom:8.0,left: 10),
                      child: Text('Movie Name',style: TextStyle(fontSize: 15,fontWeight: FontWeight.w300,),),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom:15.0,left: 10),
                      child: Text("${widget.value2}",style: style,),
                    ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                width: MediaQuery.of(context).size.width * 0.91,
                // height: 80,
                child: Container(
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.grey[300]),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top:15.0,bottom:8.0,left: 10),
                      child: Text('Amount',style: TextStyle(fontSize: 15,fontWeight: FontWeight.w300,)),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom:15.0,left: 10),
                      child: Text("${widget.value3}",style: style,),
                    ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                width: MediaQuery.of(context).size.width * 0.91,
                // height: 80,
                child: Container(
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.grey[300]),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top:15.0,bottom:8.0,left: 10),
                      child: Text('Bank Charges',style: TextStyle(fontSize: 15,fontWeight: FontWeight.w300,)),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom:15.0,left: 10),
                      child: Text("${widget.value4}",style: style,),
                    ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                width: MediaQuery.of(context).size.width * 0.91,
                // height: 80,
                child: Container(
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.grey[300]),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top:15.0,bottom:8.0,left: 10),
                      child: Text('Total Amount',style: TextStyle(fontSize: 15,fontWeight: FontWeight.w300,)),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom:15.0,left: 10),
                      child: Text("$c",style: style,),
                    ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                width: MediaQuery.of(context).size.width * 0.91,
                // height: 80,
                child: Container(
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.grey[300]),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top:15.0,bottom:8.0,left: 10),
                      child: Text('Transaction Date',style: TextStyle(fontSize: 15,fontWeight: FontWeight.w300,)),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom:15.0,left: 10),
                      child: Text("${widget.value5}",style: style,),
                    ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: BottomAppBar(
      child: new Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            width: MediaQuery.of(context).size.width * 0.90,
            child: RaisedButton(
              onPressed: (){
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) =>WalletPage()),
                  );
              },
              child: Text('CLOSE',style: TextStyle(fontSize: 17,color: Colors.white,fontWeight: FontWeight.w300,)),
              color: Colors.blue,
              textColor: Colors.white,
            ),
          ),
        ],
      ),
      ),
    ),
    );
  }
}