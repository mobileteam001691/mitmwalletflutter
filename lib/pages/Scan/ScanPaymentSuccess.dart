import 'package:flutter/material.dart';
import 'package:nsb/pages/Wallet.dart';

class ScanPaymentSuccess extends StatefulWidget {
  final String value;
  final String value1;
  final String value2;
  final String value3;

  ScanPaymentSuccess({Key key, this.value,this.value1,this.value2,this.value3}) : super(key: key);

  @override
  _MeterBillSuccessPageState createState() => _MeterBillSuccessPageState();
}

class _MeterBillSuccessPageState extends State<ScanPaymentSuccess> {

Widget build(BuildContext context) {

final style =TextStyle(fontFamily:'Montserrat', fontSize:16.0);

final titleField = new Container(
      child: Row(
        children: <Widget> [
             Text("              Payment Successful",style: TextStyle(fontSize: 19, color: Colors.white,),textAlign: TextAlign.center,),
        ]
      )
    );

final loginField = new Container(
      child: Column(
        children: <Widget> [
          ListTile(
            title: Padding(
              padding: const EdgeInsets.fromLTRB(8, 2, 8, 15),
              child: Text("Transaction No.",style: TextStyle(fontSize: 16),),
            ),  
                     
            subtitle: Padding(
              padding: const EdgeInsets.fromLTRB(8.0, 3.5, 0.0, 0.0),
              child: Text("${widget.value1}",style: TextStyle(fontSize: 18,color: Colors.black)),
            ),            
          ),
          Divider(color: Colors.black,)
        ]
      )
    );
 
final passwordField = new Container(
      child: Column(
        children: <Widget> [
          ListTile(
            title: Padding(
              padding: const EdgeInsets.fromLTRB(8, 2, 8, 15),
              child: Text("Name",style: TextStyle(fontSize: 16,color: Colors.blueGrey),),
            ),  
                     
            subtitle: Padding(
              padding: const EdgeInsets.fromLTRB(8.0, 3.5, 0.0, 0.0),
              child: Text("${widget.value3}",style: TextStyle(fontSize: 18,color: Colors.black)),
            ),            
          ),
          Divider(color: Colors.black,)
        ]
      )
    );

final passwordField2 = new Container(
  child: Column(
        children: <Widget> [
          ListTile(
            title: Padding(
              padding: const EdgeInsets.fromLTRB(8, 2, 8, 15),
              child: Text("Amount",style: TextStyle(fontSize: 16,color: Colors.blueGrey),),
            ),  
                     
            subtitle: Padding(
              padding: const EdgeInsets.fromLTRB(8.0, 3.5, 0.0, 0.0),
              child: Text("${widget.value}"+".00 MMK",style: TextStyle(fontSize: 18,color: Colors.black)),
            ),            
          ),
          Divider(color: Colors.black,)
        ]
      )
    );

final transactiondate = new Container(
  child: Column(
        children: <Widget> [
          ListTile(
            title: Padding(
              padding: const EdgeInsets.fromLTRB(8, 2, 8, 15),
              child: Text("Transaction Date",style: TextStyle(fontSize: 16,color: Colors.blueGrey),),
            ),  
            subtitle: Padding(
              padding: const EdgeInsets.fromLTRB(8.0, 3.5, 0.0, 0.0),
              child: Text("${widget.value2}",style: TextStyle(fontSize: 18,color: Colors.black)),
            ),            
          ),
          Divider(color: Colors.black,)
        ]
      )
    );

final transferbutton = new RaisedButton(
  onPressed: (){
    Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => WalletPage()),
       );
  },
  color: Color.fromRGBO(40, 103, 178, 1),
      shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(10.0),),
      textColor:Colors.white,
      child:Container(
        width:300.0,
        height:43.0,
        child:Center(child:Text('Close',style: TextStyle(fontSize: 18,color: Colors.white)) ),  
      ),
    );

return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: new AppBar(//Application Bar
        elevation: 0.0,
        backgroundColor:Color.fromRGBO(40, 103, 178, 1),
        title: Center(child: Text('Transfer Success', style: TextStyle(
        fontSize: 20.0,
        color: Colors.white,
        height: 1.0,
        fontWeight: FontWeight.w600),
        ),) 
        ),
        body: new Form(
              child: new ListView(
                children: <Widget>[
                  SizedBox(height: 10.0),

            Container(
              
              padding: EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 0.0),
              height: 550,
              child: Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),),
                elevation: 3.0,
                child: ListView(
                  padding: EdgeInsets.all(5.0),
                  children: <Widget>[
                    Center(
                      child: new Container(
                        height: 80,
                        color: Colors.green[500],
                        padding: EdgeInsets.only(left: 30.0, right: 10.0),
                        child: titleField,
                      ),
                    ),
                    SizedBox(height: 5.0),
                    Center(
                      child: new Container(
                        padding: EdgeInsets.only(left: 10.0, right: 10.0),
                        child: loginField,
                      ),
                    ),
                    SizedBox(height: 5.0),
                    Center(
                     child: new Container(
                        padding: EdgeInsets.only(left: 10.0, right: 10.0),
                        child: passwordField,
                      ),
                    ),
                     SizedBox(height: 5.0),
                    Center(
                     child: new Container(
                        padding: EdgeInsets.only(left: 10.0, right: 10.0),
                        child: passwordField2,
                      ),
                    ),
                    
                    SizedBox(height: 5.0),
                    Center(
                     child: new Container(
                        padding: EdgeInsets.only(left: 10.0, right: 10.0),
                        child: transactiondate,
                      ),
                    ),
                    SizedBox(height: 20.0),
                    Center(
                      child: new Container(
                        
                        padding: EdgeInsets.only(left: 30.0, right: 30.0),
                        child: transferbutton,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],                
        ),
      ),
    );
  }
}



