import 'package:flutter/material.dart';
import 'package:nsb/constants/rout_path.dart' as routes;
import 'package:nsb/pages/Scan/GenerateQR/QRConfirm.dart';
import 'package:shared_preferences/shared_preferences.dart';

class QRPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => new CashInPage1();
}

class CashInPage1 extends State<QRPage> {
  final myController = TextEditingController();
  final myrefController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  String amount;
  String refNo;
  String checklang = '';
  List textMyan = ["ငွေလက်ခံရန်", "ငွေပမာဏ", "အမှတ်စဥ်", "ကျပ်", "QR ပြမည်"];
  List textEng = ["Cash In", "Amount", "Reference", "MMK", "Display OR"];

  @override
  void initState() {
    checkLanguage();
    super.initState();
  }

  checkLanguage() async {
    final prefs = await SharedPreferences.getInstance();
    checklang = prefs.getString("Lang");
    // print(lang);
    if (checklang == "" || checklang == null || checklang.length == 0) {
      checklang = "Eng";
    } else {
      checklang = checklang;
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    final passwordField2 = new TextFormField(
      controller: myController,
      keyboardType: TextInputType.number,
      autofocus: true,
      decoration: InputDecoration(
        icon: const Icon(Icons.attach_money),
        suffixText: (checklang == "Eng") ? textEng[3] : textMyan[3],
        labelText: (checklang == "Eng") ? textEng[1] : textMyan[1],
        hasFloatingPlaceholder: true,
        labelStyle: (checklang == "Eng")
            ? TextStyle(fontSize: 17, color: Colors.black,height: 0, fontWeight: FontWeight.w300)
            : TextStyle(fontSize: 16, color: Colors.black, height: 0),
        fillColor: Colors.black87,
      ),
    );
    final referenceField = new TextFormField(
      controller: myrefController,
      decoration: InputDecoration(
        icon: const Icon(Icons.add_box),
        labelText: (checklang == "Eng") ? textEng[2] : textMyan[2],
        hasFloatingPlaceholder: true,
        labelStyle: (checklang == "Eng")
            ? TextStyle(fontSize: 17, color: Colors.black,height: 0, fontWeight: FontWeight.w300)
            : TextStyle(fontSize: 16, color: Colors.black, height: 0),
        fillColor: Colors.black87,
      ),
    );

    final transferbutton = new RaisedButton(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      onPressed: () async {
        final prefs = await SharedPreferences.getInstance();
        String userID = prefs.getString('userId');
        String name = prefs.getString('name');

        amount = myController.text;
        refNo = myrefController.text;
        print(amount);
        var route = new MaterialPageRoute(
            builder: (BuildContext context) => new QRConfirm(
                value: myController.text,
                value1: myrefController.text,
                value2: userID,
                value3: name));
        Navigator.of(context).push(route);
      },
      // },
      color: Color.fromRGBO(40, 103, 178, 1),
      textColor: Colors.white,
      child: Container(
        width: 300.0,
        height: 43.0,
        child: Center(
          child: (checklang == "Eng")
              ? Text(
                  (checklang == "Eng") ? textEng[4] : textMyan[4],
                  style: TextStyle(fontSize: 18),
                )
              : Text(
                  (checklang == "Eng") ? textEng[4] : textMyan[4],
                  style: TextStyle(fontSize: 16),
                ),
        ),
      ),
    );

    return Scaffold(
      appBar: new AppBar(
        //Application Bar
        elevation: 0.0,
        backgroundColor: Color.fromRGBO(40, 103, 178, 1),
        centerTitle: true,
        title: (checklang == "Eng")
            ? Text(
                (checklang == "Eng") ? textEng[0] : textMyan[0],
                style: TextStyle(fontSize: 20, color: Colors.white),
              )
            : Text(
                (checklang == "Eng") ? textEng[0] : textMyan[0],
                style: TextStyle(fontSize: 18, color: Colors.white),
              ),
      ),
      body: new Form(
        key: _formKey,
        child: new ListView(
          children: <Widget>[
            SizedBox(height: 10.0),
            Container(
              padding: EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 0.0),
              height: 280,
              child: Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                elevation: 3.0,
                child: ListView(
                  padding: EdgeInsets.all(5.0),
                  children: <Widget>[
                    Center(
                      child: new Container(
                        padding: EdgeInsets.only(left: 10.0, right: 10.0),
                      ),
                    ),
                    SizedBox(height: 20.0),
                    Center(
                      child: new Container(
                        padding: EdgeInsets.only(left: 10.0, right: 10.0),
                        child: passwordField2,
                      ),
                    ),
                    SizedBox(height: 20.0),
                    Center(
                      child: new Container(
                        padding: EdgeInsets.only(left: 10.0, right: 10.0),
                        child: referenceField,
                      ),
                    ),
                    SizedBox(height: 40.0),
                    Center(
                      child: new Container(
                        padding: EdgeInsets.only(left: 30.0, right: 30.0),
                        child: transferbutton,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
