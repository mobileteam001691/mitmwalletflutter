
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:nsb/constants/constant.dart';
import 'package:nsb/constants/rout_path.dart' as routes;
import 'package:nsb/model/GoTransferRequest.dart';
import 'package:nsb/model/GoTransferResponse.dart';
import 'package:nsb/model/ReadMessageRequest.dart';
import 'package:nsb/model/ReadMessageResponse.dart';
import 'package:nsb/pages/Scan/ScanPaymentSuccess.dart';
import 'package:nsb/utils/crypt_helper.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:nsb/framework/http_service.dart' as http;

class ScanPaymentConfirm extends StatefulWidget {
  final String value1;
  final String value2;
  final String value3;
  final String value4;
  
  ScanPaymentConfirm({Key key, this.value1,this.value2,this.value3,this.value4}) : super(key: key);

  @override
  _TransferConfirmPageState createState() => _TransferConfirmPageState();
}

class _TransferConfirmPageState extends State<ScanPaymentConfirm> {
   String alertmsg = "";
   String rkey="";
   String password = "";
   bool _isLoading;
   bool isLoading = false;
   final _formKey = new GlobalKey<FormState>();
   final GlobalKey<ScaffoldState> _scaffoldkey = new GlobalKey<ScaffoldState>();
   static final CREATE_POST_URL = 'http://122.248.120.16:8080/WalletService/module001';

    void initState(){
    this._isLoading = false;
    super.initState();
  }
  void _method1(){
    _scaffoldkey.currentState.showSnackBar(new SnackBar(content: new Text(this.alertmsg),duration: Duration(seconds: 1)));
  }

Widget build(BuildContext context) {
final style =TextStyle(fontFamily:'Montserrat', fontSize:16.0);

final PhoneField = new Container(
      child: Column(
        children: <Widget> [
          ListTile(
            title: Padding(
              padding: const EdgeInsets.fromLTRB(8.0, 2.5, 0.0, 0.0),
              child: Text("Payee",style: TextStyle(fontSize: 15,color: Colors.blueGrey),),
            ),  
                     
            subtitle: Padding(
              padding: const EdgeInsets.fromLTRB(8.0, 6.5, 0.0, 0.0),
              child: Text("${widget.value1}",style: TextStyle(fontSize: 18,color:Colors.black)),
            ),            
          ),
          Divider(color: Colors.black,)
        ]
      )
    );
 
final NameField = new Container(
      child: Column(
        children: <Widget> [
          ListTile(
            title: Padding(
              padding: const EdgeInsets.fromLTRB(8.0, 2.5, 0.0, 0.0),
              child: Text("Name",style: TextStyle(fontSize: 15,color: Colors.blueGrey),),
            ),  
                     
            subtitle: Padding(
              padding: const EdgeInsets.fromLTRB(8.0, 6.5, 0.0, 0.0),
              child: Text("${widget.value2}",style: TextStyle(fontSize: 18,color: Colors.black)),
            ),            
          ),
          Divider(color: Colors.black,)
        ]
      )
    );
final AmountField = new Container(
      child: Column(
        children: <Widget> [
          ListTile(
            title: Padding(
              padding: const EdgeInsets.fromLTRB(8.0, 2.5, 0.0, 0.0),
              child: Text("Amount",style: TextStyle(fontSize: 15,color: Colors.blueGrey),),
            ),  
                     
            subtitle: Padding(
              padding: const EdgeInsets.fromLTRB(8.0, 6.5, 0.0, 0.0),
              child: Text("${widget.value3}",style: TextStyle(fontSize: 18,color: Colors.black)),
            ),            
          ),
          Divider(color: Colors.black,)
        ]
      )
    );
final RefField = new Container(
      child: Column(
        children: <Widget> [
          ListTile(
            title: Padding(
              padding: const EdgeInsets.fromLTRB(8.0, 2.5, 0.0, 0.0),
              child: Text("Reference",style: TextStyle(fontSize: 15,color: Colors.blueGrey),),
            ),  
                     
            subtitle: Padding(
              padding: const EdgeInsets.fromLTRB(8.0, 6.5, 0.0, 0.0),
              child: Text("${widget.value4}",style: TextStyle(fontSize: 18,color: Colors.black)),
            ),            
          ),
          Divider(color: Colors.black,)
        ]
      )
    );
 
final cancelbutton = new RaisedButton(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),),
      onPressed: () async{
        this.alertmsg = '';
        Navigator.pop(context);
      },
      color:Colors.grey[400],
      textColor:Colors.white,
      child:Container(
        width:130.0,
        height:43.0,
        child:Center(child:Text('Cancel',style: TextStyle(fontSize: 18)) ),
      ),
    );

  final transferbutton = new RaisedButton(
      shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(10.0),),
      onPressed: () async{
        setState(() {
          isLoading=true;
        });
         final prefs = await SharedPreferences.getInstance();
         String userID=prefs.getString('userId');
         String username=prefs.getString('name');
         String sessionID=prefs.getString('sessionID');
          final iv = AesUtil.random(16);
          print("iv :"+iv);
          final dm = AesUtil.random(16);
          print("dm :"+dm);
          final salt = AesUtil.random(16);
          print("salt :"+salt);
          String res = AesUtil.encrypt(salt, iv, this.password);
          print("res is :"+res);
        GoTransferRequest goTransferRequest=new GoTransferRequest(
          token: sessionID,senderCode: userID,receiverCode: "${widget.value1}",fromName: username,toName: "${widget.value2}",
          amount: "${widget.value3}",prevBalance: "",password: res,iv: iv,dm: dm,salt: salt,appType: "wallet"
        );
        GoTransferResponse goTransferResponse= await goOpenAccount(CREATE_POST_URL+'/payment/goTransfer', goTransferRequest.toMap());
        if(goTransferResponse.code=='0000'){
          setState(() {
            isLoading=false;
          });
               this.alertmsg="";
               print(goTransferResponse.toString());
               var route = new MaterialPageRoute(
               builder: (BuildContext context) =>
              new ScanPaymentSuccess(value:"${widget.value3}",value1:goTransferResponse.bankRefNumber,
              value2:goTransferResponse.transactionDate,value3:goTransferRequest.toName));
              Navigator.of(context).push(route); 
              this.alertmsg = goTransferResponse.desc;
              this._method1();
        }else {
                this.alertmsg = goTransferResponse.desc;
                this._method1();
                final form = _formKey.currentState;
                form.validate();
                this._isLoading = false;
                setState(() {
                  isLoading=true;
                });
              }
      },
      color:Color.fromRGBO(40, 103, 178, 1),
      textColor:Colors.white,
      child:Container(
        width:130.0,
        height:43.0,
        child:Center(child:Text('Confirm',style: TextStyle(fontSize: 18)) ),
      ),
    );

    var paybody = new Form(
               key: _formKey,
              child: new ListView(
                children: <Widget>[
                  SizedBox(height: 5.0),

            Container(
              padding: EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 0.0),
              height: 420,
              child: Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20.0),),
                elevation: 3.0,
                child: ListView(
                  padding: EdgeInsets.all(0.0),
                  children: <Widget>[
                    Center(
                      child: new Container(
                        padding: EdgeInsets.only(left: 15.0, right: 10.0),
                      ),
                    ),
                    Center(
                      child: new Container(
                        padding: EdgeInsets.only(left: 15.0, right: 10.0),
                        child: PhoneField,
                      ),
                    ),
                    Center(
                     child: new Container(
                        padding: EdgeInsets.only(left: 15.0, right: 10.0),
                        child: NameField,
                      ),
                    ),
                    Center(
                     child: new Container(
                        padding: EdgeInsets.only(left: 15.0, right: 10.0),
                        child: AmountField,
                      ),
                    ),
                    Center(
                     child: new Container(
                        padding: EdgeInsets.only(left: 15.0, right: 10.0),
                        child: RefField,
                      ),
                    ),
                    Row(
                     children: <Widget>[
                       new Container(
                         padding: EdgeInsets.only(left: 12.0),
                         child: cancelbutton,
                       ),
                       new Container(
                         padding: EdgeInsets.only(left: 14.0),
                         child: transferbutton,
                       )
                     ],
                    ),
                  ],
                ),
              ),
            ),
          ],                
              ),
            );

    var bodyProgress = new Container(
      child: new Stack(
        children: <Widget>[
          paybody,
          Container(
              decoration: new BoxDecoration(
                color: Color.fromRGBO(255, 255, 255, 0.5),
              ),
              width: MediaQuery.of(context).size.width * 0.99,
              height: MediaQuery.of(context).size.height * 0.9,
              child: Center(
                child: CircularProgressIndicator(
                  backgroundColor: Colors.amber,
                ),
              ))
        ],
      ),
    );

return Scaffold(
        key: _scaffoldkey,
        backgroundColor: Colors.grey[200],
        appBar: new AppBar(//Application Bar
        elevation: 0.0,
        backgroundColor:Color.fromRGBO(40, 103, 178, 1),
        title: Center(child: Text('Confirmation', style: TextStyle(
        fontSize: 20.0,
        color: Colors.white,
        height: 1.0,
        fontWeight: FontWeight.w600),),)),
        body: isLoading ? bodyProgress : paybody,
        // body: new Form(
        //        key: _formKey,
        //       child: new ListView(
        //         children: <Widget>[
        //           SizedBox(height: 5.0),

        //     Container(
        //       padding: EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 0.0),
        //       height: 420,
        //       child: Card(
        //         shape: RoundedRectangleBorder(
        //           borderRadius: BorderRadius.circular(20.0),),
        //         elevation: 3.0,
        //         child: ListView(
        //           padding: EdgeInsets.all(0.0),
        //           children: <Widget>[
        //             Center(
        //               child: new Container(
        //                 padding: EdgeInsets.only(left: 15.0, right: 10.0),
        //               ),
        //             ),
        //             Center(
        //               child: new Container(
        //                 padding: EdgeInsets.only(left: 15.0, right: 10.0),
        //                 child: PhoneField,
        //               ),
        //             ),
        //             Center(
        //              child: new Container(
        //                 padding: EdgeInsets.only(left: 15.0, right: 10.0),
        //                 child: NameField,
        //               ),
        //             ),
        //             Center(
        //              child: new Container(
        //                 padding: EdgeInsets.only(left: 15.0, right: 10.0),
        //                 child: AmountField,
        //               ),
        //             ),
        //             Center(
        //              child: new Container(
        //                 padding: EdgeInsets.only(left: 15.0, right: 10.0),
        //                 child: RefField,
        //               ),
        //             ),
        //             Row(
        //              children: <Widget>[
        //                new Container(
        //                  padding: EdgeInsets.only(left: 12.0),
        //                  child: cancelbutton,
        //                ),
        //                new Container(
        //                  padding: EdgeInsets.only(left: 14.0),
        //                  child: transferbutton,
        //                )
        //              ],
        //             ),
        //           ],
        //         ),
        //       ),
        //     ),
        //   ],                
        //       ),
        //     ),
    );
  }
  Future<GoTransferResponse> goOpenAccount(url, Map jsonMap) async{
    GoTransferResponse p = new GoTransferResponse();
    var body;
    try{
      body = await http.doPost(url, jsonMap);
      p = GoTransferResponse.fromJson(json.decode(body.toString()));
    }catch(e){
      p.code= Constants.responseCode_Error;
      p.desc = e.toString();
    }
    print(p.toMap());
    return p;
  }
}
class AlwaysDisabledFocusNode extends FocusNode {
  @override
  bool get hasFocus => false;
}


