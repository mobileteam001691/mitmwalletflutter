
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:nsb/constants/rout_path.dart' as routes;
import 'package:nsb/pages/Scan/ScanPaymentConfirm.dart';
import 'package:nsb/framework/http_service.dart' as http;

class ScanPayment extends StatefulWidget {
   final String value;  
  
  ScanPayment({Key key, this.value}) : super(key: key);

  @override
  _TransferPageState createState() => _TransferPageState();
}

class _TransferPageState extends State<ScanPayment> {
  String alertmsg = "";
  String rkey="";
  bool _isLoading;
  final _formKey = new GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldkey = new GlobalKey<ScaffoldState>();
  static final CREATE_POST_URL = 'http://122.248.120.16:8080/WalletService/module001';
  String ph,name,amount,ref;
  var results;

  void initState(){
    this._isLoading = false;
    super.initState();
    results=jsonDecode(widget.value);
    print(results);
    ph=results["pay"];
    name=results["ref"];
    amount=results["amt"];
    ref=results["name"];
  }
  void _method1(){
    print("Snack Bar");
    print(this.alertmsg);
    _scaffoldkey.currentState.showSnackBar(new SnackBar(content: new Text(this.alertmsg),duration: Duration(seconds: 1)));
  }
Widget build(BuildContext context) {
final style =TextStyle(fontFamily:'Montserrat', fontSize:16.0);

final PhoneField = new Container(
      child: Column(
        children: <Widget> [
          ListTile(
            title: Padding(
              padding: const EdgeInsets.fromLTRB(8, 2, 8, 15),
              child: Text("Payee",style: TextStyle(fontSize: 15,color: Colors.blueGrey),),
            ),  
                     
            subtitle: Padding(
              padding: const EdgeInsets.fromLTRB(8.0, 3.5, 0.0, 0.0),
              child: Text("$ph",style: TextStyle(fontSize: 18,color:Colors.black)),
            ),            
          ),
          Divider(color: Colors.black,)
        ]
      )
    );
 
final NameField = new Container(
      child: Column(
        children: <Widget> [
          ListTile(
            title: Padding(
              padding: const EdgeInsets.fromLTRB(8, 2, 8, 15),
              child: Text("Name",style: TextStyle(fontSize: 15,color: Colors.blueGrey),),
            ),  
                     
            subtitle: Padding(
              padding: const EdgeInsets.fromLTRB(8.0, 3.5, 0.0, 0.0),
              child: Text("$name",style: TextStyle(fontSize: 18,color: Colors.black)),
            ),            
          ),
          Divider(color: Colors.black,)
        ]
      )
    );
final AmountField = new Container(
      child: Column(
        children: <Widget> [
          ListTile(
            title: Padding(
              padding: const EdgeInsets.fromLTRB(8, 2, 8, 15),
              child: Text("Amount",style: TextStyle(fontSize: 15,color: Colors.blueGrey),),
            ),  
                     
            subtitle: Padding(
              padding: const EdgeInsets.fromLTRB(8.0, 3.5, 0.0, 0.0),
              child: Text("$amount",style: TextStyle(fontSize: 18,color: Colors.black)),
            ),            
          ),
          Divider(color: Colors.black,)
        ]
      )
    );
final RefField = new Container(
      child: Column(
        children: <Widget> [
          ListTile(
            title: Padding(
              padding: const EdgeInsets.fromLTRB(8, 2, 8, 15),
              child: Text("Reference",style: TextStyle(fontSize: 15,color: Colors.blueGrey),),
            ),  
                     
            subtitle: Padding(
              padding: const EdgeInsets.fromLTRB(8.0, 3.5, 0.0, 0.0),
              child: Text("$ref",style: TextStyle(fontSize: 18,color: Colors.black)),
            ),            
          ),
          Divider(color: Colors.black,)
        ]
      )
    );
 
final transferbutton = new RaisedButton(
      shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(10.0),),
      onPressed: () async{
        var route = new MaterialPageRoute(
        builder: (BuildContext context) =>
        new ScanPaymentConfirm(value1: ph,value2: name,value3: amount,value4: ref,));
        Navigator.of(context).push(route);           
      },
      color:Color.fromRGBO(40, 103, 178, 1),
      textColor:Colors.white,
      child:Container(
        width:300.0,
        height:43.0,
        child:Center(child:Text('Pay',style: TextStyle(fontSize: 18)) ),  
      ),
    );

return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: new AppBar(//Application Bar
        elevation: 0.0,
        backgroundColor:Color.fromRGBO(40, 103, 178, 1),
        title: Center(child: Text('Payment', style: TextStyle(
        fontSize: 20.0,
        color: Colors.white,
        height: 1.0,
        fontWeight: FontWeight.w600),
        ),) 
        ),
        body: new Form(
              key: _formKey,
              child: new ListView(
                children: <Widget>[
                  SizedBox(height: 10.0),

            Container(
              padding: EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 0.0),
              height: 490,
              child: Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),),
                elevation: 3.0,
                child: ListView(
                  padding: EdgeInsets.all(5.0),
                  children: <Widget>[
                    Center(
                      child: new Container(
                        padding: EdgeInsets.only(left: 10.0, right: 10.0),
                      ),
                    ),
                    Center(
                      child: new Container(
                        padding: EdgeInsets.only(left: 10.0, right: 10.0),
                        child: PhoneField,
                      ),
                    ),
                    SizedBox(height: 15.0),
                    Center(
                     child: new Container(
                        padding: EdgeInsets.only(left: 10.0, right: 10.0),
                        child: NameField,
                      ),
                    ),
                     SizedBox(height: 15.0),
                    Center(
                     child: new Container(
                        padding: EdgeInsets.only(left: 10.0, right: 10.0),
                        child: AmountField,
                      ),
                    ),
                    SizedBox(height: 15.0),
                    Center(
                     child: new Container(
                        padding: EdgeInsets.only(left: 10.0, right: 10.0),
                        child: RefField,
                      ),
                    ),
                    SizedBox(height: 15.0),
                    Center(
                      child: new Container(
                        padding: EdgeInsets.only(left: 30.0, right: 30.0),
                        child: transferbutton,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],                
              ),
            ),
    );
  }
}

// final prefs = await SharedPreferences.getInstance();
//     String userID=prefs.getString('userId');
//     String url = "http://122.248.120.16:8080/WalletService/module001/service001/getName";
//     Map<String, String> headers = {"Content-type": "application/json"};
//     String json = '{"userID": "'+userID+'", "name": "","nrc": "","sessionID": "","institutionCode": "", "sKey": "","field1": "","field2": ""}';
//     http.Response response = await http.post(url, headers: headers, body: json);
//     int statusCode = response.statusCode;
//     if (statusCode == 200) {
//       String body = response.body;
//       print(body);
//       var data = jsonDecode(body);
//       setState(() {
//          locationList=data;
//       });
//       print(locationList);
//     }
//     else {
//       print("Connection Fail");
//     }
// if (locationList["messageCode"] == "0000"){
//         Navigator.push(context, MaterialPageRoute(builder: (context)=>NewFeedsPage()));
//       }else{
//       }
