import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:nsb/framework/http_service.dart' as http;
import 'package:nsb/model/notificationRequest.dart';
import 'package:nsb/model/notificationResponse.dart';
// import 'package:nsb/model/notificationRequest.dart';
// import 'package:nsb/model/notificationResponse.dart';

import 'package:shared_preferences/shared_preferences.dart';


class Noti extends StatefulWidget {
  @override
  _NotiState createState() => _NotiState();
}

class _NotiState extends State<Noti> {

  static final url = 'http://122.248.120.16:8080/WalletService/module001';
  var list=[];
  bool isLoading = true;
  @override
  void initState(){
    super.initState();
    method1();
  }

  method1() async{
    final prefs = await SharedPreferences.getInstance();
    String userID=prefs.getString('userId');
    String sessionID ="";
    String customerNo ="";
    String totalCount ="0";
    String currentPage ="";
    String pageSize = "";
    String pageCount ="0";
    NotificationRequest notificationRequest=new NotificationRequest(userID: userID,sessionID: sessionID,customerNo: customerNo,totalCount: totalCount,currentPage: currentPage,pageSize: pageSize,pageCount: pageCount);
    NotiRemove notificationResponse = await goLogin(url+'/service007/removeNotiCount', notificationRequest.toMap());
        if(notificationResponse.count==0){
          String sessionID=prefs.getString('sessionID');
          String customerNo ="";
          String totalCount ="0";
          String currentPage = "1";
          String pageSize = "10";
          String pageCount ="0";
          NotificationRequest notificationRequest1=new NotificationRequest(userID: userID,sessionID: sessionID,customerNo: customerNo,totalCount: totalCount,currentPage: currentPage,pageSize: pageSize,pageCount: pageCount);
          NotificationResponse notificationResponse1 = await goLogin1(url+'/service007/getAllTokenNotification', notificationRequest1.toMap());
            if(notificationResponse1.msgCode=='0000'){
              setState(() {
              isLoading=false;
              list=notificationResponse1.data;
              });
            }
        }  
  }
  @override
  Widget build(BuildContext context) {

    var notiload = ListView.builder(
        itemCount: list.length,
        itemBuilder: (context, index) {
          return Container(
            child: Padding(
              padding: const EdgeInsets.all(5.0),
              child: Column(
                children: <Widget>[
                  ListTile(
                    title: new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Text('${list[index].description}'),
                      ],
                    )
                  ),
                  Divider(),
                ],
              ),
            ),
          );
        }
      );

    var bodyProgress = new Container(
      child: new Stack(
        children: <Widget>[
          notiload,
          Container(
              decoration: new BoxDecoration(
                color: Color.fromRGBO(255, 255, 255, 0.5),
              ),
              width: MediaQuery.of(context).size.width * 0.99,
              height: MediaQuery.of(context).size.height * 0.9,
              child: Center(
                child: CircularProgressIndicator(
                  backgroundColor: Colors.amber,
                ),
              ))
        ],
      ),
    );

    return Scaffold(
      appBar: AppBar(
        backgroundColor:  Color.fromRGBO(40, 103, 178, 1),
        title: Padding(
          padding: const EdgeInsets.only(right:35.0),
          child: Center(child: new Text('Notification')),
        ),
      ),
      body: isLoading ? bodyProgress : notiload
      // body: ListView.builder(
      //   itemCount: list.length,
      //   itemBuilder: (context, index) {
      //     return Container(
      //       child: Padding(
      //         padding: const EdgeInsets.all(5.0),
      //         child: Column(
      //           children: <Widget>[
      //             ListTile(
      //               title: new Column(
      //                 crossAxisAlignment: CrossAxisAlignment.start,
      //                 mainAxisAlignment: MainAxisAlignment.start,
      //                 children: <Widget>[
      //                   Text('${list[index].description}'),
      //                 ],
      //               )
      //             ),
      //             Divider(),
      //           ],
      //         ),
      //       ),
      //     );
      //   }
      // ),
    );
  }
  Future<NotiRemove> goLogin(url, Map jsonMap) async{
    NotiRemove p = new NotiRemove();
    var body;
    try{
      body = await http.doPost(url, jsonMap);
      // print(body);
      p = NotiRemove.fromJson(json.decode(body.toString()));
    }catch(e){
      print(e.toString());
    }
    return p;
  }
  Future<NotificationResponse> goLogin1(url, Map jsonMap) async{
    NotificationResponse p = new NotificationResponse();
    var body;
    try{
      body = await http.doPost(url, jsonMap);
      // print(body);
      p = NotificationResponse.fromJson(json.decode(body.toString()));
    }catch(e){
      print(e.toString());
    }
    return p;
  }
}
