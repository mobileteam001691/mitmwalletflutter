import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:nsb/constants/constant.dart';
import 'package:nsb/constants/rout_path.dart' as routes;
import 'package:nsb/framework.dart/popUp.dart';
import 'package:nsb/model/CheckOTPRequestData.dart';
import 'package:nsb/model/Otp.dart';
import 'package:nsb/model/OtpResponse.dart';
import 'package:nsb/model/WalletResponseData.dart';
import 'package:nsb/pages/Wallet.dart';
import 'package:nsb/pages/signUp.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:nsb/framework/http_service.dart' as http;

class NewOtpPage extends StatefulWidget {
  final String value;
  final String value1;

  NewOtpPage({Key key, this.value, Key key1, this.value1}) : super(key: key);

  @override
  State<StatefulWidget> createState() => new NewOtpPageState();
}

class NewOtpPageState extends State<NewOtpPage> {
  final myController = TextEditingController();
  final myPwdController = TextEditingController();
  String alertmsg = "";
  String rkey = "";
  bool _isLoading;
  bool isLoading = false;
  final _formKey = new GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldkey = new GlobalKey<ScaffoldState>();
  String checklang = '';
  List textMyan = [
    "သင်၏ကုဒ်ကိုရိုက်ထည့်ပါ",
    "သင်၏ဖုန်းနံပါတ်သို့ကုဒ်ပို့ပြီးပါပြီ",
    "အတည်ပြုကုဒ်",
    "၀င်ရန်"
  ];
  List textEng = [
    "Enter Your OTP",
    "The OTP is already sent to your phone number",
    "OTP",
    "Next"
  ];

  static final CREATE_POST_URL =
      "http://122.248.120.16:8080/WalletService/module001";

  void initState() {
    this._isLoading = false;
    checkLanguage();
    super.initState();
  }

  void _method1() {
    print("Snack Bar");
    print(this.alertmsg);
    _scaffoldkey.currentState.showSnackBar(new SnackBar(
        content: new Text(this.alertmsg), duration: Duration(seconds: 1)));
  }

  checkLanguage() async {
    final prefs = await SharedPreferences.getInstance();
    checklang = prefs.getString("Lang");
    // print(lang);
    if (checklang == "" || checklang == null || checklang.length == 0) {
      checklang = "Eng";
    } else {
      checklang = checklang;
    }
    setState(() {});
  }
  Future _login() async{
        setState((){
          isLoading = false;
        });
      }

  @override
  Widget build(BuildContext context) {
    final style = TextStyle(fontFamily: 'Montserrat', fontSize: 14.0);
    final passwordField = TextField(
      controller: myPwdController,
      obscureText: false,
      style: style,
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
          prefixIcon: Icon(Icons.lock),
          hintText: (checklang == "Eng") ? textEng[2] : textMyan[2],
          hintStyle:
              TextStyle(fontSize: 12, color: Color.fromARGB(100, 90, 90, 90)),
          fillColor: Colors.black,
          focusedBorder: OutlineInputBorder(
            borderSide: const BorderSide(color: Colors.black26, width: 1),
            borderRadius: BorderRadius.circular(10.0),
          ),
          border: OutlineInputBorder()),
    );

    final loginButon = Padding(
        padding: EdgeInsets.fromLTRB(0.0, 20.0, 00.0, 0.0),
        child: SizedBox(
            height: 45.0,
            width: MediaQuery.of(context).size.width,
            child: new RaisedButton(
              elevation: 5.0,
              shape: new RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(10.0)),
              color: Color.fromRGBO(40, 103, 178, 1),
              child: (checklang == "Eng")
                  ? Text(
                      (checklang == "Eng") ? textEng[3] : textMyan[3],
                      style: TextStyle(fontSize: 18),
                    )
                  : Text(
                      (checklang == "Eng") ? textEng[3] : textMyan[3],
                      style: TextStyle(fontSize: 16),
                    ),
              textColor: Colors.white,
              onPressed: () async {
                setState(() {
                  isLoading = true;
                });
                this.alertmsg = '';
                CheckOTPRequestData otp = new CheckOTPRequestData(
                    deviceID: '',
                    fcmToken: "",
                    otpCode: myPwdController.text,
                    rKey: "${widget.value1}",
                    sessionID: '',
                    userID: "${widget.value}");

                WalletResponseData res1 = await goLogin(
                    CREATE_POST_URL + '/service001/login', otp.toMap());
                print("gz");
                if (res1.messageCode == Constants.responseCode_Success) {
                  setState(() {
                  isLoading = false;
                });
                  this.alertmsg = res1.messageDesc;
                  this._method1();
                  print(res1.toString());
                  final prefs = await SharedPreferences.getInstance();
                  final key = 'sKey';
                  final sKey = res1.sKey;
                  prefs.setString(key, sKey);
                  print('saved  Data $sKey');
                  final key1 = 'accountNo';
                  final accountNo = res1.accountNo;
                  prefs.setString(key1, accountNo);
                  final key3 = 'sessionID';
                  final sessionID = res1.sessionID;
                  prefs.setString(key3, sessionID);
                  final key4 = 'userId';
                  final userId = "${widget.value}";
                  prefs.setString(key4, userId);
                  final key2 = 'name';
                  final name = res1.name;
                  prefs.setString(key2, name);
                  print('saved  name $name');
                  final userData = 'userData';
                  final WalletResponseData response = res1;
                  prefs.setString(userData, json.encode(response.toJson()));
                  Map responseData = json.decode(prefs.getString(userData));
                  WalletResponseData store =
                      WalletResponseData.fromJson(responseData);
                  print(store.accountNo);
                  print("ez");
                  _scaffoldkey.currentState.removeCurrentSnackBar();
                  var route = new MaterialPageRoute(
                      builder: (BuildContext context) => new WalletPage(
                          value: "${widget.value}", value1: name));

                  Navigator.of(context).push(route);
                } else if (res1.messageCode == '0009') {
                  this.alertmsg = res1.messageDesc;
                  this._method1();
                  final prefs = await SharedPreferences.getInstance();
                  final sessionID = res1.sessionID;
                  prefs.setString('sessionID', sessionID);
                  prefs.getString('sessionID');
                  final userId = res1.userId;
                  prefs.setString('userId', userId);
                  prefs.getString('userId');
                  final sKey = res1.sKey;
                  prefs.setString('sKey', sKey);
                  prefs.getString('sKey');
                  final accNo = res1.accountNo;
                  prefs.setString('accountNo', accNo);
                  prefs.getString('accountNo');
                  print('saved  Data $sessionID');
                  print(res1.messageCode);
                  var route = new MaterialPageRoute(
                      builder: (BuildContext context) => new SignUpPage(
                          value: "${widget.value}", value1: sessionID));
                  Navigator.of(context).push(route);
                } else {
                  print('OTP Error');
                  setState(() {
                  isLoading = true;
                  new Future.delayed(new Duration(seconds: 3), _login);
                });
                  this.alertmsg = res1.messageDesc;
                  this._method1();
                  final form = _formKey.currentState;
                  form.validate();
                }
              },
            )));

    final size = MediaQuery.of(context).size;

    var body = new Form(
      key: _formKey,
      child: new ListView(
        children: <Widget>[
          SizedBox(height: 50.0),
          Container(
            padding: EdgeInsets.all(10.0),
            height: 500,
            child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(18.0),
              ),
              elevation: 3.0,
              child: ListView(
                padding: EdgeInsets.all(5.0),
                children: <Widget>[
                  Center(
                    child: new Container(
                      padding: EdgeInsets.only(left: 30.0, right: 30.0),
                    ),
                  ),
                  SizedBox(height: 10.0),
                  new Container(
                    child: new Image.asset(
                      'assets/Picture10.png',
                      width: size.width,
                    ),
                  ),
                  SizedBox(height: 20.0),
                  Center(
                    child: (checklang == "Eng")
                        ? Text((checklang == "Eng") ? textEng[0] : textMyan[0],
                            style: TextStyle(fontSize: 30.0))
                        : Text((checklang == "Eng") ? textEng[0] : textMyan[0],
                            style: TextStyle(fontSize: 25.0)),
                  ),
                  SizedBox(height: 15.0),
                  Center(
                      child: Text(
                    (checklang == "Eng") ? textEng[1] : textMyan[1],
                  )),
                  SizedBox(height: 30.0),
                  Center(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          width: 180,
                          child: passwordField,
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 24.0),
                  Center(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          width: 280,
                          child: loginButon,
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 30.0),
                ],
              ),
            ),
          ),
        ],
      ),
    );

    var bodyProgress = new Container(
      child: new Stack(
        children: <Widget>[
          body,
          Container(
              decoration: new BoxDecoration(
                color: Color.fromRGBO(255, 255, 255, 0.5),
              ),
              width: MediaQuery.of(context).size.width * 0.99,
              height: MediaQuery.of(context).size.height * 0.9,
              child: Center(
                child: CircularProgressIndicator(
                  backgroundColor: Colors.amber,
                ),
              ))
        ],
      ),
    );

    return new MaterialApp(
      debugShowCheckedModeBanner: false,
      home: new Scaffold(
          key: _scaffoldkey,
          appBar: new AppBar(
            //Application Bar
            elevation: 0.0,
            backgroundColor: Color.fromRGBO(40, 103, 178, 1),

            leading: new IconButton(
              icon: new Icon(Icons.arrow_back, color: Colors.white),
              onPressed: () => Navigator.of(context).pop(),
            ),
          ),
          body: isLoading ? bodyProgress : body

          // body: new Form(
          //   key: _formKey,
          //   child: new ListView(
          //     children: <Widget>[
          //       SizedBox(height: 50.0),
          //       Container(
          //         padding: EdgeInsets.all(10.0),
          //         height: 500,
          //         child: Card(
          //           shape: RoundedRectangleBorder(
          //             borderRadius: BorderRadius.circular(18.0),
          //           ),
          //           elevation: 3.0,
          //           child: ListView(
          //             padding: EdgeInsets.all(5.0),
          //             children: <Widget>[
          //               Center(
          //                 child: new Container(
          //                   padding: EdgeInsets.only(left: 30.0, right: 30.0),
          //                 ),
          //               ),
          //               SizedBox(height: 10.0),
          //               new Container(
          //                 child: new Image.asset(
          //                   'assets/Picture10.png',
          //                   width: size.width,
          //                 ),
          //               ),
          //               SizedBox(height: 20.0),
          //               Center(
          //                   child: Text(
          //                       (checklang == "Eng") ? textEng[0] : textMyan[0],
          //                       style: TextStyle(fontSize: 30.0))),
          //               SizedBox(height: 15.0),
          //               Center(
          //                   child: Text(
          //                       (checklang == "Eng") ? textEng[1] : textMyan[1],)),
          //               SizedBox(height: 30.0),
          //               Center(
          //                 child: Row(
          //                   mainAxisAlignment: MainAxisAlignment.center,
          //                   children: <Widget>[
          //                     Container(
          //                       width: 180,
          //                       child: passwordField,
          //                     ),
          //                   ],
          //                 ),
          //               ),
          //               SizedBox(height: 24.0),
          //               Center(
          //                 child: Row(
          //                   mainAxisAlignment: MainAxisAlignment.center,
          //                   children: <Widget>[
          //                     Container(
          //                       width: 280,
          //                       child: loginButon,
          //                     ),
          //                   ],
          //                 ),
          //               ),
          //               SizedBox(height: 30.0),
          //             ],
          //           ),
          //         ),
          //       ),
          //     ],
          //   ),
          // ),
          ),
    );
  }

  Widget _showCircularProgress() {
    if (_isLoading) {
      return Center(child: CircularProgressIndicator());
    }
    return Container(
      height: 0.0,
      width: 0.0,
    );
  }

  Widget _InterestCaculator() {
    return Padding(
      padding: const EdgeInsets.only(left: 30.0, right: 30.0),
      child: Container(
        height: 40.0,
        child: new RaisedButton(
          shape: RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(18.0),
              side: BorderSide(color: Colors.white)),
          onPressed: () async {
            Navigator.of(context).pushNamed(routes.FaqPageRoute);
          },
          color: Colors.purple[900],
          textColor: Colors.white,
          child: Row(
            children: <Widget>[
              new Padding(
                  padding: EdgeInsets.only(
                left: 10.0,
              )),
              Icon(
                Icons.payment,
                color: Colors.white,
              ),
              new Padding(
                  padding: EdgeInsets.only(
                right: 10.0,
              )),
              Text(
                'FAQ',
                style: TextStyle(color: Colors.white),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _Product() {
    return Padding(
      padding: const EdgeInsets.only(left: 30.0, right: 30.0),
      child: Container(
        height: 40.0,
        child: new RaisedButton(
          shape: RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(18.0),
              side: BorderSide(color: Colors.white)),
          onPressed: () async {
            Navigator.of(context).pushNamed(routes.ProductsPageRoute);
          },
          color: Colors.purple[900],
          textColor: Colors.white,
          child: Row(
            children: <Widget>[
              new Padding(
                  padding: EdgeInsets.only(
                left: 10.0,
              )),
              Icon(
                Icons.payment,
                color: Colors.white,
              ),
              new Padding(
                  padding: EdgeInsets.only(
                right: 10.0,
              )),
              Text(
                'Product',
                style: TextStyle(color: Colors.white),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _Location() {
    return Padding(
      padding: const EdgeInsets.only(left: 30.0, right: 30.0),
      child: Container(
        height: 40.0,
        child: new RaisedButton(
          shape: RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(18.0),
              side: BorderSide(color: Colors.white)),
          onPressed: () async {
            Navigator.of(context).pushNamed(routes.LocationPageRoute);
          },
          color: Colors.purple[900],
          textColor: Colors.white,
          child: Row(
            children: <Widget>[
              new Padding(
                  padding: EdgeInsets.only(
                left: 10.0,
              )),
              Icon(
                Icons.payment,
                color: Colors.white,
              ),
              new Padding(
                  padding: EdgeInsets.only(
                right: 10.0,
              )),
              Text(
                'Location',
                style: TextStyle(color: Colors.white),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _ExchangeRate() {
    return Padding(
      padding: const EdgeInsets.only(left: 30.0, right: 30.0),
      child: Container(
        height: 40.0,
        child: new RaisedButton(
          shape: RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(18.0),
              side: BorderSide(color: Colors.white)),
          onPressed: () async {
            Navigator.of(context).pushNamed(routes.ExchangeRatePageRoute);
          },
          color: Colors.purple[900],
          textColor: Colors.white,
          child: Row(
            children: <Widget>[
              new Padding(
                  padding: EdgeInsets.only(
                left: 10.0,
              )),
              Icon(
                Icons.payment,
                color: Colors.white,
              ),
              new Padding(
                  padding: EdgeInsets.only(
                right: 10.0,
              )),
              Text(
                'Exchange Rate',
                style: TextStyle(color: Colors.white),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _ContactUs() {
    return Padding(
      padding: const EdgeInsets.only(left: 30.0, right: 30.0),
      child: Container(
        height: 40.0,
        child: new RaisedButton(
          shape: RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(18.0),
              side: BorderSide(color: Colors.white)),
          onPressed: () async {},
          color: Colors.purple[900],
          textColor: Colors.white,
          child: Row(
            children: <Widget>[
              new Padding(
                padding: EdgeInsets.only(
                  left: 10.0,
                ),
              ),
              Icon(
                Icons.payment,
                color: Colors.white,
              ),
              new Padding(
                  padding: EdgeInsets.only(
                right: 10.0,
              )),
              Text(
                'Contact Us',
                style: TextStyle(color: Colors.white),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _ChequeInquiry() {
    return Padding(
      padding: const EdgeInsets.only(left: 30.0, right: 30.0),
      child: Container(
        height: 40.0,
        child: new RaisedButton(
          shape: RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(18.0),
              side: BorderSide(color: Colors.white)),
          onPressed: () async {},
          color: Colors.purple[900],
          textColor: Colors.white,
          child: Row(
            children: <Widget>[
              new Padding(
                  padding: EdgeInsets.only(
                left: 10.0,
              )),
              Icon(
                Icons.payment,
                color: Colors.white,
              ),
              new Padding(
                  padding: EdgeInsets.only(
                right: 10.0,
              )),
              Text(
                'Cheque Inquiry',
                style: TextStyle(color: Colors.white),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<WalletResponseData> goLogin(url, Map jsonMap) async {
    WalletResponseData p = new WalletResponseData();
    var body;
    try {
      body = await http.doPost(url, jsonMap);
      p = WalletResponseData.fromJson(json.decode(body.toString()));
    } catch (e) {
      p.messageCode = Constants.responseCode_Error;
      p.messageDesc = e.toString();
    }
    print(p.toMap());
    return p;
  }

  Future<OtpResponse> goLoginOTP(url, Map jsonMap) async {
    OtpResponse p = new OtpResponse();
    var body;
    try {
      body = await http.doPost(url, jsonMap);
      p = OtpResponse.fromJson(json.decode(body.toString()));
    } catch (e) {
      p.code = Constants.responseCode_Error;
      p.desc = e.toString();
    }
    print(p.toMap());
    return p;
  }
}
