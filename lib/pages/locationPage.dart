import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:nsb/pages/LocationDetail.dart';
import 'package:nsb/pages/Transaction/TransitionMain.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class locationPage extends StatefulWidget {
  locationPage();
  @override
  _locationPageState createState() => _locationPageState();
}

class _locationPageState extends State<locationPage> {
  Completer<GoogleMapController> _controller = Completer();

  static const LatLng _center = const LatLng(45.521563, -122.677433);

  final Set<Marker> _markers = {};

  LatLng _lastMapPosition = _center;

  MapType _currentMapType = MapType.normal;
  bool isLoading = true;

  void _onMapTypeButtonPressed() {
    setState(() {
      _currentMapType = _currentMapType == MapType.normal
          ? MapType.satellite
          : MapType.normal;
    });
  }

  void _onAddMarkerButtonPressed() {
    setState(() {
      _markers.add(Marker(
        markerId: MarkerId(_lastMapPosition.toString()),
        position: _lastMapPosition,
        infoWindow: InfoWindow(
          title: 'Really cool place',
          snippet: '5 Star Rating',
        ),
        icon: BitmapDescriptor.defaultMarker,
      ));
    });
  }

  void _onCameraMove(CameraPosition position) {
    _lastMapPosition = position.target;
  }

  void _onMapCreated(GoogleMapController controller) {
    _controller.complete(controller);
  }

  var location = new Location();
  Map<String, double> userLocation;
  List ATMDataList = new List();
  List agentDataList = new List();
  List branchDataList = new List();
  List merchantDataList = new List();
  String checklang = '';
  List textMyan = ["တည်နေရာ"];
  List textEng = ["Location"];
  var locationList = [];

  checkLanguage() async {
    final prefs = await SharedPreferences.getInstance();
    checklang = prefs.getString("Lang");
    // print(lang);
    if (checklang == "" || checklang == null || checklang.length == 0) {
      checklang = "Eng";
    } else {
      checklang = checklang;
    }
    setState(() {});
  }

  void initState() {
    checkLanguage();
    super.initState();
    getLocation();
    print("ma thi bu");
  }

  getLocation() async {
    String url =
        "http://122.248.120.16:8080/WalletService/module001/service002/getLocation";
    Map<String, String> headers = {"Content-type": "application/json"};
    String json = '{ "userID": "", "sessionID": "" }';
    http.Response response = await http.post(url, headers: headers, body: json);
    int statusCode = response.statusCode;
    if (statusCode == 200) {
      setState(() {
        isLoading = false;
      });
      String body = response.body;
      print(body);
      var data = jsonDecode(body);
      setState(() {
        locationList = data["data"];
      });
    } else {
      print("Connection Fail");
      setState(() {
        isLoading=true;
      });
    }
  }

  Widget build(BuildContext context) {
    void _launchMapsUrl(String lat, String lon) async {
      final url = 'https://www.google.com/maps/search/?api=1&query=$lat,$lon';
      if (await canLaunch(url)) {
        await launch(url);
      } else {
        throw 'Could not launch $url';
      }
    }

    Future<Map<String, double>> _getLocation() async {
      var currentLocation = <String, double>{};
      try {
        currentLocation = (await location.getLocation()) as Map<String, double>;
      } catch (e) {
        currentLocation = null;
      }
      return currentLocation;
    }

    print(locationList.length);
    for (var dataLoop = 0; dataLoop < locationList.length; dataLoop++) {
      if (locationList[dataLoop]["locationType"] == "ATM") {
        ATMDataList = locationList[dataLoop]["dataList"];
      } else if (locationList[dataLoop]["locationType"] == "Branch") {
        branchDataList = locationList[dataLoop]["dataList"];
      } else if (locationList[dataLoop]["locationType"] == "Agent") {
        agentDataList = locationList[dataLoop]["dataList"];
      } else if (locationList[dataLoop]["locationType"] == "Merchant") {
        merchantDataList = locationList[dataLoop]["dataList"];
      }
    }

    var locationbody = TabBarView(children: [
                  ListView.builder(
                      itemCount: ATMDataList.length,
                      itemBuilder: (BuildContext ctxt, int index) {
                        return Column(
                          children: <Widget>[
                            ListTile(
                              contentPadding: EdgeInsets.symmetric(
                                  horizontal: 20.0, vertical: 10.0),
                              title: Text(
                                ATMDataList[index]["name"],
                                style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold),
                              ),
                              subtitle: Text(
                                "\nTelephone : " +
                                    ATMDataList[index]["phone1"] +
                                    "\n\nAddress : " +
                                    ATMDataList[index]["address"],
                                style: TextStyle(color: Colors.black),
                              ),
                              trailing: IconButton(
                                icon: Icon(Icons.phone, color: Colors.indigo),
                                onPressed: () {
                                  launch('tel:' + ATMDataList[index]["phone1"]);
                                },
                              ),
                              onTap: () {
                                _launchMapsUrl(ATMDataList[index]["latitude"],
                                    ATMDataList[index]["longitude"]);
                              },
                            ),
                            Divider(
                              height: 2,
                            )
                          ],
                        );
                      }),
                  ListView.builder(
                      itemCount: branchDataList.length,
                      itemBuilder: (BuildContext ctxt, int index) {
                        return Column(
                          children: <Widget>[
                            ListTile(
                              contentPadding: EdgeInsets.symmetric(
                                  horizontal: 20.0, vertical: 10.0),
                              title: Text(
                                branchDataList[index]["name"],
                                style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold),
                              ),
                              subtitle: Text(
                                "\nTelephone : " +
                                    branchDataList[index]["phone1"] +
                                    "\n\nAddress : " +
                                    branchDataList[index]["address"],
                                style: TextStyle(color: Colors.black),
                              ),
                              trailing: IconButton(
                                icon: Icon(Icons.phone, color: Colors.indigo),
                                onPressed: () {
                                  launch(
                                      'tel:' + branchDataList[index]["phone1"]);
                                },
                              ),
                              onTap: () {
                                _launchMapsUrl(
                                    branchDataList[index]["latitude"],
                                    branchDataList[index]["longitude"]);
                              },
                            ),
                            Divider(
                              height: 2,
                            )
                          ],
                        );
                      }),
                  ListView.builder(
                      itemCount: agentDataList.length,
                      itemBuilder: (BuildContext ctxt, int index) {
                        return Column(
                          children: <Widget>[
                            ListTile(
                              contentPadding: EdgeInsets.symmetric(
                                  horizontal: 20.0, vertical: 10.0),
                              title: Text(
                                agentDataList[index]["name"],
                                style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold),
                              ),
                              subtitle: Text(
                                "\nTelephone : " +
                                    agentDataList[index]["phone1"] +
                                    "\n\nAddress : " +
                                    agentDataList[index]["address"],
                                style: TextStyle(color: Colors.black),
                              ),
                              trailing: IconButton(
                                icon: Icon(Icons.phone, color: Colors.indigo),
                                onPressed: () {
                                  launch(
                                      'tel:' + agentDataList[index]["phone1"]);
                                },
                              ),
                              onTap: () {
                                _launchMapsUrl(agentDataList[index]["latitude"],
                                    agentDataList[index]["longitude"]);
                              },
                            ),
                            Divider(
                              height: 2,
                            )
                          ],
                        );
                      }),
                  ListView.builder(
                      itemCount: merchantDataList.length,
                      itemBuilder: (BuildContext ctxt, int index) {
                        return Column(
                          children: <Widget>[
                            ListTile(
                              contentPadding: EdgeInsets.symmetric(
                                  horizontal: 20.0, vertical: 10.0),
                              title: Text(
                                merchantDataList[index]["name"],
                                style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold),
                              ),
                              subtitle: Text(
                                "\nTelephone : " +
                                    merchantDataList[index]["phone1"] +
                                    "\n\nAddress : " +
                                    merchantDataList[index]["address"],
                                style: TextStyle(color: Colors.black),
                              ),
                              trailing: IconButton(
                                icon: Icon(Icons.phone, color: Colors.indigo),
                                onPressed: () {
                                  launch('tel:' +
                                      merchantDataList[index]["phone1"]);
                                },
                              ),
                              onTap: () {
                                _launchMapsUrl(
                                    merchantDataList[index]["latitude"],
                                    merchantDataList[index]["longitude"]);
                              },
                            ),
                            Divider(
                              height: 2,
                            )
                          ],
                        );
                      }),
                ]);

    var bodyProgress = new Container(
      child: new Stack(
        children: <Widget>[
          locationbody,
          Container(
              decoration: new BoxDecoration(
                color: Color.fromRGBO(255, 255, 255, 0.5),
              ),
              width: MediaQuery.of(context).size.width * 0.99,
              height: MediaQuery.of(context).size.height * 0.9,
              child: Center(
                child: CircularProgressIndicator(
                  backgroundColor: Colors.amber,
                ),
              ))
        ],
      ),
    );

    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: DefaultTabController(
            length: 4,
            child: Scaffold(
                backgroundColor: Colors.white,
                appBar: AppBar(
                  bottom: TabBar(
                    tabs: [
                      Tab(
                          child: Text("ATM",
                              style: TextStyle(
                                  fontSize: 11))),
                      Tab(
                          child: Text("BRANCH",
                              style: TextStyle(
                                  fontSize: 11))),
                      Tab(
                          child: Text("AGENT",
                              style: TextStyle(
                                  fontSize: 11))),
                      Tab(
                          child: Text("MERCHANT",
                              style: TextStyle(
                                  fontSize: 10))),
                    ],
                  ),
                  leading: new IconButton(
                      icon: new Icon(Icons.arrow_back),
                      // color: Colors.indigo,
                      disabledColor: Colors.indigo,
                      onPressed: () => Navigator.of(context).pop()),
                  backgroundColor: Color.fromRGBO(40, 103, 178, 1),
                  title: (checklang == "Eng")
                      ? Text(
                          (checklang == "Eng") ? textEng[0] : textMyan[0],
                          style: TextStyle(fontSize: 20), 
                        )
                      : Text(
                          (checklang == "Eng") ? textEng[0] : textMyan[0],
                          style: TextStyle(fontSize: 18),
                        ),
                  centerTitle: true,
                  actions: <Widget>[
                    new IconButton(
                        icon: Image.asset("assets/images/mapicon.jpg"),
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => LocationDetail()),
                          );
                        }),
                  ],
                ),
                body: isLoading ? bodyProgress : locationbody
                // body: TabBarView(children: [
                //   ListView.builder(
                //       itemCount: ATMDataList.length,
                //       itemBuilder: (BuildContext ctxt, int index) {
                //         return Column(
                //           children: <Widget>[
                //             ListTile(
                //               contentPadding: EdgeInsets.symmetric(
                //                   horizontal: 20.0, vertical: 10.0),
                //               title: Text(
                //                 ATMDataList[index]["name"],
                //                 style: TextStyle(
                //                     color: Colors.black,
                //                     fontWeight: FontWeight.bold),
                //               ),
                //               subtitle: Text(
                //                 "\nTelephone : " +
                //                     ATMDataList[index]["phone1"] +
                //                     "\n\nAddress : " +
                //                     ATMDataList[index]["address"],
                //                 style: TextStyle(color: Colors.black),
                //               ),
                //               trailing: IconButton(
                //                 icon: Icon(Icons.phone, color: Colors.indigo),
                //                 onPressed: () {
                //                   launch('tel:' + ATMDataList[index]["phone1"]);
                //                 },
                //               ),
                //               onTap: () {
                //                 _launchMapsUrl(ATMDataList[index]["latitude"],
                //                     ATMDataList[index]["longitude"]);
                //               },
                //             ),
                //             Divider(
                //               height: 2,
                //             )
                //           ],
                //         );
                //       }),
                //   ListView.builder(
                //       itemCount: branchDataList.length,
                //       itemBuilder: (BuildContext ctxt, int index) {
                //         return Column(
                //           children: <Widget>[
                //             ListTile(
                //               contentPadding: EdgeInsets.symmetric(
                //                   horizontal: 20.0, vertical: 10.0),
                //               title: Text(
                //                 branchDataList[index]["name"],
                //                 style: TextStyle(
                //                     color: Colors.black,
                //                     fontWeight: FontWeight.bold),
                //               ),
                //               subtitle: Text(
                //                 "\nTelephone : " +
                //                     branchDataList[index]["phone1"] +
                //                     "\n\nAddress : " +
                //                     branchDataList[index]["address"],
                //                 style: TextStyle(color: Colors.black),
                //               ),
                //               trailing: IconButton(
                //                 icon: Icon(Icons.phone, color: Colors.indigo),
                //                 onPressed: () {
                //                   launch(
                //                       'tel:' + branchDataList[index]["phone1"]);
                //                 },
                //               ),
                //               onTap: () {
                //                 _launchMapsUrl(
                //                     branchDataList[index]["latitude"],
                //                     branchDataList[index]["longitude"]);
                //               },
                //             ),
                //             Divider(
                //               height: 2,
                //             )
                //           ],
                //         );
                //       }),
                //   ListView.builder(
                //       itemCount: agentDataList.length,
                //       itemBuilder: (BuildContext ctxt, int index) {
                //         return Column(
                //           children: <Widget>[
                //             ListTile(
                //               contentPadding: EdgeInsets.symmetric(
                //                   horizontal: 20.0, vertical: 10.0),
                //               title: Text(
                //                 agentDataList[index]["name"],
                //                 style: TextStyle(
                //                     color: Colors.black,
                //                     fontWeight: FontWeight.bold),
                //               ),
                //               subtitle: Text(
                //                 "\nTelephone : " +
                //                     agentDataList[index]["phone1"] +
                //                     "\n\nAddress : " +
                //                     agentDataList[index]["address"],
                //                 style: TextStyle(color: Colors.black),
                //               ),
                //               trailing: IconButton(
                //                 icon: Icon(Icons.phone, color: Colors.indigo),
                //                 onPressed: () {
                //                   launch(
                //                       'tel:' + agentDataList[index]["phone1"]);
                //                 },
                //               ),
                //               onTap: () {
                //                 _launchMapsUrl(agentDataList[index]["latitude"],
                //                     agentDataList[index]["longitude"]);
                //               },
                //             ),
                //             Divider(
                //               height: 2,
                //             )
                //           ],
                //         );
                //       }),
                //   ListView.builder(
                //       itemCount: merchantDataList.length,
                //       itemBuilder: (BuildContext ctxt, int index) {
                //         return Column(
                //           children: <Widget>[
                //             ListTile(
                //               contentPadding: EdgeInsets.symmetric(
                //                   horizontal: 20.0, vertical: 10.0),
                //               title: Text(
                //                 merchantDataList[index]["name"],
                //                 style: TextStyle(
                //                     color: Colors.black,
                //                     fontWeight: FontWeight.bold),
                //               ),
                //               subtitle: Text(
                //                 "\nTelephone : " +
                //                     merchantDataList[index]["phone1"] +
                //                     "\n\nAddress : " +
                //                     merchantDataList[index]["address"],
                //                 style: TextStyle(color: Colors.black),
                //               ),
                //               trailing: IconButton(
                //                 icon: Icon(Icons.phone, color: Colors.indigo),
                //                 onPressed: () {
                //                   launch('tel:' +
                //                       merchantDataList[index]["phone1"]);
                //                 },
                //               ),
                //               onTap: () {
                //                 _launchMapsUrl(
                //                     merchantDataList[index]["latitude"],
                //                     merchantDataList[index]["longitude"]);
                //               },
                //             ),
                //             Divider(
                //               height: 2,
                //             )
                //           ],
                //         );
                //       }),
                // ])
                )
                )
                );
  }
}
