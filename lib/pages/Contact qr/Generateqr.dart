import 'dart:io';
import 'dart:typed_data';
import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:nsb/pages/Wallet.dart';
import 'package:nsb/utils/crypt_helper.dart';
import 'package:path_provider/path_provider.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Generateqr extends StatefulWidget {
  @override
  _CashInConfirmState createState() => _CashInConfirmState();
}

class _CashInConfirmState extends State<Generateqr> {
  GlobalKey globalKey = new GlobalKey();
  String _scanBarcode = "";
  String _dataString = "Hello from this QR";
  String _inputErrorText;
  String res, id, user;

  void initState() {
    super.initState();
    getinfo();
  }

  getinfo() async {
    final prefs = await SharedPreferences.getInstance();
    String userID = prefs.getString('userId');
    String name = prefs.getString('name');
    id = userID;
    user = name;
    setState(() {
      _dataString = '{"contacts":"' + userID + '","name":"' + name + '"}';
      _inputErrorText = null;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        //Application Bar
        elevation: 0.0,
        backgroundColor: Color.fromRGBO(40, 103, 178, 1),
        title: new Center(
          child: new Text(
            'QR',
            style: TextStyle(
                fontSize: 20.0,
                color: Colors.white,
                height: 1.0,
                fontWeight: FontWeight.w600),
          ),
        ),
      ),
      body: new Form(
        key: globalKey,
        child: new ListView(
          children: <Widget>[
            Center(
              child: new Container(
                padding: EdgeInsets.only(left: 30.0, right: 30.0),
              ),
            ),
            SizedBox(height: 10.0),
            Center(
                child: Text('$user',
                    style: TextStyle(
                        fontSize: 18.0, fontWeight: FontWeight.bold))),
            SizedBox(height: 15.0),
            Center(
                child: Text('$id',
                    style: TextStyle(
                        fontSize: 18.0, fontWeight: FontWeight.bold))),
            SizedBox(height: 30.0),
            Center(
                child: Text("Add contact with QR Scan",
                    style: TextStyle(fontSize: 17.0))),
            SizedBox(height: 30.0),
            Center(
                child: RepaintBoundary(
              child: QrImage(
                data: _dataString,
                backgroundColor: Colors.white,
                size: 300,
                gapless: true,
              ),
            )),
            SizedBox(height: 25.0),
            Center(
              child: Container(
                width: 300,
                height: 50,
                child: RaisedButton(
                  color: Color.fromRGBO(40, 103, 178, 1),
                  elevation: 5.0,
                  splashColor: Colors.red,
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => WalletPage()));
                  },
                  child: new Text(
                    "Close",
                    style: TextStyle(fontSize: 18),
                  ),
                  textColor: Colors.white,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
