import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:nsb/constants/constant.dart';
import 'package:nsb/constants/rout_path.dart' as routes;
import 'package:nsb/model/GoTransferRequest.dart';
import 'package:nsb/model/GoTransferResponse.dart';
import 'package:nsb/pages/Transfer/transfersuccess.dart';
import 'package:nsb/utils/crypt_helper.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:nsb/framework/http_service.dart' as http;

class TransferConfirmPage extends StatefulWidget {
  final String value1;
  final String value2;
  final String value3;
  final String value4;

  TransferConfirmPage(
      {Key key, this.value1, this.value2, this.value3, this.value4})
      : super(key: key);

  @override
  _TransferConfirmPageState createState() => _TransferConfirmPageState();
}

class _TransferConfirmPageState extends State<TransferConfirmPage> {
  String alertmsg = "";
  String rkey = "";
  String password = "";
  bool _isLoading;
  bool isLoading = false;
  String checklang = '';
  final myControlleramout = TextEditingController();
  final myControllerref = TextEditingController();
  final myControllerphone = TextEditingController();
  final myControllername = TextEditingController();
  List textMyan = [
    "အတည်ပြုခြင်း",
    "ဖုန်းနံပါတ်",
    "အမည်",
    "ငွေပမာဏ",
    "အကြောင်းအရာ",
    "ပယ်ဖျက်မည်",
    "လုပ်ဆောင်မည်",
    "ကျပ်"
  ];
  List textEng = [
    "Transfer Confirm",
    "Phone Number",
    "Name",
    "Amount",
    "Reference",
    "Cancel",
    "Confirm",
    "MMK"
  ];
  final _formKey = new GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldkey = new GlobalKey<ScaffoldState>();
  static final CREATE_POST_URL =
      'http://122.248.120.16:8080/WalletService/module001';

  void initState() {
    this._isLoading = false;
    // _isButtonDisabled = false;
    myControlleramout.text = widget.value3;
    myControllerref.text = widget.value4;
    myControllerphone.text = widget.value1;
    myControllername.text = widget.value2;
    checkLanguage();
    super.initState();
  }

  checkLanguage() async {
    final prefs = await SharedPreferences.getInstance();
    checklang = prefs.getString("Lang");
    // print(lang);
    if (checklang == "" || checklang == null || checklang.length == 0) {
      checklang = "Eng";
    } else {
      checklang = checklang;
    }
    setState(() {});
  }

  void _method1() {
    _scaffoldkey.currentState.showSnackBar(new SnackBar(
        content: new Text(this.alertmsg), duration: Duration(seconds: 1)));
  }

  Future _transfer() async{
        setState((){
          isLoading = false;
        });
      }

//locationList[0]["name"]
//locationList[0].name
  Widget build(BuildContext context) {
    final style = TextStyle(fontFamily: 'Montserrat', fontSize: 16.0);

    final phoneNumber = new TextFormField(
      controller: myControllerphone,
      readOnly: true,
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
        // icon: const Icon(Icons.attach_money),
        labelText: (checklang == "Eng") ? textEng[1] : textMyan[1],
        hasFloatingPlaceholder: true,
        labelStyle: (checklang == "Eng")
            ? TextStyle(fontSize: 17, color: Colors.black,height: 0, fontWeight: FontWeight.w300)
            : TextStyle(fontSize: 16, color: Colors.black, height: 0),
        fillColor: Colors.black87,
      ),
    );

    final name = new TextFormField(
      controller: myControllername,
      readOnly: true,
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
        // icon: const Icon(Icons.attach_money),

        labelText: (checklang == "Eng") ? textEng[2] : textMyan[2],
        hasFloatingPlaceholder: true,
        labelStyle: (checklang == "Eng")
            ? TextStyle(fontSize: 17, color: Colors.black,height: 0, fontWeight: FontWeight.w300)
            : TextStyle(fontSize: 16, color: Colors.black, height: 0),
        fillColor: Colors.black87,
      ),
    );

    final amount = new TextFormField(
      controller: myControlleramout,
      readOnly: true,
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
        // icon: const Icon(Icons.attach_money),
        suffixText: "MMK",
        labelText: (checklang == "Eng") ? textEng[3] : textMyan[3],
        hasFloatingPlaceholder: true,
        labelStyle: (checklang == "Eng")
            ? TextStyle(fontSize: 17, color: Colors.black,height: 0, fontWeight: FontWeight.w300)
            : TextStyle(fontSize: 16, color: Colors.black, height: 0),
        fillColor: Colors.black87,
      ),
    );

    final reference = new TextFormField(
      controller: myControllerref,
      readOnly: true,
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
        // icon: const Icon(Icons.attach_money),
        // suffixText: "MMK",
        labelText: (checklang == "Eng") ? textEng[4] : textMyan[4],
        hasFloatingPlaceholder: true,
        labelStyle: (checklang == "Eng")
            ? TextStyle(fontSize: 17, color: Colors.black,height: 0, fontWeight: FontWeight.w300)
            : TextStyle(fontSize: 16, color: Colors.black, height: 0),
        fillColor: Colors.black87,
      ),
    );

    // final phoneNumber = new Container(
    //     child: Column(children: <Widget>[
    //   ListTile(
    //     title: Padding(
    //       padding: const EdgeInsets.fromLTRB(5, 2, 8, 15),
    //       child: Text(
    //         (checklang == "Eng") ? textEng[1] : textMyan[1],
    //         style: TextStyle(fontSize: 15),
    //       ),
    //     ),
    //     subtitle: Padding(
    //       padding: const EdgeInsets.all(5.0),
    //       child: Text("${widget.value1}",
    //           style: TextStyle(fontSize: 18, color: Colors.grey)),
    //     ),
    //   ),
    //   Divider(
    //     color: Colors.black,
    //   )
    // ]));

    // final name = new Container(
    //     child: Column(children: <Widget>[
    //   ListTile(
    //     title: Padding(
    //       padding: const EdgeInsets.fromLTRB(8, 2, 8, 15),
    //       child: Text(
    //         (checklang == "Eng") ? textEng[2] : textMyan[2],
    //         style: TextStyle(fontSize: 15),
    //       ),
    //     ),
    //     subtitle: Padding(
    //       padding: const EdgeInsets.all(5.0),
    //       child: Text("${widget.value2}",
    //           style: TextStyle(fontSize: 18, color: Colors.grey)),
    //     ),
    //   ),
    //   Divider(
    //     color: Colors.black,
    //   )
    // ]));

    // final amount = new Container(
    //     child: Column(children: <Widget>[
    //   ListTile(
    //     title: Padding(
    //       padding: const EdgeInsets.fromLTRB(5, 2, 8, 15),
    //       child: Text(
    //         (checklang == "Eng") ? textEng[3] : textMyan[3],
    //         style: TextStyle(fontSize: 15, color: Colors.black),
    //       ),
    //     ),
    //     subtitle: Padding(
    //       padding: const EdgeInsets.all(5.0),
    //       child: Text("${widget.value3}" + " .00MMK",
    //           style: TextStyle(fontSize: 18, color: Colors.grey)),
    //     ),
    //   ),
    //   Divider(
    //     color: Colors.black,
    //   )
    // ]));

    // final reference = new Container(
    //     child: Column(children: <Widget>[
    //   ListTile(
    //     title: Padding(
    //       padding: const EdgeInsets.fromLTRB(5, 2, 8, 15),
    //       child: Text(
    //         (checklang == "Eng") ? textEng[4] : textMyan[4],
    //         style: TextStyle(fontSize: 15, color: Colors.black),
    //       ),
    //     ),
    //     subtitle: Padding(
    //       padding: const EdgeInsets.all(5.0),
    //       child: Text("${widget.value4}",
    //           style: TextStyle(fontSize: 18, color: Colors.grey)),
    //     ),
    //   ),
    //   Divider(
    //     color: Colors.black,
    //   )
    // ]));

    final cancelbutton = new RaisedButton(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      onPressed: () async {
        this.alertmsg = '';
        Navigator.of(context).pushNamed(routes.TransferPageRoute);
      },
      color: Colors.grey[400],
      textColor: Colors.white,
      child: Container(
        width: 130.0,
        height: 43.0,
        child: Center(
          child: (checklang == "Eng")
              ? Text(
                  (checklang == "Eng") ? textEng[5] : textMyan[5],
                  style: TextStyle(fontSize: 18),
                )
              : Text(
                  (checklang == "Eng") ? textEng[5] : textMyan[5],
                  style: TextStyle(fontSize: 16),
                ),
        ),
      ),
    );

    final transferbutton = new RaisedButton(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      onPressed: () async {
        setState(() {
        isLoading = true;
        });
        final prefs = await SharedPreferences.getInstance();
        String userID = prefs.getString('userId');
        String username = prefs.getString('name');
        String sessionID = prefs.getString('sessionID');
        final iv = AesUtil.random(16);
        print("iv :" + iv);
        final dm = AesUtil.random(16);
        print("dm :" + dm);
        final salt = AesUtil.random(16);
        print("salt :" + salt);
        String res = AesUtil.encrypt(salt, iv, this.password);
        print("res is :" + res);
        GoTransferRequest goTransferRequest = new GoTransferRequest(
            token: sessionID,
            senderCode: userID,
            receiverCode: "${widget.value2}",
            fromName: username,
            toName: "${widget.value1}",
            amount: "${widget.value3}",
            prevBalance: "",
            password: res,
            iv: iv,
            dm: dm,
            salt: salt,
            appType: "wallet");
        GoTransferResponse goTransferResponse = await goOpenAccount(
            CREATE_POST_URL + '/payment/goTransfer', goTransferRequest.toMap());
        if (goTransferResponse.code == '0000') {
          setState(() {
            isLoading = false;
           });
          this.alertmsg = "";
          print(goTransferResponse.toString());
          var route = new MaterialPageRoute(
              builder: (BuildContext context) => new TransferSuccessPage(
                  value: "${widget.value3}",
                  value1: goTransferResponse.bankRefNumber,
                  value2: goTransferResponse.transactionDate,
                  value3: goTransferRequest.toName));
          Navigator.of(context).push(route);
          this.alertmsg = goTransferResponse.desc;
          this._method1();
        } else {
          this.alertmsg = goTransferResponse.desc;
          this._method1();
          final form = _formKey.currentState;
          form.validate();
          this._isLoading = false;
          setState(() {
            isLoading = true;
             new Future.delayed(new Duration(seconds: 3), _transfer);
          });
        }
      },
      color: Color.fromRGBO(40, 103, 178, 1),
      textColor: Colors.white,
      child: Container(
        width: 130.0,
        height: 43.0,
        child: Center(
          child: (checklang == "Eng")
              ? Text(
                  (checklang == "Eng") ? textEng[6] : textMyan[6],
                  style: TextStyle(fontSize: 18),
                )
              : Text(
                  (checklang == "Eng") ? textEng[6] : textMyan[6],
                  style: TextStyle(fontSize: 16),
                ),
        ),
      ),
    );

    var body =new Form(
        key: _formKey,
        child: new ListView(
          children: <Widget>[
            SizedBox(height: 10.0),
            Container(
              padding: EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 0.0),
              height: 550,
              child: Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20.0),
                ),
                elevation: 3.0,
                child: ListView(
                  padding: EdgeInsets.all(2.0),
                  children: <Widget>[
                    // SizedBox(height: 20.0),
                    Center(
                      child: new Container(
                        padding: EdgeInsets.only(left: 15.0, right: 10.0),
                      ),
                    ),
                    SizedBox(height: 30.0),
                    Center(
                      child: new Container(
                        padding: EdgeInsets.only(left: 15.0, right: 10.0),
                        child: phoneNumber,
                      ),
                    ),
                    SizedBox(height: 30.0),
                    Center(
                      child: new Container(
                        padding: EdgeInsets.only(left: 15.0, right: 10.0),
                        child: name,
                      ),
                    ),
                    SizedBox(height: 30.0),
                    Center(
                      child: new Container(
                        padding: EdgeInsets.only(left: 15.0, right: 10.0),
                        child: amount,
                      ),
                    ),
                    SizedBox(height: 30.0),
                    Center(
                      child: new Container(
                        padding: EdgeInsets.only(left: 15.0, right: 10.0),
                        child: reference,
                      ),
                    ),
                    SizedBox(height: 40.0),
                    Row(
                      children: <Widget>[
                        new Container(
                          padding: EdgeInsets.only(left: 12.0),
                          child: cancelbutton,
                        ),
                        new Container(
                          padding: EdgeInsets.only(left: 14.0),
                          child: transferbutton,
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      );

    var bodyProgress = new Container(
      child: new Stack(
        children: <Widget>[
          body,
          Container(
              decoration: new BoxDecoration(
                color: Color.fromRGBO(255, 255, 255, 0.5),
              ),
              width: MediaQuery.of(context).size.width * 0.99,
              height: MediaQuery.of(context).size.height * 0.9,
              child: Center(
                child: CircularProgressIndicator(
                  backgroundColor: Colors.amber,
                ),
              ))
        ],
      ),
    );

    return Scaffold(
      key: _scaffoldkey,
      backgroundColor: Colors.grey[200],
      appBar: new AppBar(
        //Application Bar
        elevation: 0.0,
        backgroundColor: Color.fromRGBO(40, 103, 178, 1),
        centerTitle: true,
          title: (checklang == "Eng")
            ? Text(
                (checklang == "Eng") ? textEng[0] : textMyan[0],
                style: TextStyle(fontSize: 20, color: Colors.white),
              )
            : Text(
                (checklang == "Eng") ? textEng[0] : textMyan[0],
                style: TextStyle(fontSize: 18, color: Colors.white),
              ),
      ),
      body: isLoading ? bodyProgress : body,
      
    );
  }

  Future<GoTransferResponse> goOpenAccount(url, Map jsonMap) async {
    GoTransferResponse p = new GoTransferResponse();
    var body;
    try {
      body = await http.doPost(url, jsonMap);
      p = GoTransferResponse.fromJson(json.decode(body.toString()));
    } catch (e) {
      p.code = Constants.responseCode_Error;
      p.desc = e.toString();
    }
    print(p.toMap());
    return p;
  }
}

class AlwaysDisabledFocusNode extends FocusNode {
  @override
  bool get hasFocus => false;
}
// Future<dynamic> get(String url, Map jsonMap) async{
//     return http
//         .get(
//       url,
//     )
//         .then((http.Response response) {
//       String res = response.body;
//       int statusCode = response.statusCode;
//       print("API Response: " + res);
//       if (statusCode < 200 || statusCode > 400 || json == null) {
//         res = "{\"status\":"+
//             statusCode.toString() +
//             ",\"message\":\"error\",\"response\":" +
//             res +
//             "}";
//         throw new Exception( statusCode);
//       }
//       return _decoder.convert(res);
//     });
//   }
