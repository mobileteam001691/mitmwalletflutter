import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:nsb/constants/constant.dart';
import 'package:nsb/model/ContactRequest.dart';
import 'package:nsb/model/ContactResponse.dart';
import 'package:nsb/pages/Transfer/transfer.dart';
import 'package:nsb/pages/Transfer/transferBind.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:nsb/framework/http_service.dart' as http;

class TransferContact extends StatefulWidget {
  @override
  _TransferContactState createState() => _TransferContactState();
}

class _TransferContactState extends State<TransferContact> {
  String phoneno;
  String alertmsg = "";
  bool _isLoading;
  bool isLoading = true;
  String phoneNo = "";
  String checklang = '';
  List textMyan = ["ငွေလွှဲမည့်သူရွေးရန်", "ဖုန်းနံပါတ်", "Next"];
  List textEng = ["Transfer To", "Phone Number", "Next"];
  final _formKey = new GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldkey = new GlobalKey<ScaffoldState>();
  static final CREATE_POST_URL =
      'http://122.248.120.16:8080/WalletService/module001';
  var contactList = [];
  final myController = TextEditingController();

  void _method1() {
    _scaffoldkey.currentState.showSnackBar(new SnackBar(
        content: new Text(this.alertmsg), duration: Duration(seconds: 2)));
  }

  @override
  void initState() {
    super.initState();
    checkLanguage();
    this.getContactList();
  }

  checkLanguage() async {
    final prefs = await SharedPreferences.getInstance();
    checklang = prefs.getString("Lang");
    // print(lang);
    if (checklang == "" || checklang == null || checklang.length == 0) {
      checklang = "Eng";
    } else {
      checklang = checklang;
    }
    setState(() {});
  }

  getContactList() async {
    final prefs = await SharedPreferences.getInstance();
    String userID = prefs.getString('userId');
    String sessionID = prefs.getString('sessionID');

    SessionData contactlistrequest =
        new SessionData(userID: userID, sessionID: sessionID);
    ContactArr contactlistresponse = await getAllList(
        CREATE_POST_URL + '/chatservice/getContact',
        contactlistrequest.toMap());
    if (contactlistresponse.code == '0000') {
      print(contactList[9]);
      setState(() {
        isLoading = false;
      });
    } else {
      setState(() {
        isLoading = true;
      });
    }
  }

  Future<ContactArr> getAllList(url, Map jsonMap) async {
    ContactArr p = new ContactArr();
    var body;
    try {
      body = await http.doPost(url, jsonMap);
      p = ContactArr.fromJson(json.decode(body.toString()));
      Map data = jsonDecode(body);
      setState(() {
        contactList = data["dataList"];
      });
    } catch (e) {
      p.code = Constants.responseCode_Error;
      p.desc = e.toString();
    }
    print(p.toMap());
    return p;
  }

  @override
  Widget build(BuildContext context) {
    final passwordField2 = new TextFormField(
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
          labelText: (checklang == "Eng") ? textEng[1] : textMyan[1],
          hasFloatingPlaceholder: true,
          labelStyle: (checklang == "Eng")
              ? TextStyle(
                  fontSize: 17,
                  color: Colors.black,
                  height: 0,
                  fontWeight: FontWeight.w300)
              : TextStyle(fontSize: 16, color: Colors.black, height: 0),
          fillColor: Colors.black87,
          suffixIcon: IconButton(
            padding: EdgeInsets.all(0),
            onPressed: () {
              print(Text("gg"));
            },
            iconSize: 25,
            icon: Icon(
              Icons.contacts,
              color: Color.fromRGBO(40, 103, 178, 1),
              size: 30,
            ),
          )),
    );

    final nextButton = Padding(
        padding: EdgeInsets.fromLTRB(0.0, 20.0, 00.0, 0.0),
        child: SizedBox(
          height: 60.0,
          width: MediaQuery.of(context).size.width,
          child: new RaisedButton(
            elevation: 5.0,
            shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(10.0)),
            child: new Text(
              (checklang == "Eng") ? textEng[2] : textMyan[2],
              style: TextStyle(fontSize: 18),
            ),
            textColor: Colors.white,
            onPressed: () async {},
            color: Color.fromRGBO(40, 103, 178, 1),
          ),
        ));
    var body = new Form(
      child: new ListView(
        children: <Widget>[
          SizedBox(height: 10.0),
          Container(
            padding: EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 0.0),
            height: 120,
            child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
              ),
              elevation: 3.0,
              child: ListView(
                padding: EdgeInsets.all(5.0),
                children: <Widget>[
                  SizedBox(height: 20.0),
                  Center(
                    child: new Container(
                      padding: EdgeInsets.only(left: 22.0, right: 10.0),
                      child: passwordField2,
                    ),
                  ),
                ],
              ),
            ),
          ),
          SizedBox(height: 5.0),
          Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  width: 340,
                  height: 65,
                  child: nextButton,
                ),
              ],
            ),
          ),
          SizedBox(height: 20),
          Container(
            padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
            height: 480,
            child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5.0),
              ),
              elevation: 3.0,
              child: ListView.separated(
                padding: EdgeInsets.all(5.0),
                separatorBuilder: (context, index) => Divider(
                  color: Colors.black,
                ),
                key: _formKey,
                itemCount: contactList == null ? 0 : contactList.length,
                itemBuilder: (BuildContext context, int index) {
                  return ListTile(
                    leading: CircleAvatar(
                      radius: 30.0,
                      child:
                          Text("${contactList[index]["name"]}".substring(0, 1)),
                    ),
                    contentPadding:
                        EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
                    title: Text("${contactList[index]["name"]}",
                        style: TextStyle(
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold,
                            color: Colors.black)),
                    subtitle: Text("${contactList[index]["phone"]}"),
                    onTap: () {
                      // (contactList[index]["name"],contactList[index]["phone"]);
                      var route = new MaterialPageRoute(
                          builder: (BuildContext context) => new transferBind(
                              value: (contactList[index]["name"]),
                              value1: (contactList[index]["phone"])));
                      Navigator.of(context).push(route);
                    },
                  );
                },
              ),
            ),
          ),
        ],
      ),
    );
    var bodyProgress = new Container(
      child: new Stack(
        children: <Widget>[
          body,
          Container(
              decoration: new BoxDecoration(
                color: Color.fromRGBO(255, 255, 255, 0.5),
              ),
              width: MediaQuery.of(context).size.width * 0.99,
              height: MediaQuery.of(context).size.height * 0.9,
              child: Center(
                child: CircularProgressIndicator(
                  backgroundColor: Colors.amber,
                ),
              ))
        ],
      ),
    );

    return Scaffold(
      key: _scaffoldkey,
      backgroundColor: Colors.grey[200],
      appBar: new AppBar(
        //Application Bar
        centerTitle: true,
        elevation: 0.0,
        backgroundColor: Color.fromRGBO(40, 103, 178, 1),
        title: (checklang == "Eng")
            ? Text(
                (checklang == "Eng") ? textEng[0] : textMyan[0],
                style: TextStyle(fontSize: 20, color: Colors.white),
              )
            : Text(
                (checklang == "Eng") ? textEng[0] : textMyan[0],
                style: TextStyle(fontSize: 18, color: Colors.white),
              ),

        actions: <Widget>[
          IconButton(
            padding: EdgeInsets.all(0),
            onPressed: () {
              print(Text("ez"));
            },
            iconSize: 25,
            icon: Icon(
              Icons.view_list,
              color: Colors.white,
            ),
          )
        ],
      ),
      body: isLoading ? bodyProgress : body,
    );
  }
}
