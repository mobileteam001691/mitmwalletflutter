import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:nsb/constants/constant.dart';
import 'package:nsb/constants/rout_path.dart' as routes;
import 'package:nsb/model/ReadMessageRequest.dart';
import 'package:nsb/model/ReadMessageResponse.dart';
import 'package:nsb/pages/Transfer/transferconfirm.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:nsb/framework/http_service.dart' as http;

class TransferPage extends StatefulWidget {
  @override
  _TransferPageState createState() => _TransferPageState();
}

class _TransferPageState extends State<TransferPage> {
  String alertmsg = "";
  String rkey = "";
  bool _isLoading;
  final myControllerno = TextEditingController();
  final myControllername = TextEditingController();
  final myControlleramout = TextEditingController();
  final myControllerref = TextEditingController();
  String checklang = '';
  List textMyan = ["ငွေလွှဲမည့်သူရွေးရန်", "ဖုန်းနံပါတ်" , "Next"];
  List textEng = ["Transfer To", "Phone Number", "Next"];

  final _formKey = new GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldkey = new GlobalKey<ScaffoldState>();
  static final CREATE_POST_URL =
      'http://122.248.120.16:8080/WalletService/module001';

  void initState() {
    this._isLoading = false;
    super.initState();
  }

  void _method1() {
    print("Snack Bar");
    print(this.alertmsg);
    _scaffoldkey.currentState.showSnackBar(new SnackBar(
        content: new Text(this.alertmsg), duration: Duration(seconds: 1)));
  }

  checkLanguage() async {
    final prefs = await SharedPreferences.getInstance();
    checklang = prefs.getString("Lang");
    // print(lang);
    if (checklang == "" || checklang == null || checklang.length == 0) {
      checklang = "Eng";
    } else {
      checklang = checklang;
    }
    setState(() {});
  }

  Widget build(BuildContext context) {
    final style = TextStyle(fontFamily: 'Montserrat', fontSize: 16.0);
    final loginField = new TextFormField(
      controller: myControllerno,
      autofocus: true,
      decoration: InputDecoration(
        icon: const Icon(Icons.phone_android),
        labelText: "Mobile Number",
        hasFloatingPlaceholder: true,
        labelStyle: TextStyle(fontSize: 15, color: Colors.grey),
        fillColor: Colors.black87,
      ),
    );

    final passwordField = new TextFormField(
      controller: myControllername,
      decoration: InputDecoration(
        icon: const Icon(Icons.person_add),
        labelText: "Name",
        hasFloatingPlaceholder: true,
        labelStyle: TextStyle(fontSize: 15, color: Colors.grey),
        fillColor: Colors.black87,
      ),
    );

    final passwordField2 = new TextFormField(
      controller: myControlleramout,
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
        icon: const Icon(Icons.attach_money),
        suffixText: "MMK",
        labelText: "Amount",
        hasFloatingPlaceholder: true,
        labelStyle: TextStyle(fontSize: 15, color: Colors.grey),
        fillColor: Colors.black87,
      ),
    );
    final referenceField = new TextFormField(
      controller: myControllerref,
      decoration: InputDecoration(
        icon: const Icon(Icons.add_box),
        labelText: "Description",
        hasFloatingPlaceholder: true,
        labelStyle: TextStyle(fontSize: 15, color: Colors.grey),
        fillColor: Colors.black87,
      ),
    );

    final transferbutton = new RaisedButton(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      onPressed: () async {
        this.alertmsg = '';
        final prefs = await SharedPreferences.getInstance();
        String userID = prefs.getString('userId');
        String sessionID = prefs.getString('sessionID');
        ReadMessageRequest readMessageRequest = new ReadMessageRequest(
          userID: userID,
          sessionID: sessionID,
          type: "12",
          merchantID: "",
        );
        ReadMessageResponse readMessageResponse = await goLogin(
            CREATE_POST_URL + '/service002/readMessageSetting',
            readMessageRequest.toMap());
        print(userID);
        print(sessionID);
        if (readMessageResponse.code == '0000') {
          print(readMessageResponse.toString());
          var route = new MaterialPageRoute(
              builder: (BuildContext context) => new TransferConfirmPage(
                  value1: myControllerno.text,
                  value2: myControllername.text,
                  value3: myControlleramout.text,
                  value4: myControllerref.text));
          Navigator.of(context).push(route);
        }
      },
      color: Color.fromRGBO(40, 103, 178, 1),
      textColor: Colors.white,
      child: Container(
        width: 300.0,
        height: 43.0,
        child: Center(child: Text('Transfer', style: TextStyle(fontSize: 18))),
      ),
    );

    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: new AppBar(
          //Application Bar
          elevation: 0.0,
          backgroundColor: Color.fromRGBO(40, 103, 178, 1),
          centerTitle: true,
          title:Text(
              'Transfer',
              style: TextStyle(
                  fontSize: 20.0,
                  color: Colors.white,
                  height: 1.0,
                  fontWeight: FontWeight.w600),
            ),
          ),
      body: new Form(
        key: _formKey,
        child: new ListView(
          children: <Widget>[
            SizedBox(height: 10.0),
            Container(
              padding: EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 0.0),
              height: 430,
              child: Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                elevation: 3.0,
                child: ListView(
                  padding: EdgeInsets.all(5.0),
                  children: <Widget>[
                    Center(
                      child: new Container(
                        padding: EdgeInsets.only(left: 10.0, right: 10.0),
                      ),
                    ),
                    Center(
                      child: new Container(
                        padding: EdgeInsets.only(left: 10.0, right: 10.0),
                        child: loginField,
                      ),
                    ),
                    SizedBox(height: 20.0),
                    Center(
                      child: new Container(
                        padding: EdgeInsets.only(left: 10.0, right: 10.0),
                        child: passwordField,
                      ),
                    ),
                    SizedBox(height: 20.0),
                    Center(
                      child: new Container(
                        padding: EdgeInsets.only(left: 10.0, right: 10.0),
                        child: passwordField2,
                      ),
                    ),
                    SizedBox(height: 20.0),
                    Center(
                      child: new Container(
                        padding: EdgeInsets.only(left: 10.0, right: 10.0),
                        child: referenceField,
                      ),
                    ),
                    SizedBox(height: 40.0),
                    Center(
                      child: new Container(
                        padding: EdgeInsets.only(left: 30.0, right: 30.0),
                        child: transferbutton,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<ReadMessageResponse> goLogin(url, Map jsonMap) async {
    ReadMessageResponse p = new ReadMessageResponse();
    var body;
    try {
      body = await http.doPost(url, jsonMap);
      p = ReadMessageResponse.fromJson(json.decode(body.toString()));
    } catch (e) {
      p.otpCode = Constants.responseCode_Error;
      p.otpMessage = e.toString();
    }
    print(p.toMap());
    return p;
  }
}
