import 'dart:convert';
import 'package:nsb/framework/http_service.dart' as http;
import 'package:flutter/material.dart';
import 'package:nsb/constants/constant.dart';
import 'package:nsb/model/ReadMessageRequest.dart';
import 'package:nsb/model/ReadMessageResponse.dart';
import 'package:nsb/pages/Transfer/transferconfirm.dart';
import 'package:shared_preferences/shared_preferences.dart';

class transferBind extends StatefulWidget {
  final String value;
  final String value1;

  transferBind({Key key, this.value, Key key1, this.value1}) : super(key: key);

  @override
  _transferBindState createState() => _transferBindState();
}

class _transferBindState extends State<transferBind> {
  String alertmsg = "";
  String rkey = "";
  bool _isLoading;
  String checklang = '';
  List textMyan = [
    "ငွေလွှဲရန်",
    "ဖုန်းနံပါတ်",
    "အမည်",
    "ငွေပမာဏ",
    "အကြောင်းအရာ",
    "ငွေလွှဲမည်"
  ];
  List textEng = [
    "Transfer",
    "Phone Number",
    "Name",
    "Amount",
    "Reference",
    "Transfer"
  ];

  final myControlleramout = TextEditingController();
  final myControllerref = TextEditingController();
  final myControllerphone = TextEditingController();
  final myControllername = TextEditingController();

  final _formKey = new GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldkey = new GlobalKey<ScaffoldState>();
  static final CREATE_POST_URL =
      'http://122.248.120.16:8080/WalletService/module001';

  void initState() {
    checkLanguage();
    myControllerphone.text = widget.value1;
    myControllername.text = widget.value;
    this._isLoading = false;
    super.initState();
  }

  checkLanguage() async {
    final prefs = await SharedPreferences.getInstance();
    checklang = prefs.getString("Lang");
    // print(lang);
    if (checklang == "" || checklang == null || checklang.length == 0) {
      checklang = "Eng";
    } else {
      checklang = checklang;
    }
    setState(() {});
  }

  void _method1() {
    print("Snack Bar");
    print(this.alertmsg);
    _scaffoldkey.currentState.showSnackBar(new SnackBar(
        content: new Text(this.alertmsg), duration: Duration(seconds: 1)));
  }

  @override
  Widget build(BuildContext context) {
    // final phoneNumber = new Container(
    //     child: Column(children: <Widget>[
    //   ListTile(
    //     title: Padding(
    //       padding: const EdgeInsets.fromLTRB(0, 2, 5, 15),
    //       child: Text(
    //         (checklang == "Eng") ? textEng[1] : textMyan[1],
    //         style: TextStyle(fontSize: 15, color: Colors.black),
    //       ),
    //     ),
    //     subtitle: Padding(
    //       padding: const EdgeInsets.all(0.0),
    //       child: Text("${widget.value}",
    //           style: TextStyle(fontSize: 18, color: Colors.grey)),
    //     ),
    //   ),
    //   Divider(
    //     color: Colors.black,
    //   )
    // ]));

    final phoneNumber = new TextFormField(
      controller: myControllerphone,
      readOnly: true,
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
        // icon: const Icon(Icons.attach_money),
        labelText: (checklang == "Eng") ? textEng[1] : textMyan[1],
        hasFloatingPlaceholder: true,
        labelStyle: (checklang == "Eng")
            ? TextStyle(
                fontSize: 17,
                color: Colors.black,
                height: 0,
                fontWeight: FontWeight.w300)
            : TextStyle(fontSize: 16, color: Colors.black, height: 0),
        fillColor: Colors.black87,
      ),
    );

    final name = new TextFormField(
      controller: myControllername,
      readOnly: true,
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
        // icon: const Icon(Icons.attach_money),

        labelText: (checklang == "Eng") ? textEng[2] : textMyan[2],
        hasFloatingPlaceholder: true,
        labelStyle: (checklang == "Eng")
            ? TextStyle(
                fontSize: 17,
                color: Colors.black,
                height: 0,
                fontWeight: FontWeight.w300)
            : TextStyle(fontSize: 16, color: Colors.black, height: 0),
        fillColor: Colors.black87,
      ),
    );

    final amount = new TextFormField(
      controller: myControlleramout,
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
        // icon: const Icon(Icons.attach_money),
        suffixText: "MMK",
        labelText: (checklang == "Eng") ? textEng[3] : textMyan[3],
        hasFloatingPlaceholder: true,
        labelStyle: (checklang == "Eng")
            ? TextStyle(
                fontSize: 17,
                color: Colors.black,
                height: 0,
                fontWeight: FontWeight.w300)
            : TextStyle(fontSize: 16, color: Colors.black, height: 0),
        fillColor: Colors.black87,
      ),
    );

    final reference = new TextFormField(
      controller: myControllerref,
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
        // icon: const Icon(Icons.attach_money),
        // suffixText: "MMK",
        labelText: (checklang == "Eng") ? textEng[4] : textMyan[4],
        hasFloatingPlaceholder: true,
        labelStyle: (checklang == "Eng")
            ? TextStyle(
                fontSize: 17,
                color: Colors.black,
                height: 0,
                fontWeight: FontWeight.w300)
            : TextStyle(fontSize: 16, color: Colors.black, height: 0),
        fillColor: Colors.black87,
      ),
    );

    final transferbutton = new RaisedButton(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      onPressed: () async {
        this.alertmsg = '';
        final prefs = await SharedPreferences.getInstance();
        String userID = prefs.getString('userId');
        String sessionID = prefs.getString('sessionID');
        ReadMessageRequest readMessageRequest = new ReadMessageRequest(
          userID: userID,
          sessionID: sessionID,
          type: "12",
          merchantID: "",
        );
        ReadMessageResponse readMessageResponse = await goLogin(
            CREATE_POST_URL + '/service002/readMessageSetting',
            readMessageRequest.toMap());
        print(userID);
        print(sessionID);
        if (readMessageResponse.code == '0000') {
          print(readMessageResponse.toString());
          var route = new MaterialPageRoute(
              builder: (BuildContext context) => new TransferConfirmPage(
                  value1: "${widget.value}",
                  value2: "${widget.value1}",
                  value3: myControlleramout.text,
                  value4: myControllerref.text));
          Navigator.of(context).push(route);
        }
      },
      color: Color.fromRGBO(40, 103, 178, 1),
      textColor: Colors.white,
      child: Container(
        width: 300.0,
        height: 43.0,
        child: Center(
          child: (checklang == "Eng")
              ? Text(
                  (checklang == "Eng") ? textEng[5] : textMyan[5],
                  style: TextStyle(fontSize: 18),
                )
              : Text(
                  (checklang == "Eng") ? textEng[5] : textMyan[5],
                  style: TextStyle(fontSize: 16),
                ),
        ),
      ),
    );

    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: new AppBar(
        //Application Bar
        elevation: 0.0,
        backgroundColor: Color.fromRGBO(40, 103, 178, 1),
        centerTitle: true,
        title: (checklang == "Eng")
            ? Text(
                (checklang == "Eng") ? textEng[0] : textMyan[0],
                style: TextStyle(fontSize: 20, color: Colors.white),
              )
            : Text(
                (checklang == "Eng") ? textEng[0] : textMyan[0],
                style: TextStyle(fontSize: 18, color: Colors.white),
              ),
      ),
      body: new Form(
        key: _formKey,
        child: new ListView(
          children: <Widget>[
            SizedBox(height: 10.0),
            Container(
              padding: EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 0.0),
              height: 530,
              child: Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                elevation: 3.0,
                child: ListView(
                  padding: EdgeInsets.all(5.0),
                  children: <Widget>[
                    SizedBox(height: 30.0),
                    Center(
                      child: new Container(
                        padding: EdgeInsets.only(left: 15.0, right: 10.0),
                        child: phoneNumber,
                      ),
                    ),
                    SizedBox(height: 30.0),
                    Center(
                      child: new Container(
                        padding: EdgeInsets.only(left: 15.0, right: 10.0),
                        child: name,
                      ),
                    ),
                    SizedBox(height: 30.0),
                    Center(
                      child: new Container(
                        padding: EdgeInsets.only(left: 15.0, right: 10.0),
                        child: amount,
                      ),
                    ),
                    SizedBox(height: 30.0),
                    Center(
                      child: new Container(
                        padding: EdgeInsets.only(left: 15.0, right: 10.0),
                        child: reference,
                      ),
                    ),
                    SizedBox(height: 40.0),
                    Center(
                      child: new Container(
                        padding: EdgeInsets.only(left: 30.0, right: 30.0),
                        child: transferbutton,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<ReadMessageResponse> goLogin(url, Map jsonMap) async {
    ReadMessageResponse p = new ReadMessageResponse();
    var body;
    try {
      body = await http.doPost(url, jsonMap);
      p = ReadMessageResponse.fromJson(json.decode(body.toString()));
    } catch (e) {
      p.otpCode = Constants.responseCode_Error;
      p.otpMessage = e.toString();
    }
    print(p.toMap());
    return p;
  }
}
