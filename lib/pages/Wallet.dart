import 'dart:convert';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:nsb/constants/constant.dart';
import 'package:nsb/model/GetBalanceRequest.dart';
import 'package:nsb/model/GetBalanceResponse.dart';
import 'package:nsb/model/TransitionListRequest.dart';
import 'package:nsb/model/TransitionListResponse.dart';
import 'package:nsb/pages/Contact%20qr/Generateqr.dart';
import 'package:nsb/pages/Contact.dart';
import 'package:nsb/pages/Scan/GenerateQR/QR.dart';
import 'package:nsb/pages/Scan/Scan.dart';
import 'package:nsb/pages/Transaction/TransitionMain.dart';
import 'package:nsb/pages/Transfer/transfer.dart';
import 'package:nsb/pages/Transfer/transferContact.dart';
import 'package:nsb/pages/accountSecurity/acSecurity.dart';
import 'package:nsb/pages/agent/agent.dart';
import 'package:nsb/pages/agent/agentTransfer.dart';
import 'package:nsb/pages/billTabPage.dart';
import 'package:nsb/pages/contactsTab.dart';
import 'package:nsb/pages/exchangerate/exchangerate.dart';
import 'package:nsb/pages/faq/faq.dart';
import 'package:nsb/pages/locationPage.dart';
import 'package:nsb/pages/messagesTab.dart';
import 'package:nsb/pages/newLoginPage.dart';
import 'package:nsb/pages/newsFeeds.dart';
import 'package:nsb/constants/rout_path.dart' as routes;
import 'package:nsb/pages/notification/notification.dart';
import 'package:nsb/pages/profile/profile.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:nsb/framework/http_service.dart' as http;
import 'package:markdown/markdown.dart' as md;
import 'package:condition/condition.dart';

class WalletPage extends StatefulWidget {
  String sessionID;
  final String value;
  final String value1;

  WalletPage({Key key, this.value, this.value1}) : super(key: key);

  @override
  State<StatefulWidget> createState() => new WalletPageState();
}

class WalletPageState extends State<WalletPage> {
  static final CREATE_POST_URL =
      "http://122.248.120.16:8080/WalletService/module001";
  var balance = 0.0;
  var locationList = [];
  var displayArray = [];
  var datas="";
  var name="", topup="", amount="", date="";
  String uu, ss;
  String checklang = '';
  List advList = [];
  List textMyan = [
    "ငွေပေးချေ",
    "ငွေလက်ခံ",
    "ငွေလွှဲရန်",
    "ဘေလ်",
    "ငွေသွင်း",
    "ငွေထုတ်",
    "ဝေါ(လ်)လတ် လက်ကျန်ငွေ",
    "ကျပ်",
    "ငွေဖြည့်ရန်",
    "လုပ်ဆောင်မှုမှတ်တမ်း",
    "အားလုံးကြည့်ရန်",
    "ကိုယ်စားလှယ်",
  ];
  List textEng = [
    "Scan",
    "My QR",
    "Transfer",
    "Bills",
    "Cash In",
    "Cash Out",
    "Wallet Balance",
    "MMK",
    "Top Up",
    "Activity",
    "View all",
    "Agent",
  ];
  List drawertextMyan = [
    "အကောင့်လုံခြုံရေး",
    "ငွေလွှဲရန်",
    "တည်နေရာ",
    "ငွေလွှဲနှုန်း",
    "မေးလေ့ရှိသောမေးခွန်းများ",
    "ဆက်သွယ်ရန်",
    "ထွက်ရန်"
  ];
  List drawertextEng = [
    "Account Security",
    "Transfer",
    "Location",
    "Exchange Rate",
    "FAQ",
    "Contact Us",
    "Log Out"
  ];
  List bottombarMyan = ["ဝေါ(လ်)လတ်", "မက်ဆေ့ချ်", "အဆက်အသွယ်", "သတင်း"];
  List bottombarEng = ["Wallet", "Message", "Contacts", "News"];
  @override
  void initState() {
    super.initState();
    checkLanguage();
    this.getBalance();
    this.getList();
    this.getNotiCount();
    this.getadbLink();
  }

  CarouselSlider carouselSlider;
  int _current = 0;
  List imgList = [
    'http://122.248.120.16:8080/WalletService//image//1.jpg',
    'http://122.248.120.16:8080/WalletService//image//2.jpg',
    'http://122.248.120.16:8080/WalletService//image//3.jpg',
    'http://122.248.120.16:8080/WalletService//image//1.jpg',
    'http://122.248.120.16:8080/WalletService//image//2.jpg'
  ];

  List<T> map<T>(List list, Function handler) {
    List<T> result = [];
    for (var i = 0; i < list.length; i++) {
      result.add(handler(i, list[i]));
    }
    return result;
  }

  goToPrevious() {
    carouselSlider.previousPage(
        duration: Duration(milliseconds: 300), curve: Curves.ease);
  }

  goToNext() {
    carouselSlider.nextPage(
        duration: Duration(milliseconds: 300), curve: Curves.decelerate);
  }

  refreshPage() {
    this.getBalance();
    this.getList();
    this.getNotiCount();
    this.getadbLink();
  }
    Future<Null> refreshing() async {
    await Future.delayed(Duration(seconds: 3));
    refreshPage();
    return null;
  }

  getBalance() async {
    final prefs = await SharedPreferences.getInstance();
    String userID = prefs.getString('userId');
    String sessionID = prefs.getString('sessionID');

    GetBalanceRequest getBalanceRequest =
        new GetBalanceRequest(userID: userID, sessionID: sessionID);
    GetBalanceResponse getBalanceResponse = await goBalance(
        CREATE_POST_URL + '/wallet/getBalanceWL', getBalanceRequest.toMap());
    if (getBalanceResponse.messageCode == '0000') {
      setState(() {
        balance = getBalanceResponse.balance;
      });
      print("Result..." + getBalanceResponse.toString());
    }
  }

  checkLanguage() async {
    final prefs = await SharedPreferences.getInstance();
    checklang = prefs.getString("Lang");
    // print(lang);
    if (checklang == "" || checklang == null || checklang.length == 0) {
      checklang = "Eng";
    } else {
      checklang = checklang;
    }
    setState(() {});
  }

  getList() async {
    final prefs = await SharedPreferences.getInstance();
    String userID = prefs.getString('userId');
    String sessionID = prefs.getString('sessionID');
    String accountNo = prefs.getString('accountNo');

    TransitionListRequest transitionListRequest = new TransitionListRequest(
      userID: userID,
      sessionID: sessionID,
      customerNo: "",
      durationType: 0,
      fromDate: "",
      toDate: "",
      totalCount: 0,
      acctNo: accountNo,
      currentPage: 1,
      pageSize: 10,
      pageCount: 0,
    );
    TransitionListResponse transitionListResponse = await getAllList(
        CREATE_POST_URL + '/service001/getTransactionActivityList',
        transitionListRequest.toMap());
    String dateString = locationList[0]["txnDate"];
    String d = dateString.substring(0, 4);
    String e = dateString.substring(5, 7);
    String f = dateString.substring(8, 10);
    String g = d + e + f;
    var date = new DateFormat.yMMMd().format(DateTime.parse(g));
    datas = date;
    name = displayArray[1];
    topup = locationList[0]["remark"];
    amount = locationList[0]["txnAmount"] + " MMK";
    date = datas;
    if (transitionListResponse.code == '0000') {
      print(locationList);
      print('Get Everything.............');
      print(displayArray);
      print(locationList[0]["txnDate"]);
    }
  }

  getadbLink() async {
    final url =
        "http://122.248.120.16:8080/WalletService/module001/service001/getAdLink";
    http.get(Uri.encodeFull(url), headers: {
      "Accept": "application/json",
      "content-type": "application/json"
    }).then((dynamic res) {
      var result = json.decode(res.body);
      print("result ===> $result");
      if (result['code'] == "0000") {
        var data = result['dataList'];
        for (var i = 0; i < data.length; i++) {
          advList.add(data[i]['logo'].toString().substring(21));
        }
        // advList = data;
      }
      print(advList);
    });
  }

  getNotiCount() async {
    final prefs = await SharedPreferences.getInstance();
    uu = prefs.getString('userId');
    ss = prefs.getString('name');
    setState(() {});
  }
  int _selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    final _tabs = [
      storeTab(context),
      messagesTab(),
      contactsTab(),
      NewFeedsPage(),
      Text('Tab5'),
    ];
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          centerTitle: true,
          elevation: 5.0,
          backgroundColor: Colors.white,
          leading: Builder(
            builder: (context) => IconButton(
              icon:
                  new Icon(Icons.menu, color: Color.fromRGBO(40, 103, 178, 1)),
              onPressed: () => Scaffold.of(context).openDrawer(),
            ),
          ),
          title: Text(
            "mWallet",
            textAlign: TextAlign.center,
            style:
                TextStyle(color: Color.fromRGBO(40, 103, 178, 1), fontSize: 20),
          ),
          actions: <Widget>[
            IconButton(
              padding: EdgeInsets.all(0),
              onPressed: () {
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => Noti()));
              },
              iconSize: 25,
              icon: Icon(
                Icons.notifications_active,
                color: Color.fromRGBO(40, 103, 178, 1),
              ),
            )
          ],
        ),
        drawer: _drawer(),
        body: RefreshIndicator(
          onRefresh: ()async{
            await refreshing();
          },
          child: _tabs[_selectedIndex],  
        ),
        bottomNavigationBar: BottomNavigationBar(
          unselectedItemColor: Colors.white,
          backgroundColor: Color.fromRGBO(40, 103, 178, 1),
          items: <BottomNavigationBarItem>[
            BottomNavigationBarItem(
                icon: Image.asset("assets/images/wallet-home.png",
                    width: 24, height: 24),
                title: Text(
                  (checklang == "Eng") ? bottombarEng[0] : bottombarMyan[0],
                )),
            BottomNavigationBarItem(
                icon: Image.asset("assets/images/message.png",
                    width: 24, height: 24),
                title: Text(
                  (checklang == "Eng") ? bottombarEng[1] : bottombarMyan[1],
                )),
            BottomNavigationBarItem(
                icon: Image.asset("assets/images/contact.png",
                    width: 24, height: 24),
                title: Text(
                  (checklang == "Eng") ? bottombarEng[2] : bottombarMyan[2],
                )),
            BottomNavigationBarItem(
                icon: Image.asset("assets/images/news.png",
                    width: 24, height: 24),
                title: Text(
                  (checklang == "Eng") ? bottombarEng[3] : bottombarMyan[3],
                )),
          ],
          currentIndex: _selectedIndex,
          type: BottomNavigationBarType.fixed,
          fixedColor: Colors.blue[200],
          onTap: _onItemTapped,
        ));
  }

  Future<TransitionListResponse> getAllList(url, Map jsonMap) async {
    TransitionListResponse p = new TransitionListResponse();
    var body;
    try {
      body = await http.doPost(url, jsonMap);
      p = TransitionListResponse.fromJson(json.decode(body.toString()));
      Map data = jsonDecode(body);
      setState(() {
        locationList = data["data"];
        this.displayArray = this.locationList[0]["txnTypeDesc"].split(',');
      });
    } catch (e) {
      p.code = Constants.responseCode_Error;
      p.desc = e.toString();
    }
    print(p.toMap());
    return p;
  }

  Future<GetBalanceResponse> goBalance(url, Map jsonMap) async {
    GetBalanceResponse p = new GetBalanceResponse();
    var body;
    try {
      body = await http.doPost(url, jsonMap);
      p = GetBalanceResponse.fromJson(json.decode(body.toString()));
    } catch (e) {
      p.messageCode = Constants.responseCode_Error;
      p.messageDesc = e.toString();
    }
    print(p.toMap());
    return p;
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  ListView storeTab(BuildContext context) {
    return ListView(
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(top: 0.0),
          height: 770.0,
          child: Stack(
            children: <Widget>[
              Container(
                height: 160,
                color: Color.fromRGBO(40, 103, 178, 1),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    height: 120,
                    child: ListView(
                      scrollDirection: Axis.horizontal,
                      shrinkWrap: true,
                      children: <Widget>[
                        SizedBox(width: 20),
                        Column(
                          children: <Widget>[
                            SizedBox(height: 15),
                            GestureDetector(
                              onTap: () {
                                setState(() {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => Scan()));
                                });
                              },
                              child: Container(
                                child: Image.asset(
                                  "assets/images/scan.png",
                                  width: 45.00,
                                  height: 45.00,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                            SizedBox(height: 15),
                            Container(
                              child: (checklang == "Eng")
                                  ? Text(
                                      (checklang == "Eng")
                                          ? textEng[0]
                                          : textMyan[0],
                                      style: TextStyle(
                                          fontSize: 16, color: Colors.white),
                                    )
                                  : Text(
                                      (checklang == "Eng")
                                          ? textEng[0]
                                          : textMyan[0],
                                      style: TextStyle(
                                          fontSize: 14, color: Colors.white),
                                    ),
                            ),
                          ],
                        ),
                        SizedBox(
                          width: 35,
                        ),
                        Column(
                          children: <Widget>[
                            SizedBox(height: 10),
                            GestureDetector(
                              onTap: () {
                                setState(() {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => QRPage()));
                                });
                              },
                              child: Container(
                                child: Image.asset(
                                  "assets/images/qr.png",
                                  width: 55.00,
                                  height: 55.00,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                            SizedBox(height: 10),
                            Container(
                              child: (checklang == "Eng")
                                  ? Text(
                                      (checklang == "Eng")
                                          ? textEng[1]
                                          : textMyan[1],
                                      style: TextStyle(
                                          fontSize: 16, color: Colors.white),
                                    )
                                  : Text(
                                      (checklang == "Eng")
                                          ? textEng[1]
                                          : textMyan[1],
                                      style: TextStyle(
                                          fontSize: 14, color: Colors.white),
                                    ),
                            ),
                          ],
                        ),
                        SizedBox(
                          width: 35,
                        ),
                        Column(
                          children: <Widget>[
                            SizedBox(height: 5),
                            GestureDetector(
                              onTap: () {
                                setState(() {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              TransferContact()));
                                });
                              },
                              child: Container(
                                child: Image.asset(
                                  "assets/images/transfer.png",
                                  width: 65.00,
                                  height: 65.00,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                            SizedBox(height: 4),
                            Container(
                              child: (checklang == "Eng")
                                  ? Text(
                                      (checklang == "Eng")
                                          ? textEng[2]
                                          : textMyan[2],
                                      style: TextStyle(
                                          fontSize: 16, color: Colors.white),
                                    )
                                  : Text(
                                      (checklang == "Eng")
                                          ? textEng[2]
                                          : textMyan[2],
                                      style: TextStyle(
                                          fontSize: 14, color: Colors.white),
                                    ),
                            ),
                          ],
                        ),
                        SizedBox(
                          width: 35,
                        ),
                        Column(
                          children: <Widget>[
                            SizedBox(height: 10),
                            GestureDetector(
                              onTap: () {
                                setState(() {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => billTabPage()));
                                });
                              },
                              child: Container(
                                child: Image.asset(
                                  "assets/images/bill.png",
                                  width: 55.00,
                                  height: 55.00,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                            SizedBox(height: 10),
                            Container(
                              child: (checklang == "Eng")
                                  ? Text(
                                      (checklang == "Eng")
                                          ? textEng[3]
                                          : textMyan[3],
                                      style: TextStyle(
                                          fontSize: 16, color: Colors.white),
                                    )
                                  : Text(
                                      (checklang == "Eng")
                                          ? textEng[3]
                                          : textMyan[3],
                                      style: TextStyle(
                                          fontSize: 14, color: Colors.white),
                                    ),
                            ),
                          ],
                        ),
                        SizedBox(
                          width: 35,
                        ),
                        Column(
                          children: <Widget>[
                            SizedBox(height: 10),
                            GestureDetector(
                              onTap: () {
                                setState(() {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => QRPage()));
                                });
                              },
                              child: Container(
                                child: Image.asset(
                                  "assets/images/qr.png",
                                  width: 55.00,
                                  height: 55.00,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                            SizedBox(height: 10),
                            Container(
                              child: (checklang == "Eng")
                                  ? Text(
                                      (checklang == "Eng")
                                          ? textEng[4]
                                          : textMyan[4],
                                      style: TextStyle(
                                          fontSize: 16, color: Colors.white),
                                    )
                                  : Text(
                                      (checklang == "Eng")
                                          ? textEng[4]
                                          : textMyan[4],
                                      style: TextStyle(
                                          fontSize: 14, color: Colors.white),
                                    ),
                            ),
                          ],
                        ),
                        SizedBox(
                          width: 35,
                        ),
                        Column(
                          children: <Widget>[
                            SizedBox(height: 15),
                            GestureDetector(
                              onTap: () {
                                setState(() {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => Scan()));
                                });
                              },
                              child: Container(
                                child: Image.asset(
                                  "assets/images/scan.png",
                                  width: 45.00,
                                  height: 45.00,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                            SizedBox(height: 15),
                            Container(
                              child: (checklang == "Eng")
                                  ? Text(
                                      (checklang == "Eng")
                                          ? textEng[5]
                                          : textMyan[5],
                                      style: TextStyle(
                                          fontSize: 16, color: Colors.white),
                                    )
                                  : Text(
                                      (checklang == "Eng")
                                          ? textEng[5]
                                          : textMyan[5],
                                      style: TextStyle(
                                          fontSize: 14, color: Colors.white),
                                    ),
                            ),
                          ],
                        ),
                        SizedBox(width: 20),
                      ],
                    ),
                  ),
                ],
              ),
              Container(
                padding: EdgeInsets.only(
                    top: 110.0, left: 15.0, right: 15.0, bottom: 561.0),
                child: Material(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20.0)),
                  elevation: 10.0,
                  color: Colors.white,
                  child: Column(
                    children: <Widget>[
                      Card(
                        elevation: 10.0,
                        margin: const EdgeInsets.symmetric(
                            horizontal: 0, vertical: 0),
                        child: Padding(
                          padding: const EdgeInsets.all(16.0),
                          child: Row(children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                (checklang == "Eng")
                                    ? Text(
                                        (checklang == "Eng")
                                            ? textEng[6]
                                            : textEng[6],
                                        style: TextStyle(
                                            fontSize: 16,
                                            color: Colors.black,
                                            fontWeight: FontWeight.w300),
                                      )
                                    : Text(
                                        (checklang == "Eng")
                                            ? textEng[6]
                                            : textEng[6],
                                        style: TextStyle(
                                            fontSize: 16, color: Colors.black),
                                      ),
                                // Text(
                                //   (checklang == "Eng")
                                //       ? textEng[6]
                                //       : textMyan[6],
                                //   style: TextStyle(
                                //       fontSize: 16, color: Colors.grey),
                                // ),
                                Divider(
                                  height: 20,
                                  color: Colors.black,
                                ),
                                (checklang == "Eng")
                                    ? Text(
                                        "$balance" + " " + textEng[7],
                                        style: TextStyle(
                                          fontSize: 16,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      )
                                    : Text(
                                        "$balance" + " " + textEng[7],
                                        style: TextStyle(
                                            fontSize: 16,
                                            fontWeight: FontWeight.bold),
                                      ),
                                // Text(
                                //   (checklang == "Eng")
                                //       ? "$balance" + " " + textEng[7]
                                //       : "$balance" + " " + textMyan[7],
                                //   style: TextStyle(
                                //       fontSize: 16,
                                //       fontWeight: FontWeight.bold),
                                // ),
                              ],
                            ),
                          ]),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.only(
                    top: 230.0, left: 15.0, right: 15.0, bottom: 355.0),
                child: Material(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0)),
                  elevation: 10.0,
                  color: Colors.white,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Card(
                        margin: const EdgeInsets.symmetric(
                            horizontal: 0, vertical: 0),
                        child: Padding(
                          padding: const EdgeInsets.all(16.0),
                          child: Row(children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                (checklang == "Eng")
                                    ? Text(
                                        (checklang == "Eng")
                                            ? textEng[9]
                                            : textMyan[9],
                                        style: TextStyle(
                                            fontSize: 16,
                                            color: Colors.black,
                                            fontWeight: FontWeight.w300),
                                      )
                                    : Text(
                                        (checklang == "Eng")
                                            ? textEng[9]
                                            : textMyan[9],
                                        style: TextStyle(
                                            fontSize: 14, color: Colors.black),
                                      ),
                                Divider(
                                  height: 25,
                                ),
                                Text(
                                  "$name",
                                  style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold),
                                ),
                                Divider(height: 10),
                                Text(
                                  "$topup",
                                  style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.w300),
                                ),
                              ],
                            ),
                            Spacer(),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: <Widget>[
                                Divider(
                                  height: 45,
                                ),
                                Text(
                                  "$amount",
                                  style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold),
                                ),
                                Divider(height: 10),
                                Text(
                                  "$datas",
                                  style: TextStyle(
                                      fontSize: 15,
                                      fontWeight: FontWeight.w300),
                                ),
                              ],
                            ),
                          ]),
                        ),
                      ),
                      SizedBox(
                        height: 18,
                      ),
                      new GestureDetector(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => TransitionMain()),
                          );
                        },
                        child: Padding(
                          padding: EdgeInsets.only(left: 16),
                          child: (checklang == "Eng")
                              ? Text(
                                  (checklang == "Eng")
                                      ? textEng[10]
                                      : textMyan[10],
                                  style: TextStyle(
                                      fontSize: 16,
                                      color: Colors.black,
                                      fontWeight: FontWeight.w300),
                                )
                              : Text(
                                  (checklang == "Eng")
                                      ? textEng[10]
                                      : textMyan[10],
                                  style: TextStyle(
                                      fontSize: 12, color: Colors.black),
                                ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.only(
                    top: 435.0, left: 15.0, right: 15.0, bottom: 280.0),
                child: Material(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0)),
                  elevation: 10.0,
                  color: Colors.white,
                  child: Column(
                    children: <Widget>[
                      Card(
                        elevation: 10,
                        margin: const EdgeInsets.symmetric(
                            horizontal: 0, vertical: 0),
                        child: Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Row(children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Divider(
                                  height: 4,
                                ),
                                Icon(Icons.contacts,
                                    color: Color.fromRGBO(40, 103, 178, 1)),
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Divider(
                                  height: 1,
                                ),
                                new GestureDetector(
                                  onTap: () {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => AgentPage()),
                                    );
                                  },
                                  child: Padding(
                                    padding: EdgeInsets.only(left: 16),
                                    child: (checklang == "Eng")
                                        ? Text(
                                            (checklang == "Eng")
                                                ? textEng[11]
                                                : textMyan[11],
                                            style: TextStyle(
                                                fontSize: 16,
                                                color: Colors.black,
                                                fontWeight: FontWeight.w300),
                                          )
                                        : Text(
                                            (checklang == "Eng")
                                                ? textEng[11]
                                                : textMyan[11],
                                            style: TextStyle(
                                                fontSize: 14,
                                                color: Colors.black),
                                          ),
                                  ),
                                )
                              ],
                            ),
                            Spacer(),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: <Widget>[
                                GestureDetector(
                                  onTap: () {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => AgentPage()),
                                    );
                                  },
                                                                  child: Icon(Icons.arrow_forward_ios,
                                      color: Colors.black),
                                ),
                                Divider(
                                  height: 5,
                                ),
                              ],
                            ),
                          ]),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.only(
                    top: 510.0, left: 15.0, right: 15.0, bottom: 40.0),
                child: Material(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0)),
                  elevation: 5.0,
                  color: Colors.white,
                  child: Column(
                    children: <Widget>[
                      SizedBox(
                        height: 10.0,
                      ),
                      (advList == [] || advList == null || advList.length == 0)
                          ? Card(
                              elevation: 2,
                              child: CarouselSlider(
                                height: 160.0,
                                initialPage: 0,
                                enlargeCenterPage: false,
                                autoPlay: true,
                                reverse: false,
                                enableInfiniteScroll: true,
                                autoPlayInterval: Duration(seconds: 5),
                                // autoPlayAnimationDuration:
                                //     Duration(milliseconds: 2000),
                                pauseAutoPlayOnTouch: Duration(seconds: 0),
                                scrollDirection: Axis.horizontal,
                                items: <Widget>[SizedBox(height: 0)],
                                // initialPage: 0,
                              ),
                            )
                          : Card(
                              elevation: 2,
                              child: CarouselSlider(
                                height: 160.0,
                                initialPage: 0,
                                enlargeCenterPage: false,
                                autoPlay: true,
                                reverse: false,
                                enableInfiniteScroll: true,
                                autoPlayInterval: Duration(seconds: 5),
                                // autoPlayAnimationDuration:
                                //     Duration(milliseconds: 5000),
                                pauseAutoPlayOnTouch: Duration(seconds: 0),
                                scrollDirection: Axis.horizontal,
                                items: advList.map((imgUrl) {
                                  return Builder(
                                    builder: (BuildContext context) {
                                      return Container(
                                        width:
                                            MediaQuery.of(context).size.width,
                                        margin: EdgeInsets.all(5.0),
                                        decoration: BoxDecoration(
                                            // color: Colors.green,
                                            ),
                                        child: Image.network(
                                          "http://122.248.120.16:8080" + imgUrl,
                                          fit: BoxFit.fill,
                                        ),
                                      );
                                    },
                                  );
                                }).toList(),
                              ),
                            )
                    ],
                  ),
                ),
              ),
            ],
          ),
        )
      ],
    );
  }

  Widget _drawer() {
    return Container(
      color: Colors.grey[100],
      child: new SizedBox(
          width: MediaQuery.of(context).size.width * 0.80,
          child: new ListView(
              padding: EdgeInsets.fromLTRB(0, 00, 0, 0),
              children: <Widget>[
                Container(
                  height: 200,
                  width: 100,
                  // color: Color.fromRGBO(40, 103, 178, 1),
                  padding: EdgeInsets.all(0),
                  child: new DrawerHeader(
                    
                    margin: EdgeInsets.only(bottom: 0),
                    padding: EdgeInsets.only(left: 0, right: 0, bottom: 0, top :0),
                    child: Column(
                      children: <Widget>[
                        Container(
                          decoration: new BoxDecoration(
                            image: new DecorationImage(
                              image: new AssetImage('assets/images/three.png'),
                              fit: BoxFit.cover,
                            ),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(15.0),
                            child: Row(                            
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Column(
                                  
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Stack(
                                      children: <Widget>[
                                        Positioned(
                                          // bottom: 12.0,
                                          // left: 16.0,
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: <Widget>[
                                              GestureDetector(
                                                  onTap: () {
                                                    var route =
                                                        new MaterialPageRoute(
                                                            builder: (BuildContext
                                                                    context) =>
                                                                new Profile(
                                                                  value1: '$uu',
                                                                  value2: '$ss',
                                                                ));
                                                    Navigator.of(context)
                                                        .push(route);
                                                  },
                                                  child: CircleAvatar(
                                                    radius: 37,
                                                    backgroundImage: AssetImage(
                                                        'assets/images/user.png'),
                                                  )),
                                              SizedBox(height: 20),
                                              Padding(
                                                padding:
                                                    EdgeInsets.only(left: 10),
                                                child: Text(
                                                  '$ss',
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: 15.0),
                                                ),
                                              ),
                                              Padding(
                                                padding: EdgeInsets.only(
                                                    left: 10, top: 5),
                                                child: Text(
                                                  '$uu',
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: 15.0),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Stack(
                                      children: <Widget>[
                                        Positioned(
                                          // bottom: 12.0,
                                          // left: 16.0,
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.end,
                                            children: <Widget>[
                                              GestureDetector(
                                                onTap: () {
                                                  Navigator.push(
                                                    context,
                                                    MaterialPageRoute(
                                                      builder: (context) =>
                                                          Generateqr(),
                                                    ),
                                                  );
                                                },
                                                child: Container(
                                                  child: Image.asset(
                                                      'assets/images/contactqr.png',
                                                      height: 75,
                                                      width: 75),
                                                ),
                                              ),
                                              SizedBox(height: 20),
                                              Padding(
                                                padding:
                                                    EdgeInsets.only(left: 10),
                                                child: Text(
                                                  "Version 1.0.8",
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: 15.0),
                                                ),
                                              ),
                                              Padding(
                                                padding: EdgeInsets.only(
                                                    left: 10, top: 5),
                                                child: Text(
                                                  "",
                                                  style: TextStyle(
                                                      color: Color(0xFF525252),
                                                      fontSize: 15.0),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                // Divider(
                //   indent: 60,
                //   endIndent: 10,
                //   color: Colors.grey[100],
                //   height: 7,
                // ),
                ListTile(
                  leading:
                      Icon(Icons.lock, color: Color.fromRGBO(40, 103, 178, 1)),
                  title: (checklang == "Eng")
                      ? Text(
                          (checklang == "Eng")
                              ? drawertextEng[0]
                              : drawertextMyan[0],
                          style: TextStyle(fontSize: 16, color: Colors.black))
                      : Text(
                          (checklang == "Eng")
                              ? drawertextEng[0]
                              : drawertextMyan[0],
                          style: TextStyle(fontSize: 15, color: Colors.black)),
                  trailing: Icon(Icons.chevron_right,
                      color: Color.fromRGBO(40, 103, 178, 1)),
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => AcSecurityPage()),
                    );
                  },
                ),
                Divider(
                  indent: 60,
                  endIndent: 10,
                  color: Colors.grey[500],
                  height: 5,
                ),
                ListTile(
                  leading: Icon(Icons.money_off,
                      color: Color.fromRGBO(40, 103, 178, 1)),
                  title: (checklang == "Eng")
                      ? Text(
                          (checklang == "Eng")
                              ? drawertextEng[1]
                              : drawertextMyan[1],
                          style: TextStyle(fontSize: 16, color: Colors.black))
                      : Text(
                          (checklang == "Eng")
                              ? drawertextEng[1]
                              : drawertextMyan[1],
                          style: TextStyle(fontSize: 15, color: Colors.black)),
                  trailing: Icon(Icons.chevron_right,
                      color: Color.fromRGBO(40, 103, 178, 1)),
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => TransferPage()),
                    );
                  },
                ),
                Divider(
                  indent: 60,
                  endIndent: 10,
                  color: Colors.grey[500],
                  height: 5,
                ),
                ListTile(
                  leading: Icon(Icons.location_on,
                      color: Color.fromRGBO(40, 103, 178, 1)),
                  title: (checklang == "Eng")
                      ? Text(
                          (checklang == "Eng")
                              ? drawertextEng[2]
                              : drawertextMyan[2],
                          style: TextStyle(fontSize: 16, color: Colors.black))
                      : Text(
                          (checklang == "Eng")
                              ? drawertextEng[2]
                              : drawertextMyan[2],
                          style: TextStyle(fontSize: 15, color: Colors.black)),
                  trailing: Icon(Icons.chevron_right,
                      color: Color.fromRGBO(40, 103, 178, 1)),
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => locationPage()),
                    );
                  },
                ),
                Divider(
                  indent: 60,
                  endIndent: 10,
                  color: Colors.grey[500],
                  height: 5,
                ),
                ListTile(
                  leading:
                      Icon(Icons.loop, color: Color.fromRGBO(40, 103, 178, 1)),
                  title: (checklang == "Eng")
                      ? Text(
                          (checklang == "Eng")
                              ? drawertextEng[3]
                              : drawertextMyan[3],
                          style: TextStyle(fontSize: 16, color: Colors.black))
                      : Text(
                          (checklang == "Eng")
                              ? drawertextEng[3]
                              : drawertextMyan[3],
                          style: TextStyle(fontSize: 15, color: Colors.black)),
                  trailing: Icon(Icons.chevron_right,
                      color: Color.fromRGBO(40, 103, 178, 1)),
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => Exchanerate()));
                  },
                ),
                Divider(
                  indent: 60,
                  endIndent: 10,
                  color: Colors.grey[500],
                  height: 5,
                ),
                ListTile(
                  leading: Icon(Icons.question_answer,
                      color: Color.fromRGBO(40, 103, 178, 1)),
                  title: (checklang == "Eng")
                      ? Text(
                          (checklang == "Eng")
                              ? drawertextEng[4]
                              : drawertextMyan[4],
                          style: TextStyle(fontSize: 16, color: Colors.black))
                      : Text(
                          (checklang == "Eng")
                              ? drawertextEng[4]
                              : drawertextMyan[4],
                          style: TextStyle(fontSize: 15, color: Colors.black)),
                  trailing: Icon(Icons.chevron_right,
                      color: Color.fromRGBO(40, 103, 178, 1)),
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => FAQ()),
                    );
                  },
                ),
                Divider(
                  indent: 60,
                  endIndent: 10,
                  color: Colors.grey[500],
                  height: 5,
                ),
                ListTile(
                  leading:
                      Icon(Icons.phone, color: Color.fromRGBO(40, 103, 178, 1)),
                  title: (checklang == "Eng")
                      ? Text(
                          (checklang == "Eng")
                              ? drawertextEng[5]
                              : drawertextMyan[5],
                          style: TextStyle(fontSize: 16, color: Colors.black))
                      : Text(
                          (checklang == "Eng")
                              ? drawertextEng[5]
                              : drawertextMyan[5],
                          style: TextStyle(fontSize: 15, color: Colors.black)),
                  trailing: Icon(Icons.chevron_right,
                      color: Color.fromRGBO(40, 103, 178, 1)),
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => Contact()),
                    );
                  },
                ),
                Divider(
                  indent: 60,
                  endIndent: 10,
                  color: Colors.grey[500],
                  height: 5,
                ),
                ListTile(
                  leading: Icon(Icons.power_settings_new,
                      color: Color.fromRGBO(40, 103, 178, 1)),
                  title: (checklang == "Eng")
                      ? Text(
                          (checklang == "Eng")
                              ? drawertextEng[6]
                              : drawertextMyan[6],
                          style: TextStyle(fontSize: 16, color: Colors.black))
                      : Text(
                          (checklang == "Eng")
                              ? drawertextEng[6]
                              : drawertextMyan[6],
                          style: TextStyle(fontSize: 15, color: Colors.black)),
                  trailing: Icon(Icons.chevron_right,
                      color: Color.fromRGBO(40, 103, 178, 1)),
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => NewLoginPage()),
                    );
                  },
                )
              ])),
    );
  }
}

Widget headerTopCategories(BuildContext context) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.center,
    children: <Widget>[
      SizedBox(
        height: 130,
        child: ListView(
          scrollDirection: Axis.horizontal,
          shrinkWrap: true,
          children: <Widget>[
            headerCategoryItem(
                'Scan',
                Image.asset(
                  "assets/images/scan.png",
                  width: 45.00,
                  height: 45.00,
                  color: Colors.white,
                ), onPressed: () {
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => Scan()));
            }),
            headerCategoryItem(
                'My QR',
                Image.asset(
                  "assets/images/qr.png",
                  width: 55.00,
                  height: 55.00,
                  color: Colors.white,
                ), onPressed: () {
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => QRPage()));
            }),
            headerCategoryItem(
                'Transfer',
                Image.asset(
                  "assets/images/transfer.png",
                  width: 65.00,
                  height: 65.00,
                  color: Colors.white,
                ), onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => TransferContact()));
            }),
            headerCategoryItem(
                'Bills',
                Image.asset(
                  "assets/images/bill.png",
                  width: 55.00,
                  height: 55.00,
                  color: Colors.white,
                ), onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => billTabPage()));
            }),
            headerCategoryItem(
                'Cash In',
                Image.asset(
                  "assets/images/qr.png",
                  width: 55.00,
                  height: 55.00,
                  color: Colors.red,
                ), onPressed: () {
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => QRPage()));
            }),
            headerCategoryItem(
                'Cash Out',
                Image.asset(
                  "assets/images/scan.png",
                  width: 45.00,
                  height: 45.00,
                  color: Colors.white,
                ), onPressed: () {
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => Scan()));
            }),
          ],
        ),
      ),
    ],
  );
}

Widget headerCategoryItem(String name, Image img, {onPressed}) {
  return Container(
    margin: EdgeInsets.only(left: 12, top: 5),
    child: Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Container(
            margin: EdgeInsets.only(bottom: 10),
            width: 86,
            height: 66,
            child: FloatingActionButton(
              elevation: 0,
              shape: RoundedRectangleBorder(),
              heroTag: name,
              onPressed: onPressed,
              backgroundColor: Color.fromRGBO(40, 103, 178, 1),
              child: img,
            )),
        Text(name + ' ',
            style: TextStyle(
              fontSize: 16.0,
              color: Colors.white,
            ))
      ],
    ),
  );
}
